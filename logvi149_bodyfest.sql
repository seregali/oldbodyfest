-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Хост: localhost:3306
-- Время создания: Май 20 2019 г., 16:43
-- Версия сервера: 5.6.43-cll-lve
-- Версия PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `logvi149_bodyfest`
--

-- --------------------------------------------------------

--
-- Структура таблицы `wp_commentmeta`
--

CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `wp_commentmeta`
--

INSERT INTO `wp_commentmeta` (`meta_id`, `comment_id`, `meta_key`, `meta_value`) VALUES
(1, 2, 'verified', '0');

-- --------------------------------------------------------

--
-- Структура таблицы `wp_comments`
--

CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext NOT NULL,
  `comment_author_email` varchar(100) NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) NOT NULL DEFAULT '',
  `comment_type` varchar(20) NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `wp_comments`
--

INSERT INTO `wp_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'Автор комментария', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2018-07-03 14:05:24', '2018-07-03 11:05:24', 'Привет! Это комментарий.\nЧтобы начать модерировать, редактировать и удалять комментарии, перейдите на экран «Комментарии» в консоли.\nАватары авторов комментариев загружаются с сервиса <a href=\"https://ru.gravatar.com\">Gravatar</a>.', 0, '1', '', '', 0, 0),
(2, 95, 'admin', 'help@ai.kz', '', '77.94.12.192', '2018-08-13 07:29:39', '2018-08-13 04:29:39', 'Лучший кошелек из кожи крокодила.', 0, '1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', '', 0, 1),
(3, 149, 'WooCommerce', 'woocommerce@bodyfest.kz', '', '', '2018-08-20 06:15:13', '2018-08-20 03:15:13', 'Оплата по факту доставки. Статус заказа изменен с В ожидании оплаты на Обработка.', 0, '1', 'WooCommerce', 'order_note', 0, 0),
(4, 150, 'WooCommerce', 'woocommerce@bodyfest.kz', '', '', '2018-08-20 15:03:47', '2018-08-20 12:03:47', 'Оплата по факту доставки. Статус заказа изменен с В ожидании оплаты на Обработка.', 0, '1', 'WooCommerce', 'order_note', 0, 0),
(5, 151, 'WooCommerce', 'woocommerce@bodyfest.kz', '', '', '2018-08-21 13:50:52', '2018-08-21 10:50:52', 'Заказ отменен клиентом. Статус заказа изменен с В ожидании оплаты на Отменен.', 0, '1', 'WooCommerce', 'order_note', 0, 0),
(6, 153, 'WooCommerce', 'woocommerce@bodyfest.kz', '', '', '2018-09-11 19:33:15', '2018-09-11 16:33:15', 'Оплата по факту доставки. Статус заказа изменен с В ожидании оплаты на Обработка.', 0, '1', 'WooCommerce', 'order_note', 0, 0),
(7, 155, 'WooCommerce', 'woocommerce@bodyfest.kz', '', '', '2019-02-17 17:55:02', '2019-02-17 14:55:02', 'Оплата по факту доставки. Статус заказа изменен с В ожидании оплаты на Обработка.', 0, '1', 'WooCommerce', 'order_note', 0, 0),
(8, 156, 'WooCommerce', 'woocommerce@bodyfest.kz', '', '', '2019-03-15 22:34:19', '2019-03-15 19:34:19', 'Оплата по факту доставки. Статус заказа изменен с В ожидании оплаты на Обработка.', 0, '1', 'WooCommerce', 'order_note', 0, 0),
(9, 161, 'WooCommerce', 'woocommerce@bodyfest.kz', '', '', '2019-03-16 13:37:24', '2019-03-16 10:37:24', 'Оплата по факту доставки. Статус заказа изменен с В ожидании оплаты на Обработка.', 0, '1', 'WooCommerce', 'order_note', 0, 0),
(10, 161, 'WooCommerce', 'woocommerce@bodyfest.kz', '', '', '2019-03-16 13:37:24', '2019-03-16 10:37:24', 'Запасы Спортивный костюм (2221111) уменьшены с 10 до 9.', 0, '1', 'WooCommerce', 'order_note', 0, 0),
(11, 157, 'WooCommerce', 'woocommerce@bodyfest.kz', '', '', '2019-03-16 23:15:14', '2019-03-16 20:15:14', 'PayBox Order payment failed (%1$s:%2$s) Статус заказа изменен с В ожидании оплаты на Не удался.', 0, '1', 'WooCommerce', 'order_note', 0, 0),
(12, 159, 'WooCommerce', 'woocommerce@bodyfest.kz', '', '', '2019-03-17 13:45:25', '2019-03-17 10:45:25', 'PayBox Order payment failed (%1$s:%2$s) Статус заказа изменен с В ожидании оплаты на Не удался.', 0, '1', 'WooCommerce', 'order_note', 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `wp_failed_jobs`
--

CREATE TABLE `wp_failed_jobs` (
  `id` bigint(20) NOT NULL,
  `job` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `wp_links`
--

CREATE TABLE `wp_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) NOT NULL DEFAULT '',
  `link_name` varchar(255) NOT NULL DEFAULT '',
  `link_image` varchar(255) NOT NULL DEFAULT '',
  `link_target` varchar(25) NOT NULL DEFAULT '',
  `link_description` varchar(255) NOT NULL DEFAULT '',
  `link_visible` varchar(20) NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) NOT NULL DEFAULT '',
  `link_notes` mediumtext NOT NULL,
  `link_rss` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `wp_mailchimp_carts`
--

CREATE TABLE `wp_mailchimp_carts` (
  `id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `cart` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `wp_options`
--

CREATE TABLE `wp_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) NOT NULL DEFAULT '',
  `option_value` longtext NOT NULL,
  `autoload` varchar(20) NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `wp_options`
--

INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://bodyfest.kz', 'yes'),
(2, 'home', 'http://bodyfest.kz', 'yes'),
(3, 'blogname', 'Bodyfest', 'yes'),
(4, 'blogdescription', '', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'bessonoff_a_v@mail.ru', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '1', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'd.m.Y', 'yes'),
(24, 'time_format', 'H:i', 'yes'),
(25, 'links_updated_date_format', 'd.m.Y H:i', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%year%/%monthnum%/%day%/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:182:{s:24:\"^wc-auth/v([1]{1})/(.*)?\";s:63:\"index.php?wc-auth-version=$matches[1]&wc-auth-route=$matches[2]\";s:22:\"^wc-api/v([1-3]{1})/?$\";s:51:\"index.php?wc-api-version=$matches[1]&wc-api-route=/\";s:24:\"^wc-api/v([1-3]{1})(.*)?\";s:61:\"index.php?wc-api-version=$matches[1]&wc-api-route=$matches[2]\";s:61:\"(([^/]+/)*wishlist)/([A-Fa-f0-9]{6})?/page/([0-9]{1,})/{0,1}$\";s:69:\"index.php?pagename=$matches[1]&tinvwlID=$matches[3]&paged=$matches[4]\";s:44:\"(([^/]+/)*wishlist)/([A-Fa-f0-9]{6})?/{0,1}$\";s:51:\"index.php?pagename=$matches[1]&tinvwlID=$matches[3]\";s:7:\"shop/?$\";s:27:\"index.php?post_type=product\";s:37:\"shop/feed/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?post_type=product&feed=$matches[1]\";s:32:\"shop/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?post_type=product&feed=$matches[1]\";s:24:\"shop/page/([0-9]{1,})/?$\";s:45:\"index.php?post_type=product&paged=$matches[1]\";s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:11:\"partners/?$\";s:28:\"index.php?post_type=partners\";s:41:\"partners/feed/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?post_type=partners&feed=$matches[1]\";s:36:\"partners/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?post_type=partners&feed=$matches[1]\";s:28:\"partners/page/([0-9]{1,})/?$\";s:46:\"index.php?post_type=partners&paged=$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:32:\"category/(.+?)/wc-api(/(.*))?/?$\";s:54:\"index.php?category_name=$matches[1]&wc-api=$matches[3]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:29:\"tag/([^/]+)/wc-api(/(.*))?/?$\";s:44:\"index.php?tag=$matches[1]&wc-api=$matches[3]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:55:\"product-category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_cat=$matches[1]&feed=$matches[2]\";s:50:\"product-category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_cat=$matches[1]&feed=$matches[2]\";s:31:\"product-category/(.+?)/embed/?$\";s:44:\"index.php?product_cat=$matches[1]&embed=true\";s:43:\"product-category/(.+?)/page/?([0-9]{1,})/?$\";s:51:\"index.php?product_cat=$matches[1]&paged=$matches[2]\";s:25:\"product-category/(.+?)/?$\";s:33:\"index.php?product_cat=$matches[1]\";s:52:\"product-tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_tag=$matches[1]&feed=$matches[2]\";s:47:\"product-tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_tag=$matches[1]&feed=$matches[2]\";s:28:\"product-tag/([^/]+)/embed/?$\";s:44:\"index.php?product_tag=$matches[1]&embed=true\";s:40:\"product-tag/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?product_tag=$matches[1]&paged=$matches[2]\";s:22:\"product-tag/([^/]+)/?$\";s:33:\"index.php?product_tag=$matches[1]\";s:35:\"product/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:45:\"product/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:65:\"product/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:60:\"product/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:60:\"product/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:41:\"product/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:24:\"product/([^/]+)/embed/?$\";s:40:\"index.php?product=$matches[1]&embed=true\";s:28:\"product/([^/]+)/trackback/?$\";s:34:\"index.php?product=$matches[1]&tb=1\";s:48:\"product/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?product=$matches[1]&feed=$matches[2]\";s:43:\"product/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?product=$matches[1]&feed=$matches[2]\";s:36:\"product/([^/]+)/page/?([0-9]{1,})/?$\";s:47:\"index.php?product=$matches[1]&paged=$matches[2]\";s:43:\"product/([^/]+)/comment-page-([0-9]{1,})/?$\";s:47:\"index.php?product=$matches[1]&cpage=$matches[2]\";s:33:\"product/([^/]+)/wc-api(/(.*))?/?$\";s:48:\"index.php?product=$matches[1]&wc-api=$matches[3]\";s:39:\"product/[^/]+/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:50:\"product/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:32:\"product/([^/]+)(?:/([0-9]+))?/?$\";s:46:\"index.php?product=$matches[1]&page=$matches[2]\";s:24:\"product/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:34:\"product/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:54:\"product/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:49:\"product/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:49:\"product/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:30:\"product/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:34:\"partners/.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:44:\"partners/.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:64:\"partners/.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:59:\"partners/.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:59:\"partners/.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:40:\"partners/.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:23:\"partners/(.+?)/embed/?$\";s:41:\"index.php?partners=$matches[1]&embed=true\";s:27:\"partners/(.+?)/trackback/?$\";s:35:\"index.php?partners=$matches[1]&tb=1\";s:47:\"partners/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?partners=$matches[1]&feed=$matches[2]\";s:42:\"partners/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?partners=$matches[1]&feed=$matches[2]\";s:35:\"partners/(.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?partners=$matches[1]&paged=$matches[2]\";s:42:\"partners/(.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?partners=$matches[1]&cpage=$matches[2]\";s:32:\"partners/(.+?)/wc-api(/(.*))?/?$\";s:49:\"index.php?partners=$matches[1]&wc-api=$matches[3]\";s:38:\"partners/.+?/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:49:\"partners/.+?/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:31:\"partners/(.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?partners=$matches[1]&page=$matches[2]\";s:12:\"robots\\.txt$\";s:18:\"index.php?robots=1\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:38:\"index.php?&page_id=2&cpage=$matches[1]\";s:17:\"wc-api(/(.*))?/?$\";s:29:\"index.php?&wc-api=$matches[2]\";s:26:\"wcj-my-products(/(.*))?/?$\";s:38:\"index.php?&wcj-my-products=$matches[2]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:26:\"comments/wc-api(/(.*))?/?$\";s:29:\"index.php?&wc-api=$matches[2]\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:29:\"search/(.+)/wc-api(/(.*))?/?$\";s:42:\"index.php?s=$matches[1]&wc-api=$matches[3]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:32:\"author/([^/]+)/wc-api(/(.*))?/?$\";s:52:\"index.php?author_name=$matches[1]&wc-api=$matches[3]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:54:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/wc-api(/(.*))?/?$\";s:82:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&wc-api=$matches[5]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:41:\"([0-9]{4})/([0-9]{1,2})/wc-api(/(.*))?/?$\";s:66:\"index.php?year=$matches[1]&monthnum=$matches[2]&wc-api=$matches[4]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:28:\"([0-9]{4})/wc-api(/(.*))?/?$\";s:45:\"index.php?year=$matches[1]&wc-api=$matches[3]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:58:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:68:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:88:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:64:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:53:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/embed/?$\";s:91:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/trackback/?$\";s:85:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&tb=1\";s:77:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:65:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/page/?([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&paged=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/comment-page-([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&cpage=$matches[5]\";s:62:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/wc-api(/(.*))?/?$\";s:99:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&wc-api=$matches[6]\";s:62:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:73:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:61:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)(?:/([0-9]+))?/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&page=$matches[5]\";s:47:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:57:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:77:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:53:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&cpage=$matches[4]\";s:51:\"([0-9]{4})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&cpage=$matches[3]\";s:38:\"([0-9]{4})/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&cpage=$matches[2]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:25:\"(.?.+?)/wc-api(/(.*))?/?$\";s:49:\"index.php?pagename=$matches[1]&wc-api=$matches[3]\";s:34:\"(.?.+?)/wcj-my-products(/(.*))?/?$\";s:58:\"index.php?pagename=$matches[1]&wcj-my-products=$matches[3]\";s:28:\"(.?.+?)/order-pay(/(.*))?/?$\";s:52:\"index.php?pagename=$matches[1]&order-pay=$matches[3]\";s:33:\"(.?.+?)/order-received(/(.*))?/?$\";s:57:\"index.php?pagename=$matches[1]&order-received=$matches[3]\";s:25:\"(.?.+?)/orders(/(.*))?/?$\";s:49:\"index.php?pagename=$matches[1]&orders=$matches[3]\";s:29:\"(.?.+?)/view-order(/(.*))?/?$\";s:53:\"index.php?pagename=$matches[1]&view-order=$matches[3]\";s:28:\"(.?.+?)/downloads(/(.*))?/?$\";s:52:\"index.php?pagename=$matches[1]&downloads=$matches[3]\";s:31:\"(.?.+?)/edit-account(/(.*))?/?$\";s:55:\"index.php?pagename=$matches[1]&edit-account=$matches[3]\";s:31:\"(.?.+?)/edit-address(/(.*))?/?$\";s:55:\"index.php?pagename=$matches[1]&edit-address=$matches[3]\";s:34:\"(.?.+?)/payment-methods(/(.*))?/?$\";s:58:\"index.php?pagename=$matches[1]&payment-methods=$matches[3]\";s:32:\"(.?.+?)/lost-password(/(.*))?/?$\";s:56:\"index.php?pagename=$matches[1]&lost-password=$matches[3]\";s:34:\"(.?.+?)/customer-logout(/(.*))?/?$\";s:58:\"index.php?pagename=$matches[1]&customer-logout=$matches[3]\";s:37:\"(.?.+?)/add-payment-method(/(.*))?/?$\";s:61:\"index.php?pagename=$matches[1]&add-payment-method=$matches[3]\";s:40:\"(.?.+?)/delete-payment-method(/(.*))?/?$\";s:64:\"index.php?pagename=$matches[1]&delete-payment-method=$matches[3]\";s:45:\"(.?.+?)/set-default-payment-method(/(.*))?/?$\";s:69:\"index.php?pagename=$matches[1]&set-default-payment-method=$matches[3]\";s:31:\".?.+?/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:42:\".?.+?/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:7:{i:0;s:36:\"contact-form-7/wp-contact-form-7.php\";i:1;s:51:\"mailchimp-for-woocommerce/mailchimp-woocommerce.php\";i:2;s:25:\"paybox/gateway-paybox.php\";i:3;s:51:\"ti-woocommerce-wishlist/ti-woocommerce-wishlist.php\";i:4;s:43:\"woocommerce-jetpack/woocommerce-jetpack.php\";i:5;s:27:\"woocommerce/woocommerce.php\";i:6;s:97:\"yikes-inc-easy-custom-woocommerce-product-tabs/yikes-inc-easy-custom-woocommerce-product-tabs.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '3', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', 'a:5:{i:0;s:90:\"/var/www/vhosts/ai.kz/public_html/wpsites/bodyfest.kz/wp-content/themes/os_theme/style.css\";i:1;s:96:\"/var/www/vhosts/ai.kz/public_html/wpsites/bodyfest.kz/wp-content/themes/os_theme/woocommerce.php\";i:3;s:107:\"/var/www/vhosts/ai.kz/public_html/wpsites/bodyfest.kz/wp-content/themes/os_theme/inc/block-main-sidebar.php\";i:4;s:91:\"/var/www/vhosts/ai.kz/public_html/wpsites/bodyfest.kz/wp-content/themes/os_theme/footer.php\";i:5;s:116:\"/var/www/vhosts/ai.kz/public_html/wpsites/bodyfest.kz/wp-content/plugins/woocommerce-jetpack/woocommerce-jetpack.php\";}', 'no'),
(40, 'template', 'os_theme', 'yes'),
(41, 'stylesheet', 'os_theme', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '38590', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '1', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'page', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:8:{i:1;a:0:{}i:2;a:4:{s:5:\"title\";s:33:\"Экспресс-доставка\";s:4:\"text\";s:190:\"Бесплатная доставка по всему Казахстану. При оплате заказа на сайте доставка в любой службой бесплатна.\";s:6:\"filter\";b:1;s:6:\"visual\";b:1;}i:3;a:4:{s:5:\"title\";s:27:\"Возврат товара\";s:4:\"text\";s:160:\"Любой товар не подошедший по каким либо причинам, может быть возвращён в течении 14 дней.\";s:6:\"filter\";b:1;s:6:\"visual\";b:1;}i:4;a:4:{s:5:\"title\";s:42:\"Удобные способы оплаты\";s:4:\"text\";s:129:\"Вы можете оплатить покупки не только наличными, но и банковской картой\";s:6:\"filter\";b:1;s:6:\"visual\";b:1;}i:7;a:4:{s:5:\"title\";s:27:\"Способы оплаты\";s:4:\"text\";s:155:\"Вы можете оплатить покупки наличными при получении, либо банковской картой на сайте.\";s:6:\"filter\";b:1;s:6:\"visual\";b:1;}i:8;a:4:{s:5:\"title\";s:16:\"Контакты\";s:4:\"text\";s:163:\"<strong>Адрес:</strong> Санкибай батыра 45 Б\r\n<strong>Телефон:</strong> +7 (7132) 54-62-85\r\n<strong>Почта:</strong> tshirts@mail.kz\";s:6:\"filter\";b:1;s:6:\"visual\";b:1;}i:9;a:4:{s:5:\"title\";s:0:\"\";s:4:\"text\";s:18:\"+7 (7132) 44-55-66\";s:6:\"filter\";b:1;s:6:\"visual\";b:1;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(80, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(81, 'uninstall_plugins', 'a:1:{s:51:\"ti-woocommerce-wishlist/ti-woocommerce-wishlist.php\";s:23:\"uninstall_tinv_wishlist\";}', 'no'),
(82, 'timezone_string', '', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '2', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '13', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'wp_page_for_privacy_policy', '3', 'yes'),
(92, 'initial_db_version', '38590', 'yes'),
(93, 'wp_user_roles', 'a:7:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:117:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:18:\"manage_woocommerce\";b:1;s:24:\"view_woocommerce_reports\";b:1;s:12:\"edit_product\";b:1;s:12:\"read_product\";b:1;s:14:\"delete_product\";b:1;s:13:\"edit_products\";b:1;s:20:\"edit_others_products\";b:1;s:16:\"publish_products\";b:1;s:21:\"read_private_products\";b:1;s:15:\"delete_products\";b:1;s:23:\"delete_private_products\";b:1;s:25:\"delete_published_products\";b:1;s:22:\"delete_others_products\";b:1;s:21:\"edit_private_products\";b:1;s:23:\"edit_published_products\";b:1;s:20:\"manage_product_terms\";b:1;s:18:\"edit_product_terms\";b:1;s:20:\"delete_product_terms\";b:1;s:20:\"assign_product_terms\";b:1;s:15:\"edit_shop_order\";b:1;s:15:\"read_shop_order\";b:1;s:17:\"delete_shop_order\";b:1;s:16:\"edit_shop_orders\";b:1;s:23:\"edit_others_shop_orders\";b:1;s:19:\"publish_shop_orders\";b:1;s:24:\"read_private_shop_orders\";b:1;s:18:\"delete_shop_orders\";b:1;s:26:\"delete_private_shop_orders\";b:1;s:28:\"delete_published_shop_orders\";b:1;s:25:\"delete_others_shop_orders\";b:1;s:24:\"edit_private_shop_orders\";b:1;s:26:\"edit_published_shop_orders\";b:1;s:23:\"manage_shop_order_terms\";b:1;s:21:\"edit_shop_order_terms\";b:1;s:23:\"delete_shop_order_terms\";b:1;s:23:\"assign_shop_order_terms\";b:1;s:16:\"edit_shop_coupon\";b:1;s:16:\"read_shop_coupon\";b:1;s:18:\"delete_shop_coupon\";b:1;s:17:\"edit_shop_coupons\";b:1;s:24:\"edit_others_shop_coupons\";b:1;s:20:\"publish_shop_coupons\";b:1;s:25:\"read_private_shop_coupons\";b:1;s:19:\"delete_shop_coupons\";b:1;s:27:\"delete_private_shop_coupons\";b:1;s:29:\"delete_published_shop_coupons\";b:1;s:26:\"delete_others_shop_coupons\";b:1;s:25:\"edit_private_shop_coupons\";b:1;s:27:\"edit_published_shop_coupons\";b:1;s:24:\"manage_shop_coupon_terms\";b:1;s:22:\"edit_shop_coupon_terms\";b:1;s:24:\"delete_shop_coupon_terms\";b:1;s:24:\"assign_shop_coupon_terms\";b:1;s:23:\"tinvwl_general_settings\";b:1;s:21:\"tinvwl_style_settings\";b:1;s:14:\"tinvwl_upgrade\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}s:8:\"customer\";a:2:{s:4:\"name\";s:8:\"Customer\";s:12:\"capabilities\";a:1:{s:4:\"read\";b:1;}}s:12:\"shop_manager\";a:2:{s:4:\"name\";s:12:\"Shop manager\";s:12:\"capabilities\";a:92:{s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:4:\"read\";b:1;s:18:\"read_private_pages\";b:1;s:18:\"read_private_posts\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_posts\";b:1;s:10:\"edit_pages\";b:1;s:20:\"edit_published_posts\";b:1;s:20:\"edit_published_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"edit_private_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:17:\"edit_others_pages\";b:1;s:13:\"publish_posts\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_posts\";b:1;s:12:\"delete_pages\";b:1;s:20:\"delete_private_pages\";b:1;s:20:\"delete_private_posts\";b:1;s:22:\"delete_published_pages\";b:1;s:22:\"delete_published_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:19:\"delete_others_pages\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:17:\"moderate_comments\";b:1;s:12:\"upload_files\";b:1;s:6:\"export\";b:1;s:6:\"import\";b:1;s:10:\"list_users\";b:1;s:18:\"manage_woocommerce\";b:1;s:24:\"view_woocommerce_reports\";b:1;s:12:\"edit_product\";b:1;s:12:\"read_product\";b:1;s:14:\"delete_product\";b:1;s:13:\"edit_products\";b:1;s:20:\"edit_others_products\";b:1;s:16:\"publish_products\";b:1;s:21:\"read_private_products\";b:1;s:15:\"delete_products\";b:1;s:23:\"delete_private_products\";b:1;s:25:\"delete_published_products\";b:1;s:22:\"delete_others_products\";b:1;s:21:\"edit_private_products\";b:1;s:23:\"edit_published_products\";b:1;s:20:\"manage_product_terms\";b:1;s:18:\"edit_product_terms\";b:1;s:20:\"delete_product_terms\";b:1;s:20:\"assign_product_terms\";b:1;s:15:\"edit_shop_order\";b:1;s:15:\"read_shop_order\";b:1;s:17:\"delete_shop_order\";b:1;s:16:\"edit_shop_orders\";b:1;s:23:\"edit_others_shop_orders\";b:1;s:19:\"publish_shop_orders\";b:1;s:24:\"read_private_shop_orders\";b:1;s:18:\"delete_shop_orders\";b:1;s:26:\"delete_private_shop_orders\";b:1;s:28:\"delete_published_shop_orders\";b:1;s:25:\"delete_others_shop_orders\";b:1;s:24:\"edit_private_shop_orders\";b:1;s:26:\"edit_published_shop_orders\";b:1;s:23:\"manage_shop_order_terms\";b:1;s:21:\"edit_shop_order_terms\";b:1;s:23:\"delete_shop_order_terms\";b:1;s:23:\"assign_shop_order_terms\";b:1;s:16:\"edit_shop_coupon\";b:1;s:16:\"read_shop_coupon\";b:1;s:18:\"delete_shop_coupon\";b:1;s:17:\"edit_shop_coupons\";b:1;s:24:\"edit_others_shop_coupons\";b:1;s:20:\"publish_shop_coupons\";b:1;s:25:\"read_private_shop_coupons\";b:1;s:19:\"delete_shop_coupons\";b:1;s:27:\"delete_private_shop_coupons\";b:1;s:29:\"delete_published_shop_coupons\";b:1;s:26:\"delete_others_shop_coupons\";b:1;s:25:\"edit_private_shop_coupons\";b:1;s:27:\"edit_published_shop_coupons\";b:1;s:24:\"manage_shop_coupon_terms\";b:1;s:22:\"edit_shop_coupon_terms\";b:1;s:24:\"delete_shop_coupon_terms\";b:1;s:24:\"assign_shop_coupon_terms\";b:1;}}}', 'yes'),
(94, 'fresh_site', '0', 'yes'),
(95, 'WPLANG', 'ru_RU', 'yes'),
(96, 'widget_search', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(97, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(98, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'widget_archives', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(100, 'widget_meta', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(101, 'sidebars_widgets', 'a:11:{s:19:\"wp_inactive_widgets\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:16:\"block_social_top\";a:4:{i:0;s:13:\"custom_html-2\";i:1;s:6:\"text-9\";i:2;s:7:\"os_sw-2\";i:3;s:29:\"wcj_widget_country_switcher-2\";}s:13:\"block_catalog\";a:2:{i:0;s:28:\"woocommerce_product_search-2\";i:1;s:32:\"woocommerce_product_categories-2\";}s:11:\"block_top_1\";a:1:{i:0;s:13:\"custom_html-3\";}s:18:\"block_product_menu\";a:0:{}s:5:\"bonus\";a:3:{i:0;s:6:\"text-2\";i:1;s:6:\"text-3\";i:2;s:6:\"text-4\";}s:14:\"footer_block_1\";a:1:{i:0;s:10:\"nav_menu-2\";}s:14:\"footer_block_2\";a:1:{i:0;s:10:\"nav_menu-3\";}s:14:\"footer_block_3\";a:1:{i:0;s:6:\"text-7\";}s:14:\"footer_block_4\";a:2:{i:0;s:6:\"text-8\";i:1;s:7:\"os_sw-3\";}s:13:\"array_version\";i:3;}', 'yes'),
(102, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(103, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(104, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(105, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(109, 'widget_nav_menu', 'a:3:{i:2;a:2:{s:5:\"title\";s:12:\"Помощь\";s:8:\"nav_menu\";i:3;}i:3;a:2:{s:5:\"title\";s:27:\"Личный кабинет\";s:8:\"nav_menu\";i:39;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'widget_custom_html', 'a:3:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:7:\"content\";s:29:\"<i class=\"fa fa-mobile\"></i> \";}i:3;a:2:{s:5:\"title\";s:0:\"\";s:7:\"content\";s:99:\"[ti_wishlist_products_counter]  <a href=\"/my-account\" class=\"house\">Личный кабинет</a>\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'cron', 'a:14:{i:1558352191;a:1:{s:29:\"wcj_download_tcpdf_fonts_hook\";a:1:{s:32:\"26b69dc9b9c77e4c1647111635ada6f2\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:1:{i:0;s:6:\"hourly\";}s:8:\"interval\";i:3600;}}}i:1558353926;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1558386000;a:1:{s:27:\"woocommerce_scheduled_sales\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1558391185;a:1:{s:28:\"woocommerce_cleanup_sessions\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1558393526;a:3:{s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1558400400;a:1:{s:37:\"tinvwl_remove_without_author_wishlist\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1558412785;a:1:{s:33:\"woocommerce_cleanup_personal_data\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1558412795;a:1:{s:30:\"woocommerce_tracker_send_event\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1558420341;a:1:{s:31:\"auto_update_exchange_rates_hook\";a:1:{s:32:\"b681bdb4ffa2e3ee000b79d7717b3cad\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:1:{i:0;s:5:\"daily\";}s:8:\"interval\";i:86400;}}}i:1558421060;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1558423585;a:1:{s:24:\"woocommerce_cleanup_logs\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1558436747;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1559736000;a:1:{s:25:\"woocommerce_geoip_updater\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:7:\"monthly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:2635200;}}}s:7:\"version\";i:2;}', 'yes'),
(112, 'theme_mods_twentyseventeen', 'a:2:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1530615969;s:4:\"data\";a:4:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:9:\"sidebar-2\";a:0:{}s:9:\"sidebar-3\";a:0:{}}}}', 'yes'),
(125, 'can_compress_scripts', '1', 'no'),
(146, 'current_theme', 'OS Theme', 'yes'),
(147, 'theme_mods_os_theme', 'a:13:{i:0;b:0;s:18:\"nav_menu_locations\";a:1:{s:11:\"header_menu\";i:2;}s:18:\"custom_css_post_id\";i:-1;s:16:\"background_image\";s:60:\"http://bodyfest.kz/wp-content/uploads/2018/08/background.jpg\";s:17:\"background_repeat\";s:9:\"no-repeat\";s:21:\"background_attachment\";s:5:\"fixed\";s:17:\"background_preset\";s:4:\"fill\";s:15:\"background_size\";s:5:\"cover\";s:21:\"background_position_x\";s:6:\"center\";s:21:\"background_position_y\";s:3:\"top\";s:11:\"custom_logo\";i:138;s:16:\"header_textcolor\";s:6:\"ffffff\";s:16:\"background_color\";s:6:\"ffffff\";}', 'yes'),
(148, 'theme_switched', '', 'yes'),
(149, 'widget_os_sw', 'a:3:{i:2;a:7:{s:5:\"title\";s:0:\"\";s:2:\"sc\";s:0:\"\";s:2:\"fb\";s:1:\"#\";s:2:\"ok\";s:1:\"#\";s:2:\"vk\";s:1:\"#\";s:2:\"mm\";s:0:\"\";s:5:\"class\";s:4:\"asoc\";}i:3;a:7:{s:5:\"title\";s:0:\"\";s:2:\"sc\";s:0:\"\";s:2:\"fb\";s:1:\"#\";s:2:\"ok\";s:1:\"#\";s:2:\"vk\";s:1:\"#\";s:2:\"mm\";s:0:\"\";s:5:\"class\";s:6:\"asoc-f\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(150, 'widget_os_pw', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(231, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}', 'yes'),
(318, 'recently_activated', 'a:0:{}', 'yes'),
(327, 'woocommerce_store_address', 'ул.Айтеке-би', 'yes'),
(328, 'woocommerce_store_address_2', 'дом 42 кв.9', 'yes'),
(329, 'woocommerce_store_city', 'Актобе', 'yes'),
(330, 'woocommerce_default_country', 'KZ', 'yes'),
(331, 'woocommerce_store_postcode', '030000', 'yes'),
(332, 'woocommerce_allowed_countries', 'all', 'yes'),
(333, 'woocommerce_all_except_countries', 'a:0:{}', 'yes'),
(334, 'woocommerce_specific_allowed_countries', 'a:0:{}', 'yes'),
(335, 'woocommerce_ship_to_countries', '', 'yes'),
(336, 'woocommerce_specific_ship_to_countries', 'a:0:{}', 'yes'),
(337, 'woocommerce_default_customer_address', 'geolocation', 'yes'),
(338, 'woocommerce_calc_taxes', 'no', 'yes'),
(339, 'woocommerce_enable_coupons', 'yes', 'yes'),
(340, 'woocommerce_calc_discounts_sequentially', 'no', 'no'),
(341, 'woocommerce_currency', 'KZT', 'yes'),
(342, 'woocommerce_currency_pos', 'left_space', 'yes'),
(343, 'woocommerce_price_thousand_sep', ' ', 'yes'),
(344, 'woocommerce_price_decimal_sep', '.', 'yes'),
(345, 'woocommerce_price_num_decimals', '0', 'yes'),
(346, 'woocommerce_shop_page_id', '44', 'yes'),
(347, 'woocommerce_cart_redirect_after_add', 'no', 'yes'),
(348, 'woocommerce_enable_ajax_add_to_cart', 'yes', 'yes'),
(349, 'woocommerce_weight_unit', 'kg', 'yes'),
(350, 'woocommerce_dimension_unit', 'cm', 'yes'),
(351, 'woocommerce_enable_reviews', 'yes', 'yes'),
(352, 'woocommerce_review_rating_verification_label', 'yes', 'no'),
(353, 'woocommerce_review_rating_verification_required', 'yes', 'no'),
(354, 'woocommerce_enable_review_rating', 'yes', 'yes'),
(355, 'woocommerce_review_rating_required', 'yes', 'no'),
(356, 'woocommerce_manage_stock', 'yes', 'yes'),
(357, 'woocommerce_hold_stock_minutes', '60', 'no'),
(358, 'woocommerce_notify_low_stock', 'yes', 'no'),
(359, 'woocommerce_notify_no_stock', 'yes', 'no'),
(360, 'woocommerce_stock_email_recipient', 'help@ai.kz', 'no'),
(361, 'woocommerce_notify_low_stock_amount', '2', 'no'),
(362, 'woocommerce_notify_no_stock_amount', '0', 'yes'),
(363, 'woocommerce_hide_out_of_stock_items', 'no', 'yes'),
(364, 'woocommerce_stock_format', '', 'yes'),
(365, 'woocommerce_file_download_method', 'force', 'no'),
(366, 'woocommerce_downloads_require_login', 'no', 'no'),
(367, 'woocommerce_downloads_grant_access_after_payment', 'yes', 'no'),
(368, 'woocommerce_prices_include_tax', 'no', 'yes'),
(369, 'woocommerce_tax_based_on', 'shipping', 'yes'),
(370, 'woocommerce_shipping_tax_class', 'inherit', 'yes'),
(371, 'woocommerce_tax_round_at_subtotal', 'no', 'yes'),
(372, 'woocommerce_tax_classes', 'Пониженная ставка\nНулевая ставка', 'yes'),
(373, 'woocommerce_tax_display_shop', 'excl', 'yes'),
(374, 'woocommerce_tax_display_cart', 'excl', 'yes'),
(375, 'woocommerce_price_display_suffix', '', 'yes'),
(376, 'woocommerce_tax_total_display', 'itemized', 'no'),
(377, 'woocommerce_enable_shipping_calc', 'yes', 'no'),
(378, 'woocommerce_shipping_cost_requires_address', 'no', 'yes'),
(379, 'woocommerce_ship_to_destination', 'billing', 'no'),
(380, 'woocommerce_shipping_debug_mode', 'no', 'yes'),
(381, 'woocommerce_enable_guest_checkout', 'yes', 'no'),
(382, 'woocommerce_enable_checkout_login_reminder', 'yes', 'no'),
(383, 'woocommerce_enable_signup_and_login_from_checkout', 'yes', 'no'),
(384, 'woocommerce_enable_myaccount_registration', 'yes', 'no'),
(385, 'woocommerce_registration_generate_username', 'yes', 'no'),
(386, 'woocommerce_registration_generate_password', 'yes', 'no'),
(387, 'woocommerce_erasure_request_removes_order_data', 'no', 'no'),
(388, 'woocommerce_erasure_request_removes_download_data', 'no', 'no'),
(389, 'woocommerce_registration_privacy_policy_text', 'Ваши личные данные будут использоваться для упрощения вашей работы с сайтом, управления доступом к вашей учётной записи и для других целей, описанных в нашей [privacy_policy].', 'yes'),
(390, 'woocommerce_checkout_privacy_policy_text', 'Ваши личные данные будут использоваться для обработки ваших заказов, упрощения вашей работы с сайтом и для других целей, описанных в нашей [privacy_policy].', 'yes'),
(391, 'woocommerce_delete_inactive_accounts', 'a:2:{s:6:\"number\";s:0:\"\";s:4:\"unit\";s:6:\"months\";}', 'no'),
(392, 'woocommerce_trash_pending_orders', 'a:2:{s:6:\"number\";s:0:\"\";s:4:\"unit\";s:4:\"days\";}', 'no'),
(393, 'woocommerce_trash_failed_orders', 'a:2:{s:6:\"number\";s:0:\"\";s:4:\"unit\";s:4:\"days\";}', 'no'),
(394, 'woocommerce_trash_cancelled_orders', 'a:2:{s:6:\"number\";s:0:\"\";s:4:\"unit\";s:4:\"days\";}', 'no'),
(395, 'woocommerce_anonymize_completed_orders', 'a:2:{s:6:\"number\";s:0:\"\";s:4:\"unit\";s:6:\"months\";}', 'no'),
(396, 'woocommerce_email_from_name', 'Bodyfest', 'no'),
(397, 'woocommerce_email_from_address', 'help@ai.kz', 'no'),
(398, 'woocommerce_email_header_image', '', 'no'),
(399, 'woocommerce_email_footer_text', '{site_title}', 'no'),
(400, 'woocommerce_email_base_color', '#96588a', 'no'),
(401, 'woocommerce_email_background_color', '#f7f7f7', 'no'),
(402, 'woocommerce_email_body_background_color', '#ffffff', 'no'),
(403, 'woocommerce_email_text_color', '#3c3c3c', 'no'),
(404, 'woocommerce_cart_page_id', '45', 'yes'),
(405, 'woocommerce_checkout_page_id', '46', 'yes'),
(406, 'woocommerce_myaccount_page_id', '47', 'yes'),
(407, 'woocommerce_terms_page_id', '', 'no'),
(408, 'woocommerce_force_ssl_checkout', 'no', 'yes'),
(409, 'woocommerce_unforce_ssl_checkout', 'no', 'yes'),
(410, 'woocommerce_checkout_pay_endpoint', 'order-pay', 'yes'),
(411, 'woocommerce_checkout_order_received_endpoint', 'order-received', 'yes'),
(412, 'woocommerce_myaccount_add_payment_method_endpoint', 'add-payment-method', 'yes'),
(413, 'woocommerce_myaccount_delete_payment_method_endpoint', 'delete-payment-method', 'yes'),
(414, 'woocommerce_myaccount_set_default_payment_method_endpoint', 'set-default-payment-method', 'yes'),
(415, 'woocommerce_myaccount_orders_endpoint', 'orders', 'yes'),
(416, 'woocommerce_myaccount_view_order_endpoint', 'view-order', 'yes'),
(417, 'woocommerce_myaccount_downloads_endpoint', 'downloads', 'yes'),
(418, 'woocommerce_myaccount_edit_account_endpoint', 'edit-account', 'yes'),
(419, 'woocommerce_myaccount_edit_address_endpoint', 'edit-address', 'yes'),
(420, 'woocommerce_myaccount_payment_methods_endpoint', 'payment-methods', 'yes'),
(421, 'woocommerce_myaccount_lost_password_endpoint', 'lost-password', 'yes'),
(422, 'woocommerce_logout_endpoint', 'customer-logout', 'yes'),
(423, 'woocommerce_api_enabled', 'no', 'yes'),
(424, 'woocommerce_single_image_width', '600', 'yes'),
(425, 'woocommerce_thumbnail_image_width', '300', 'yes'),
(426, 'woocommerce_checkout_highlight_required_fields', 'yes', 'yes'),
(427, 'woocommerce_demo_store', 'no', 'yes'),
(428, 'woocommerce_permalinks', 'a:5:{s:12:\"product_base\";s:7:\"product\";s:13:\"category_base\";s:16:\"product-category\";s:8:\"tag_base\";s:11:\"product-tag\";s:14:\"attribute_base\";s:0:\"\";s:22:\"use_verbose_page_rules\";b:0;}', 'yes'),
(429, 'current_theme_supports_woocommerce', 'yes', 'yes'),
(430, 'woocommerce_queue_flush_rewrite_rules', 'no', 'yes'),
(433, 'default_product_cat', '17', 'yes'),
(438, 'woocommerce_admin_notices', 'a:1:{i:0;s:20:\"no_secure_connection\";}', 'yes'),
(439, '_transient_woocommerce_webhook_ids', 'a:0:{}', 'yes'),
(440, 'widget_woocommerce_widget_cart', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(441, 'widget_woocommerce_layered_nav_filters', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(442, 'widget_woocommerce_layered_nav', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(443, 'widget_woocommerce_price_filter', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(444, 'widget_woocommerce_product_categories', 'a:2:{i:2;a:8:{s:5:\"title\";s:0:\"\";s:7:\"orderby\";s:5:\"order\";s:8:\"dropdown\";i:0;s:5:\"count\";i:0;s:12:\"hierarchical\";i:1;s:18:\"show_children_only\";i:0;s:10:\"hide_empty\";i:0;s:9:\"max_depth\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(445, 'widget_woocommerce_product_search', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(446, 'widget_woocommerce_product_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(447, 'widget_woocommerce_products', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(448, 'widget_woocommerce_recently_viewed_products', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(449, 'widget_woocommerce_top_rated_products', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(450, 'widget_woocommerce_recent_reviews', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(451, 'widget_woocommerce_rating_filter', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(456, 'woocommerce_meta_box_errors', 'a:0:{}', 'yes'),
(463, 'woocommerce_product_type', 'physical', 'yes'),
(464, 'woocommerce_allow_tracking', 'no', 'yes');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(466, 'woocommerce_ppec_paypal_settings', 'a:17:{s:5:\"email\";b:0;s:16:\"reroute_requests\";b:0;s:7:\"enabled\";s:2:\"no\";s:11:\"button_size\";s:10:\"responsive\";s:11:\"environment\";s:4:\"live\";s:12:\"mark_enabled\";s:3:\"yes\";s:12:\"api_username\";s:0:\"\";s:12:\"api_password\";s:0:\"\";s:13:\"api_signature\";s:0:\"\";s:15:\"api_certificate\";s:0:\"\";s:11:\"api_subject\";s:0:\"\";s:5:\"debug\";s:2:\"no\";s:14:\"invoice_prefix\";s:3:\"WC-\";s:16:\"instant_payments\";s:2:\"no\";s:15:\"require_billing\";s:2:\"no\";s:13:\"paymentaction\";s:4:\"sale\";s:26:\"subtotal_mismatch_behavior\";s:3:\"add\";}', 'yes'),
(467, 'woocommerce_cheque_settings', 'a:1:{s:7:\"enabled\";s:2:\"no\";}', 'yes'),
(468, 'woocommerce_bacs_settings', 'a:1:{s:7:\"enabled\";s:2:\"no\";}', 'yes'),
(469, 'woocommerce_cod_settings', 'a:6:{s:7:\"enabled\";s:3:\"yes\";s:5:\"title\";s:36:\"Оплата при доставке\";s:11:\"description\";s:69:\"Оплата наличными при доставке заказа.\";s:12:\"instructions\";s:69:\"Оплата наличными при доставке заказа.\";s:18:\"enable_for_methods\";a:0:{}s:18:\"enable_for_virtual\";s:3:\"yes\";}', 'yes'),
(470, 'woocommerce_tracker_last_send', '1530687744', 'yes'),
(471, 'wc_ppec_version', '1.6.1', 'yes'),
(479, 'mailchimp_woocommerce_version', '2.1.6', 'no'),
(480, 'mailchimp-woocommerce', 'a:0:{}', 'yes'),
(482, 'mailchimp-woocommerce-store_id', '5b3c7123ca5fc', 'yes'),
(484, 'mailchimp_woocommerce_db_mailchimp_carts', '1', 'no'),
(487, '_transient_shipping-transient-version', '1530688294', 'yes'),
(495, 'woocommerce_tracker_ua', 'a:1:{i:0;s:113:\"mozilla/5.0 (windows nt 6.1; win64; x64) applewebkit/537.36 (khtml, like gecko) chrome/67.0.3396.99 safari/537.36\";}', 'yes'),
(650, 'auto_core_update_notified', 'a:4:{s:4:\"type\";s:7:\"success\";s:5:\"email\";s:21:\"bessonoff_a_v@mail.ru\";s:7:\"version\";s:6:\"4.9.10\";s:9:\"timestamp\";i:1552444974;}', 'no'),
(695, '_transient_product_query-transient-version', '1552732644', 'yes'),
(697, '_transient_product-transient-version', '1552732644', 'yes'),
(724, 'woocommerce_catalog_columns', '4', 'yes'),
(725, 'woocommerce_maybe_regenerate_images_hash', '991b1ca641921cf0f5baf7a2fe85861b', 'yes'),
(803, 'yit_recently_activated', 'a:1:{i:0;s:41:\"yith-woocommerce-ajax-navigation/init.php\";}', 'yes'),
(804, 'widget_yith-woo-ajax-navigation', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(805, 'widget_yith-woo-ajax-reset-navigation', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(806, 'yit_wcan_options', 'a:6:{s:29:\"yith_wcan_ajax_shop_container\";s:9:\".products\";s:30:\"yith_wcan_ajax_shop_pagination\";s:26:\"nav.woocommerce-pagination\";s:36:\"yith_wcan_ajax_shop_result_container\";s:25:\".woocommerce-result-count\";s:31:\"yith_wcan_ajax_scroll_top_class\";s:19:\".yit-wcan-container\";s:31:\"yith_wcan_ajax_shop_terms_order\";s:12:\"alphabetical\";s:22:\"yith_wcan_custom_style\";s:0:\"\";}', 'yes'),
(921, 'tinvwl_db_ver', '1.8.5', 'yes'),
(922, 'tinvwl-general', 'a:15:{s:13:\"default_title\";s:0:\"\";s:13:\"page_wishlist\";i:87;s:13:\"require_login\";b:0;s:22:\"redirect_require_login\";b:1;s:17:\"link_in_myaccount\";b:1;s:21:\"processing_autoremove\";b:1;s:28:\"processing_redirect_checkout\";b:0;s:28:\"processing_autoremove_anyone\";b:0;s:11:\"simple_flow\";b:0;s:11:\"show_notice\";b:1;s:11:\"text_browse\";s:37:\"Посмотреть гардероб\";s:8:\"redirect\";b:1;s:13:\"text_added_to\";s:47:\"Товар добавлен в гардероб\";s:15:\"text_already_in\";s:39:\"Товар уже в гардеробе\";s:17:\"text_removed_from\";s:29:\"Product removed from Wishlist\";}', 'yes'),
(923, 'tinvwl-page', 'a:1:{s:8:\"wishlist\";i:87;}', 'yes'),
(924, 'tinvwl-processing', 'a:4:{s:10:\"autoremove\";b:1;s:17:\"autoremove_status\";s:14:\"tinvwl-addcart\";s:17:\"redirect_checkout\";b:0;s:17:\"autoremove_anyone\";b:0;}', 'yes'),
(925, 'tinvwl-add_to_wishlist', 'a:8:{s:8:\"position\";s:5:\"after\";s:5:\"class\";s:0:\"\";s:4:\"icon\";s:0:\"\";s:11:\"icon_upload\";s:0:\"\";s:10:\"icon_style\";s:5:\"black\";s:9:\"show_text\";b:1;s:4:\"text\";s:19:\"В гардероб\";s:11:\"text_remove\";s:20:\"Remove from Wishlist\";}', 'yes'),
(926, 'tinvwl-add_to_wishlist_catalog', 'a:9:{s:12:\"show_in_loop\";b:1;s:8:\"position\";s:5:\"after\";s:5:\"class\";s:0:\"\";s:4:\"icon\";s:0:\"\";s:11:\"icon_upload\";s:0:\"\";s:10:\"icon_style\";s:5:\"black\";s:9:\"show_text\";b:1;s:4:\"text\";s:19:\"В гардероб\";s:11:\"text_remove\";s:20:\"Remove from Wishlist\";}', 'yes'),
(927, 'tinvwl-product_table', 'a:5:{s:11:\"add_to_cart\";b:1;s:16:\"text_add_to_cart\";s:17:\"В корзину\";s:10:\"colm_price\";b:1;s:10:\"colm_stock\";b:0;s:9:\"colm_date\";b:0;}', 'yes'),
(928, 'tinvwl-table', 'a:6:{s:13:\"colm_checkbox\";b:0;s:12:\"colm_actions\";b:1;s:18:\"add_select_to_cart\";b:1;s:23:\"text_add_select_to_cart\";s:53:\"Добавить выбранное в корзину\";s:15:\"add_all_to_cart\";b:0;s:20:\"text_add_all_to_cart\";s:41:\"Добавить все в корзину\";}', 'yes'),
(929, 'tinvwl-social', 'a:8:{s:6:\"social\";s:0:\"\";s:8:\"facebook\";b:1;s:7:\"twitter\";b:1;s:9:\"pinterest\";b:1;s:6:\"google\";b:1;s:5:\"email\";b:1;s:8:\"share_on\";s:20:\"Поделиться\";s:10:\"icon_style\";s:5:\"white\";}', 'yes'),
(930, 'tinvwl-topline', 'a:5:{s:4:\"icon\";s:0:\"\";s:11:\"icon_upload\";s:0:\"\";s:10:\"icon_style\";s:5:\"black\";s:9:\"show_text\";b:1;s:4:\"text\";s:18:\"Гардероб -\";}', 'yes'),
(931, 'tinvwl-style', 'a:1:{s:11:\"customstyle\";b:1;}', 'yes'),
(932, 'tinvwl-style_options', 'a:86:{s:32:\"bda8150264b84aeff0ca47779ebacc00\";s:7:\"#000000\";s:32:\"2d1414f360ef96f3bdd718878b838843\";s:4:\"40px\";s:32:\"0f617e666d70702b660f4a733af1cd5c\";s:7:\"#1a1a1a\";s:32:\"4578aeedef5c22d62ff52c33f9bba6cd\";s:14:\"Georgia, serif\";s:32:\"6fa639cb245131cd45e0abb36abd86ce\";s:7:\"#007acc\";s:32:\"2343474f1380ef4be1905ba43b880647\";s:7:\"#686868\";s:32:\"0b340af13bdf4cbdb056adc5f79b3373\";s:9:\"underline\";s:32:\"3fc21953166f508ee2aeeecd7aee0bd8\";s:7:\"inherit\";s:32:\"7a33c22c4c2a5c21e7c3a914f4eeb2c1\";s:7:\"#f7f7f7\";s:32:\"24179cd549f6e251373383906299b096\";s:7:\"#d1d1d1\";s:32:\"5e58bbdd0e885ca2b71116cea057e414\";s:3:\"2px\";s:32:\"c9b30b5c3e54618a46760805932ab8bf\";s:7:\"#686868\";s:32:\"6a1003e57ef5796dfcaf02fa4b8ac499\";s:7:\"inherit\";s:32:\"f7baf52c4518d3a4dfa63810320b45e2\";s:4:\"12px\";s:32:\"4bd65875dca825abc0514a1291112c54\";s:7:\"#ebe9eb\";s:32:\"33689019f13f0b0c1046f19d29903a4d\";s:7:\"#dad8da\";s:32:\"847b512c55cd043101d7f43a1bb4d4b5\";s:7:\"#515151\";s:32:\"74c1a8d1f7692f0181c50344cf2264ff\";s:7:\"#686868\";s:32:\"9e727dca77dd5482f23de230a3d5046f\";s:7:\"#007acc\";s:32:\"2ceebaa93d8a5c371551274780ddc300\";s:7:\"#686868\";s:32:\"dd67d3c367e9535414de00f3cc31a27c\";s:7:\"inherit\";s:32:\"478dbaf4932fbddfb0635a2524c109f8\";s:4:\"16px\";s:32:\"5a5c778b20bb2a740c14d622e1778927\";s:4:\"16px\";s:32:\"2722c6d783fbafcc0e95b883fbb37c49\";s:3:\"3px\";s:32:\"f85914d097d48e54807e63d6c78c7baa\";s:7:\"#ebe9eb\";s:32:\"83f51e488b095274c16b7c6461a7abff\";s:7:\"#dad8da\";s:32:\"d01dd21a426a506041ea10a9ca85c5f6\";s:7:\"#515151\";s:32:\"ff25e1ab37010f5b99f58b64dd804803\";s:7:\"#515151\";s:32:\"2b1fbec4726635eb00e802d02f6e2e7d\";s:7:\"#007acc\";s:32:\"048f149cf361ad9d0e085d4af382752b\";s:7:\"#686868\";s:32:\"7ea7dcad50af22b4ea68f4dd7e04a26e\";s:0:\"\";s:32:\"01f066844ed4e57f9ee7b8b03b2f18a7\";s:4:\"16px\";s:32:\"f3e46faa2e790800a881e6893cf2171e\";s:4:\"16px\";s:32:\"55c6b0597f0d341f15a33c368ab1e802\";s:3:\"3px\";s:32:\"27471cca2b825cfe2c307f7deb5c0f8b\";s:7:\"#ebe9eb\";s:32:\"1be5052ff24892ae6eecd8ee532be013\";s:7:\"#dad8da\";s:32:\"dcbaebb7da7109f1c0adc717b6971cd7\";s:7:\"#515151\";s:32:\"38acbc6d0ea4887e80a909b8078335a2\";s:7:\"#515151\";s:32:\"21a1f368854745ed15acea4fee3fd90a\";s:7:\"inherit\";s:32:\"a76bde0573bc009de0ff353bbac66def\";s:4:\"14px\";s:32:\"a095a25b82f1c317456ed3d5bf7bd721\";s:3:\"3px\";s:32:\"e8453b34b3ad8e5c51a4b1a8c83da979\";s:7:\"#a46497\";s:32:\"864bd3ccc745240992706b97c1599d1e\";s:7:\"#935386\";s:32:\"40bbed4ee296fd5fef264a1cc77670bf\";s:7:\"#ffffff\";s:32:\"831efa14fec3cf383879f988d32c5a15\";s:7:\"#ffffff\";s:32:\"fda80ea49f05a78edb5076e814f69292\";s:7:\"inherit\";s:32:\"3d4036f21baef6625cc3a323ee6e1e66\";s:4:\"14px\";s:32:\"672007341cff762ddf0d2281bdc0c299\";s:3:\"3px\";s:32:\"e4d5d4ef67c7ca30722286d58b2650f6\";s:7:\"#ffffff\";s:32:\"0b792e66d13026e6a122323222804c15\";s:7:\"#d1d1d1\";s:32:\"608484016590a0721da236a1885f9ade\";s:7:\"#ffffff\";s:32:\"69ae1bd0ebe53785d2b0e348b27b37dc\";s:7:\"#1a1a1a\";s:32:\"ecec585e20f406fc1ff002adfcbe9a9c\";s:7:\"inherit\";s:32:\"17742a42a30f7be0736d1e79908bc616\";s:4:\"14px\";s:32:\"aaf74cc43a3736ed621e17f1d7337965\";s:7:\"#686868\";s:32:\"e1b605ab84ede698ee5b65bdc097c550\";s:7:\"inherit\";s:32:\"59a1d057b2b45324f63ce597f36946e2\";s:4:\"14px\";s:32:\"921f8d28a66e0dbff0d74713a842c219\";s:7:\"#202020\";s:32:\"a2dabc4fdcada89b2e391e2edd7268a5\";s:7:\"inherit\";s:32:\"115256bbd72f9182797db3f54dce8f8b\";s:4:\"16px\";s:32:\"d1241272421242eb22011f3518a76da3\";s:7:\"#ffffff\";s:32:\"711de9e4983ba0e64439cec8d257fec0\";s:7:\"#007acc\";s:32:\"2c83e1eeed3250bf738550e6d52f2203\";s:7:\"#ebe9eb\";s:32:\"6eac9d09c71c87fd720963bcfc4ff973\";s:7:\"#dad8da\";s:32:\"3c6b408b1f5e325ef9b98202c693a4ec\";s:7:\"#000000\";s:32:\"f2a3a843e98eb989d8c9355f149b2b5e\";s:7:\"#686868\";s:32:\"8000e1c3ff927792052adcbaf10a57b6\";s:4:\"20px\";s:32:\"25ad1cb265d6a6f1798c03ea5e2380cc\";s:7:\"#ffffff\";s:32:\"e20fe57c88d077539db0740fbc54d95e\";s:7:\"#1a1a1a\";s:32:\"f74bce5a7e9a892248f5a1459acb2318\";s:0:\"\";s:32:\"70d5eda2690ddb4aaed81ee52db962fb\";s:4:\"16px\";s:32:\"89c5f801f41e34d0d01bf1c592f4642a\";s:7:\"#ebe9eb\";s:32:\"8256242f5a6cd2296de8ab945a060a2b\";s:7:\"#dad8da\";s:32:\"9da70190e482bf08f104dcb8280efabb\";s:7:\"#515151\";s:32:\"d7348c4c1bb37670a9d9a0202d2f056d\";s:7:\"#515151\";s:32:\"7139fa8307c4a65b7591e6dde6ae937f\";s:7:\"#ebe9eb\";s:32:\"cb88e51ff78f5d530ccf0bb719ec9c76\";s:7:\"#dad8da\";s:32:\"da0b548056da720976cf4f7f3cb5018c\";s:7:\"#515151\";s:32:\"c986c70cad5bc5f474abffea5b94ff7e\";s:7:\"#686868\";s:32:\"68940071b9fb40e3f336b7886b6f1d1b\";s:3:\"3px\";s:32:\"d5d75eab7561efa79cb1fefccd01f8a4\";s:7:\"#ebe9eb\";s:32:\"73a7a9d026401b62bf1cfe5d2a833b7e\";s:7:\"#dad8da\";s:32:\"939c7f00b7b90a16f80fd6312a4a0e0d\";s:7:\"#515151\";s:32:\"fa4f419f1114d3867a9630275364d086\";s:7:\"#515151\";s:32:\"321da9c8bc66c286a29c544fc14e3f9e\";s:0:\"\";s:32:\"8d6c42fb581f8511d2affa83aa9ef81f\";s:3:\"3px\";}', 'yes'),
(933, 'tinvwl-style_plain', 'a:2:{s:5:\"allow\";b:0;s:3:\"css\";s:0:\"\";}', 'yes'),
(936, 'tinvwl_ver', '1.8.9', 'yes'),
(937, 'ti_admin_notice_rating', 'a:1:{i:0;s:323:\"<p>Прошел месяц с тех пор, как первый список пожеланий был создан с помощью плагина <strong> Wishlist WooCommerce </strong>!</p><p>Что вы думаете о нашем плагине?</p><p>Поделитесь своей любовью с нами.</p>\";}', 'yes'),
(938, 'ti_admin_notice_trigger_rating', 'a:1:{s:32:\"74950159917226a697f42cf6865056cb\";a:4:{i:0;s:10:\"admin_init\";i:1;s:31:\"tinvwl_rating_notice_trigger_30\";i:2;i:10;i:3;i:1;}}', 'yes'),
(939, 'tinvwl_utm_source', 'wordpress_org', 'yes'),
(940, 'tinvwl_utm_medium', 'organic', 'yes'),
(941, 'tinvwl_utm_campaign', 'organic', 'yes'),
(942, 'widget_widget_top_wishlist', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(944, 'ti_admin_shownotices', 'a:1:{s:6:\"rating\";a:0:{}}', 'yes'),
(945, 'ti_admin_notices', 'a:1:{i:0;s:6:\"rating\";}', 'yes'),
(946, 'tinvwl_wizard', '1', 'yes'),
(1028, 'woocommerce_paybox_settings', 'a:9:{s:7:\"enabled\";s:3:\"yes\";s:5:\"title\";s:6:\"PayBox\";s:11:\"description\";s:0:\"\";s:8:\"testmode\";s:3:\"yes\";s:11:\"merchant_id\";s:6:\"508398\";s:12:\"merchant_key\";s:16:\"MCnFrW1rmpFtTDfh\";s:11:\"pass_phrase\";s:0:\"\";s:16:\"send_debug_email\";s:0:\"\";s:14:\"enable_logging\";s:0:\"\";}', 'yes'),
(1035, 'wcj_admin_tools_enabled', 'no', 'yes'),
(1036, 'wcj_price_labels_enabled', 'no', 'yes'),
(1037, 'wcj_call_for_price_enabled', 'no', 'yes'),
(1038, 'wcj_free_price_enabled', 'no', 'yes'),
(1039, 'wcj_product_listings_enabled', 'no', 'yes'),
(1040, 'wcj_tax_display_enabled', 'no', 'yes'),
(1041, 'wcj_admin_products_list_enabled', 'no', 'yes'),
(1042, 'wcj_products_per_page_enabled', 'no', 'yes'),
(1043, 'wcj_sorting_enabled', 'no', 'yes'),
(1044, 'wcj_product_custom_info_enabled', 'no', 'yes'),
(1045, 'wcj_product_info_enabled', 'no', 'yes'),
(1046, 'wcj_product_add_to_cart_enabled', 'no', 'yes'),
(1047, 'wcj_add_to_cart_button_visibility_enabled', 'no', 'yes'),
(1048, 'wcj_related_products_enabled', 'yes', 'yes'),
(1049, 'wcj_cross_sells_enabled', 'no', 'yes'),
(1050, 'wcj_upsells_enabled', 'no', 'yes'),
(1051, 'wcj_sku_enabled', 'no', 'yes'),
(1052, 'wcj_stock_enabled', 'no', 'yes'),
(1053, 'wcj_product_tabs_enabled', 'no', 'yes'),
(1054, 'wcj_product_input_fields_enabled', 'no', 'yes'),
(1055, 'wcj_bulk_price_converter_enabled', 'no', 'yes'),
(1056, 'wcj_product_bulk_meta_editor_enabled', 'no', 'yes'),
(1057, 'wcj_purchase_data_enabled', 'no', 'yes'),
(1058, 'wcj_product_bookings_enabled', 'no', 'yes'),
(1059, 'wcj_crowdfunding_enabled', 'no', 'yes'),
(1060, 'wcj_product_addons_enabled', 'no', 'yes'),
(1061, 'wcj_wholesale_price_enabled', 'no', 'yes'),
(1062, 'wcj_product_open_pricing_enabled', 'no', 'yes'),
(1063, 'wcj_product_msrp_enabled', 'no', 'yes'),
(1064, 'wcj_offer_price_enabled', 'no', 'yes'),
(1065, 'wcj_price_by_user_role_enabled', 'no', 'yes'),
(1066, 'wcj_global_discount_enabled', 'no', 'yes'),
(1067, 'wcj_product_price_by_formula_enabled', 'no', 'yes'),
(1068, 'wcj_product_images_enabled', 'no', 'yes'),
(1069, 'wcj_sale_flash_enabled', 'yes', 'yes'),
(1070, 'wcj_product_by_country_enabled', 'no', 'yes'),
(1071, 'wcj_product_custom_visibility_enabled', 'no', 'yes'),
(1072, 'wcj_product_by_time_enabled', 'no', 'yes'),
(1073, 'wcj_product_by_date_enabled', 'no', 'yes'),
(1074, 'wcj_product_by_user_role_enabled', 'no', 'yes'),
(1075, 'wcj_product_by_user_enabled', 'no', 'yes'),
(1076, 'wcj_add_to_cart_enabled', 'no', 'yes'),
(1077, 'wcj_more_button_labels_enabled', 'no', 'yes'),
(1078, 'wcj_cart_enabled', 'no', 'yes'),
(1079, 'wcj_cart_customization_enabled', 'no', 'yes'),
(1080, 'wcj_empty_cart_enabled', 'no', 'yes'),
(1081, 'wcj_mini_cart_enabled', 'no', 'yes'),
(1082, 'wcj_checkout_core_fields_enabled', 'yes', 'yes'),
(1083, 'wcj_checkout_custom_fields_enabled', 'no', 'yes'),
(1084, 'wcj_checkout_files_upload_enabled', 'no', 'yes'),
(1085, 'wcj_checkout_custom_info_enabled', 'no', 'yes'),
(1086, 'wcj_checkout_customization_enabled', 'no', 'yes'),
(1087, 'wcj_checkout_fees_enabled', 'no', 'yes'),
(1088, 'wcj_payment_gateways_enabled', 'no', 'yes'),
(1089, 'wcj_payment_gateways_icons_enabled', 'no', 'yes'),
(1090, 'wcj_payment_gateways_fees_enabled', 'no', 'yes'),
(1091, 'wcj_payment_gateways_per_category_enabled', 'no', 'yes'),
(1092, 'wcj_payment_gateways_currency_enabled', 'no', 'yes'),
(1093, 'wcj_payment_gateways_by_currency_enabled', 'no', 'yes'),
(1094, 'wcj_payment_gateways_min_max_enabled', 'no', 'yes'),
(1095, 'wcj_payment_gateways_by_country_enabled', 'no', 'yes'),
(1096, 'wcj_payment_gateways_by_user_role_enabled', 'no', 'yes'),
(1097, 'wcj_payment_gateways_by_shipping_enabled', 'no', 'yes'),
(1098, 'wcj_shipping_enabled', 'no', 'yes'),
(1099, 'wcj_shipping_options_enabled', 'no', 'yes'),
(1100, 'wcj_shipping_icons_enabled', 'no', 'yes'),
(1101, 'wcj_shipping_description_enabled', 'no', 'yes'),
(1102, 'wcj_shipping_time_enabled', 'no', 'yes'),
(1103, 'wcj_left_to_free_shipping_enabled', 'no', 'yes'),
(1104, 'wcj_shipping_calculator_enabled', 'no', 'yes'),
(1105, 'wcj_shipping_by_user_role_enabled', 'no', 'yes'),
(1106, 'wcj_shipping_by_products_enabled', 'no', 'yes'),
(1107, 'wcj_shipping_by_cities_enabled', 'no', 'yes'),
(1108, 'wcj_shipping_by_order_amount_enabled', 'no', 'yes'),
(1109, 'wcj_address_formats_enabled', 'no', 'yes'),
(1110, 'wcj_orders_enabled', 'no', 'yes'),
(1111, 'wcj_admin_orders_list_enabled', 'no', 'yes'),
(1112, 'wcj_order_min_amount_enabled', 'no', 'yes'),
(1113, 'wcj_order_numbers_enabled', 'no', 'yes'),
(1114, 'wcj_order_custom_statuses_enabled', 'no', 'yes'),
(1115, 'wcj_order_quantities_enabled', 'no', 'yes'),
(1116, 'wcj_max_products_per_user_enabled', 'no', 'yes'),
(1117, 'wcj_pdf_invoicing_enabled', 'no', 'yes'),
(1118, 'wcj_emails_enabled', 'no', 'yes'),
(1119, 'wcj_email_options_enabled', 'no', 'yes'),
(1120, 'wcj_emails_verification_enabled', 'no', 'yes'),
(1121, 'wcj_currency_enabled', 'no', 'yes'),
(1122, 'wcj_multicurrency_enabled', 'no', 'yes'),
(1123, 'wcj_multicurrency_base_price_enabled', 'no', 'yes'),
(1124, 'wcj_currency_per_product_enabled', 'no', 'yes'),
(1125, 'wcj_currency_external_products_enabled', 'no', 'yes'),
(1126, 'wcj_price_by_country_enabled', 'yes', 'yes'),
(1127, 'wcj_currency_exchange_rates_enabled', 'no', 'yes'),
(1128, 'wcj_price_formats_enabled', 'no', 'yes'),
(1129, 'wcj_general_enabled', 'no', 'yes'),
(1130, 'wcj_track_users_enabled', 'no', 'yes'),
(1131, 'wcj_breadcrumbs_enabled', 'no', 'yes'),
(1132, 'wcj_url_coupons_enabled', 'no', 'yes'),
(1133, 'wcj_coupon_code_generator_enabled', 'no', 'yes'),
(1134, 'wcj_coupon_by_user_role_enabled', 'no', 'yes'),
(1135, 'wcj_admin_bar_enabled', 'no', 'yes'),
(1136, 'wcj_my_account_enabled', 'no', 'yes'),
(1137, 'wcj_custom_css_enabled', 'no', 'yes'),
(1138, 'wcj_custom_js_enabled', 'no', 'yes'),
(1139, 'wcj_products_xml_enabled', 'no', 'yes'),
(1140, 'wcj_export_enabled', 'no', 'yes'),
(1141, 'wcj_eu_vat_number_enabled', 'no', 'yes'),
(1142, 'wcj_old_slugs_enabled', 'no', 'yes'),
(1143, 'wcj_reports_enabled', 'no', 'yes'),
(1144, 'wcj_wpml_enabled', 'no', 'yes'),
(1145, 'wcj_modules_by_user_roles_enabled', 'no', 'yes'),
(1146, 'booster_for_woocommerce_version', '3.8.0', 'yes'),
(1147, 'widget_wcj_widget_multicurrency', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(1148, 'widget_wcj_widget_country_switcher', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:9:\"countries\";s:14:\"KZ, RU, BY, UA\";s:21:\"replace_with_currency\";s:2:\"no\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(1149, 'widget_wcj_widget_left_to_free_shipping', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(1150, 'widget_wcj_widget_selector', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(1151, 'wcj_download_tcpdf_fonts_hook_timestamp', '1558361635', 'yes'),
(1152, 'wcj_invoicing_fonts_version_timestamp', '1558361636', 'yes'),
(1153, 'wcj_invoicing_fonts_version', '2.9.0', 'yes'),
(1154, 'wcj_product_images_sale_flash_enabled', 'yes', 'yes'),
(1155, 'wcj_product_images_sale_flash_html', '<span class=\"onsale\">Скидка!</span>', 'yes'),
(1156, 'wcj_product_images_sale_flash_hide_everywhere', 'no', 'yes'),
(1157, 'wcj_product_images_sale_flash_hide_on_archives', 'no', 'yes'),
(1158, 'wcj_product_images_sale_flash_hide_on_single', 'no', 'yes'),
(1159, 'wcj_sale_flash_per_product_enabled', 'no', 'yes'),
(1160, 'wcj_sale_flash_per_product_cat_enabled', 'no', 'yes'),
(1161, 'wcj_sale_flash_per_product_cat_17_html', '<span class=\"onsale\">Распродажа!</span>', 'yes'),
(1162, 'wcj_sale_flash_per_product_cat_20_html', '<span class=\"onsale\">Распродажа!</span>', 'yes'),
(1163, 'wcj_sale_flash_per_product_cat_31_html', '<span class=\"onsale\">Распродажа!</span>', 'yes'),
(1164, 'wcj_sale_flash_per_product_cat_26_html', '<span class=\"onsale\">Распродажа!</span>', 'yes'),
(1165, 'wcj_sale_flash_per_product_cat_21_html', '<span class=\"onsale\">Распродажа!</span>', 'yes'),
(1166, 'wcj_sale_flash_per_product_cat_23_html', '<span class=\"onsale\">Распродажа!</span>', 'yes'),
(1167, 'wcj_sale_flash_per_product_cat_25_html', '<span class=\"onsale\">Распродажа!</span>', 'yes'),
(1168, 'wcj_sale_flash_per_product_cat_37_html', '<span class=\"onsale\">Распродажа!</span>', 'yes'),
(1169, 'wcj_sale_flash_per_product_cat_28_html', '<span class=\"onsale\">Распродажа!</span>', 'yes'),
(1170, 'wcj_sale_flash_per_product_cat_19_html', '<span class=\"onsale\">Распродажа!</span>', 'yes'),
(1171, 'wcj_sale_flash_per_product_cat_18_html', '<span class=\"onsale\">Распродажа!</span>', 'yes'),
(1172, 'wcj_sale_flash_per_product_cat_32_html', '<span class=\"onsale\">Распродажа!</span>', 'yes'),
(1173, 'wcj_sale_flash_per_product_cat_24_html', '<span class=\"onsale\">Распродажа!</span>', 'yes'),
(1174, 'wcj_sale_flash_per_product_cat_33_html', '<span class=\"onsale\">Распродажа!</span>', 'yes'),
(1175, 'wcj_sale_flash_per_product_cat_22_html', '<span class=\"onsale\">Распродажа!</span>', 'yes'),
(1176, 'wcj_sale_flash_per_product_cat_29_html', '<span class=\"onsale\">Распродажа!</span>', 'yes'),
(1177, 'wcj_sale_flash_per_product_cat_27_html', '<span class=\"onsale\">Распродажа!</span>', 'yes'),
(1178, 'wcj_sale_flash_per_product_cat_30_html', '<span class=\"onsale\">Распродажа!</span>', 'yes'),
(1179, 'wcj_sale_flash_per_product_tag_enabled', 'no', 'yes'),
(1257, 'wc_gateway_ppce_prompt_to_connect', 'PayPal Checkout is almost ready. To get started, <a href=\"http://bodyfest.kz/wp-admin/admin.php?page=wc-settings&#038;tab=checkout&#038;section=ppec_paypal\">connect your PayPal account</a>.', 'yes'),
(1258, 'woocommerce_gateway_order', 'a:6:{s:4:\"bacs\";i:0;s:6:\"cheque\";i:1;s:3:\"cod\";i:2;s:6:\"paypal\";i:3;s:6:\"paybox\";i:4;s:11:\"ppec_paypal\";i:5;}', 'yes'),
(1530, 'woocommerce_version', '3.4.4', 'yes'),
(1531, 'woocommerce_db_version', '3.4.4', 'yes'),
(1534, 'wcj_gateways_fees_text', 'a:0:{}', 'yes'),
(1535, 'wcj_gateways_fees_type', 'a:0:{}', 'yes'),
(1536, 'wcj_gateways_fees_value', 'a:0:{}', 'yes'),
(1537, 'wcj_gateways_fees_min_cart_amount', 'a:0:{}', 'yes'),
(1538, 'wcj_gateways_fees_max_cart_amount', 'a:0:{}', 'yes'),
(1539, 'wcj_gateways_fees_round', 'a:0:{}', 'yes'),
(1540, 'wcj_gateways_fees_round_precision', 'a:0:{}', 'yes'),
(1541, 'wcj_gateways_fees_is_taxable', 'a:0:{}', 'yes'),
(1542, 'wcj_gateways_fees_tax_class_id', 'a:0:{}', 'yes'),
(1543, 'wcj_gateways_fees_exclude_shipping', 'a:0:{}', 'yes'),
(1585, 'wcj_coupon_by_user_role_disabled', 'a:8:{i:0;s:5:\"guest\";i:1;s:13:\"administrator\";i:2;s:6:\"editor\";i:3;s:6:\"author\";i:4;s:11:\"contributor\";i:5;s:10:\"subscriber\";i:6;s:8:\"customer\";i:7;s:12:\"shop_manager\";}', 'yes'),
(1586, 'wcj_coupon_by_user_role_invalid', 'a:0:{}', 'yes'),
(1587, 'wcj_coupon_by_user_role_invalid_per_coupon', 'no', 'yes'),
(1588, 'wcj_coupon_by_user_role_invalid_message', 'Coupon is not valid for your user role.', 'yes'),
(1663, 'wcj_product_info_related_products_hide_products_incl', 'a:0:{}', 'yes'),
(1664, 'wcj_product_info_related_products_hide_products_excl', 'a:0:{}', 'yes'),
(1665, 'wcj_product_info_related_products_num', '3', 'yes'),
(1666, 'wcj_product_info_related_products_columns', '3', 'yes'),
(1667, 'wcj_product_info_related_products_orderby', 'rand', 'yes'),
(1668, 'wcj_product_info_related_products_order', 'desc', 'yes'),
(1669, 'wcj_product_info_related_products_relate_by_category', 'yes', 'yes'),
(1670, 'wcj_product_info_related_products_relate_by_tag', 'yes', 'yes'),
(1671, 'wcj_product_info_related_products_by_attribute_enabled', 'no', 'yes'),
(1672, 'wcj_product_info_related_products_by_attribute_attribute_type', 'global', 'yes'),
(1673, 'wcj_product_info_related_products_by_attribute_attribute_name', '', 'yes'),
(1674, 'wcj_product_info_related_products_by_attribute_attribute_value', '', 'yes'),
(1675, 'wcj_product_info_related_products_per_product', 'no', 'yes'),
(1676, 'wcj_product_info_related_products_hide', 'no', 'yes'),
(1677, 'wcj_product_info_related_products_hide_cats_incl', 'a:0:{}', 'yes'),
(1678, 'wcj_product_info_related_products_hide_cats_excl', 'a:0:{}', 'yes'),
(1679, 'wcj_product_info_related_products_hide_tags_incl', 'a:0:{}', 'yes'),
(1680, 'wcj_product_info_related_products_hide_tags_excl', 'a:0:{}', 'yes'),
(1688, 'wcj_checkout_core_fields_override_default_address_fields', 'billing', 'yes'),
(1689, 'wcj_checkout_core_fields_override_country_locale_fields', 'billing', 'yes'),
(1690, 'wcj_checkout_core_fields_force_sort_by_priority', 'no', 'yes'),
(1691, 'wcj_checkout_fields_billing_country_is_enabled', 'default', 'yes'),
(1692, 'wcj_checkout_fields_billing_country_is_required', 'default', 'yes'),
(1693, 'wcj_checkout_fields_billing_country_label', '', 'yes'),
(1694, 'wcj_checkout_fields_billing_country_placeholder', '', 'yes'),
(1695, 'wcj_checkout_fields_billing_country_description', '', 'yes'),
(1696, 'wcj_checkout_fields_billing_country_class', 'default', 'yes'),
(1697, 'wcj_checkout_fields_billing_country_priority', '0', 'yes'),
(1698, 'wcj_checkout_fields_billing_country_cats_incl', 'a:0:{}', 'yes'),
(1699, 'wcj_checkout_fields_billing_country_cats_excl', 'a:0:{}', 'yes'),
(1700, 'wcj_checkout_fields_billing_first_name_is_enabled', 'default', 'yes'),
(1701, 'wcj_checkout_fields_billing_first_name_is_required', 'default', 'yes'),
(1702, 'wcj_checkout_fields_billing_first_name_label', 'ФИО', 'yes'),
(1703, 'wcj_checkout_fields_billing_first_name_placeholder', '', 'yes'),
(1704, 'wcj_checkout_fields_billing_first_name_description', '', 'yes'),
(1705, 'wcj_checkout_fields_billing_first_name_class', 'default', 'yes'),
(1706, 'wcj_checkout_fields_billing_first_name_priority', '0', 'yes'),
(1707, 'wcj_checkout_fields_billing_first_name_cats_incl', 'a:0:{}', 'yes'),
(1708, 'wcj_checkout_fields_billing_first_name_cats_excl', 'a:0:{}', 'yes'),
(1709, 'wcj_checkout_fields_billing_last_name_is_enabled', 'no', 'yes'),
(1710, 'wcj_checkout_fields_billing_last_name_is_required', 'default', 'yes'),
(1711, 'wcj_checkout_fields_billing_last_name_label', '', 'yes'),
(1712, 'wcj_checkout_fields_billing_last_name_placeholder', '', 'yes'),
(1713, 'wcj_checkout_fields_billing_last_name_description', '', 'yes'),
(1714, 'wcj_checkout_fields_billing_last_name_class', 'default', 'yes'),
(1715, 'wcj_checkout_fields_billing_last_name_priority', '0', 'yes'),
(1716, 'wcj_checkout_fields_billing_last_name_cats_incl', 'a:0:{}', 'yes'),
(1717, 'wcj_checkout_fields_billing_last_name_cats_excl', 'a:0:{}', 'yes'),
(1718, 'wcj_checkout_fields_billing_company_is_enabled', 'no', 'yes'),
(1719, 'wcj_checkout_fields_billing_company_is_required', 'default', 'yes'),
(1720, 'wcj_checkout_fields_billing_company_label', '', 'yes'),
(1721, 'wcj_checkout_fields_billing_company_placeholder', '', 'yes'),
(1722, 'wcj_checkout_fields_billing_company_description', '', 'yes'),
(1723, 'wcj_checkout_fields_billing_company_class', 'default', 'yes'),
(1724, 'wcj_checkout_fields_billing_company_priority', '0', 'yes'),
(1725, 'wcj_checkout_fields_billing_company_cats_incl', 'a:0:{}', 'yes'),
(1726, 'wcj_checkout_fields_billing_company_cats_excl', 'a:0:{}', 'yes'),
(1727, 'wcj_checkout_fields_billing_address_1_is_enabled', 'default', 'yes'),
(1728, 'wcj_checkout_fields_billing_address_1_is_required', 'default', 'yes'),
(1729, 'wcj_checkout_fields_billing_address_1_label', '', 'yes'),
(1730, 'wcj_checkout_fields_billing_address_1_placeholder', '', 'yes'),
(1731, 'wcj_checkout_fields_billing_address_1_description', '', 'yes'),
(1732, 'wcj_checkout_fields_billing_address_1_class', 'default', 'yes'),
(1733, 'wcj_checkout_fields_billing_address_1_priority', '0', 'yes'),
(1734, 'wcj_checkout_fields_billing_address_1_cats_incl', 'a:0:{}', 'yes'),
(1735, 'wcj_checkout_fields_billing_address_1_cats_excl', 'a:0:{}', 'yes'),
(1736, 'wcj_checkout_fields_billing_address_2_is_enabled', 'no', 'yes'),
(1737, 'wcj_checkout_fields_billing_address_2_is_required', 'default', 'yes'),
(1738, 'wcj_checkout_fields_billing_address_2_label', '', 'yes'),
(1739, 'wcj_checkout_fields_billing_address_2_placeholder', '', 'yes'),
(1740, 'wcj_checkout_fields_billing_address_2_description', '', 'yes'),
(1741, 'wcj_checkout_fields_billing_address_2_class', 'default', 'yes'),
(1742, 'wcj_checkout_fields_billing_address_2_priority', '0', 'yes'),
(1743, 'wcj_checkout_fields_billing_address_2_cats_incl', 'a:0:{}', 'yes'),
(1744, 'wcj_checkout_fields_billing_address_2_cats_excl', 'a:0:{}', 'yes'),
(1745, 'wcj_checkout_fields_billing_city_is_enabled', 'default', 'yes'),
(1746, 'wcj_checkout_fields_billing_city_is_required', 'default', 'yes'),
(1747, 'wcj_checkout_fields_billing_city_label', '', 'yes'),
(1748, 'wcj_checkout_fields_billing_city_placeholder', '', 'yes'),
(1749, 'wcj_checkout_fields_billing_city_description', '', 'yes'),
(1750, 'wcj_checkout_fields_billing_city_class', 'default', 'yes'),
(1751, 'wcj_checkout_fields_billing_city_priority', '0', 'yes'),
(1752, 'wcj_checkout_fields_billing_city_cats_incl', 'a:0:{}', 'yes'),
(1753, 'wcj_checkout_fields_billing_city_cats_excl', 'a:0:{}', 'yes'),
(1754, 'wcj_checkout_fields_billing_state_is_enabled', 'no', 'yes'),
(1755, 'wcj_checkout_fields_billing_state_is_required', 'default', 'yes'),
(1756, 'wcj_checkout_fields_billing_state_label', '', 'yes'),
(1757, 'wcj_checkout_fields_billing_state_placeholder', '', 'yes'),
(1758, 'wcj_checkout_fields_billing_state_description', '', 'yes'),
(1759, 'wcj_checkout_fields_billing_state_class', 'default', 'yes'),
(1760, 'wcj_checkout_fields_billing_state_priority', '0', 'yes'),
(1761, 'wcj_checkout_fields_billing_state_cats_incl', 'a:0:{}', 'yes'),
(1762, 'wcj_checkout_fields_billing_state_cats_excl', 'a:0:{}', 'yes'),
(1763, 'wcj_checkout_fields_billing_postcode_is_enabled', 'no', 'yes'),
(1764, 'wcj_checkout_fields_billing_postcode_is_required', 'default', 'yes'),
(1765, 'wcj_checkout_fields_billing_postcode_label', '', 'yes'),
(1766, 'wcj_checkout_fields_billing_postcode_placeholder', '', 'yes'),
(1767, 'wcj_checkout_fields_billing_postcode_description', '', 'yes'),
(1768, 'wcj_checkout_fields_billing_postcode_class', 'default', 'yes'),
(1769, 'wcj_checkout_fields_billing_postcode_priority', '0', 'yes'),
(1770, 'wcj_checkout_fields_billing_postcode_cats_incl', 'a:0:{}', 'yes'),
(1771, 'wcj_checkout_fields_billing_postcode_cats_excl', 'a:0:{}', 'yes'),
(1772, 'wcj_checkout_fields_billing_email_is_enabled', 'default', 'yes'),
(1773, 'wcj_checkout_fields_billing_email_is_required', 'default', 'yes'),
(1774, 'wcj_checkout_fields_billing_email_label', '', 'yes'),
(1775, 'wcj_checkout_fields_billing_email_placeholder', '', 'yes'),
(1776, 'wcj_checkout_fields_billing_email_description', '', 'yes'),
(1777, 'wcj_checkout_fields_billing_email_class', 'default', 'yes'),
(1778, 'wcj_checkout_fields_billing_email_priority', '0', 'yes'),
(1779, 'wcj_checkout_fields_billing_email_cats_incl', 'a:0:{}', 'yes'),
(1780, 'wcj_checkout_fields_billing_email_cats_excl', 'a:0:{}', 'yes'),
(1781, 'wcj_checkout_fields_billing_phone_is_enabled', 'default', 'yes'),
(1782, 'wcj_checkout_fields_billing_phone_is_required', 'default', 'yes'),
(1783, 'wcj_checkout_fields_billing_phone_label', '', 'yes'),
(1784, 'wcj_checkout_fields_billing_phone_placeholder', '', 'yes'),
(1785, 'wcj_checkout_fields_billing_phone_description', '', 'yes'),
(1786, 'wcj_checkout_fields_billing_phone_class', 'default', 'yes'),
(1787, 'wcj_checkout_fields_billing_phone_priority', '0', 'yes'),
(1788, 'wcj_checkout_fields_billing_phone_cats_incl', 'a:0:{}', 'yes'),
(1789, 'wcj_checkout_fields_billing_phone_cats_excl', 'a:0:{}', 'yes'),
(1790, 'wcj_checkout_fields_shipping_country_is_enabled', 'default', 'yes'),
(1791, 'wcj_checkout_fields_shipping_country_is_required', 'default', 'yes'),
(1792, 'wcj_checkout_fields_shipping_country_label', '', 'yes'),
(1793, 'wcj_checkout_fields_shipping_country_placeholder', '', 'yes'),
(1794, 'wcj_checkout_fields_shipping_country_description', '', 'yes'),
(1795, 'wcj_checkout_fields_shipping_country_class', 'default', 'yes'),
(1796, 'wcj_checkout_fields_shipping_country_priority', '0', 'yes'),
(1797, 'wcj_checkout_fields_shipping_country_cats_incl', 'a:0:{}', 'yes'),
(1798, 'wcj_checkout_fields_shipping_country_cats_excl', 'a:0:{}', 'yes'),
(1799, 'wcj_checkout_fields_shipping_first_name_is_enabled', 'default', 'yes'),
(1800, 'wcj_checkout_fields_shipping_first_name_is_required', 'default', 'yes'),
(1801, 'wcj_checkout_fields_shipping_first_name_label', '', 'yes'),
(1802, 'wcj_checkout_fields_shipping_first_name_placeholder', '', 'yes'),
(1803, 'wcj_checkout_fields_shipping_first_name_description', '', 'yes'),
(1804, 'wcj_checkout_fields_shipping_first_name_class', 'default', 'yes'),
(1805, 'wcj_checkout_fields_shipping_first_name_priority', '0', 'yes'),
(1806, 'wcj_checkout_fields_shipping_first_name_cats_incl', 'a:0:{}', 'yes'),
(1807, 'wcj_checkout_fields_shipping_first_name_cats_excl', 'a:0:{}', 'yes'),
(1808, 'wcj_checkout_fields_shipping_last_name_is_enabled', 'default', 'yes'),
(1809, 'wcj_checkout_fields_shipping_last_name_is_required', 'default', 'yes'),
(1810, 'wcj_checkout_fields_shipping_last_name_label', '', 'yes'),
(1811, 'wcj_checkout_fields_shipping_last_name_placeholder', '', 'yes'),
(1812, 'wcj_checkout_fields_shipping_last_name_description', '', 'yes'),
(1813, 'wcj_checkout_fields_shipping_last_name_class', 'default', 'yes'),
(1814, 'wcj_checkout_fields_shipping_last_name_priority', '0', 'yes'),
(1815, 'wcj_checkout_fields_shipping_last_name_cats_incl', 'a:0:{}', 'yes'),
(1816, 'wcj_checkout_fields_shipping_last_name_cats_excl', 'a:0:{}', 'yes'),
(1817, 'wcj_checkout_fields_shipping_company_is_enabled', 'default', 'yes'),
(1818, 'wcj_checkout_fields_shipping_company_is_required', 'default', 'yes'),
(1819, 'wcj_checkout_fields_shipping_company_label', '', 'yes'),
(1820, 'wcj_checkout_fields_shipping_company_placeholder', '', 'yes'),
(1821, 'wcj_checkout_fields_shipping_company_description', '', 'yes'),
(1822, 'wcj_checkout_fields_shipping_company_class', 'default', 'yes'),
(1823, 'wcj_checkout_fields_shipping_company_priority', '0', 'yes'),
(1824, 'wcj_checkout_fields_shipping_company_cats_incl', 'a:0:{}', 'yes'),
(1825, 'wcj_checkout_fields_shipping_company_cats_excl', 'a:0:{}', 'yes'),
(1826, 'wcj_checkout_fields_shipping_address_1_is_enabled', 'default', 'yes'),
(1827, 'wcj_checkout_fields_shipping_address_1_is_required', 'default', 'yes'),
(1828, 'wcj_checkout_fields_shipping_address_1_label', '', 'yes'),
(1829, 'wcj_checkout_fields_shipping_address_1_placeholder', '', 'yes'),
(1830, 'wcj_checkout_fields_shipping_address_1_description', '', 'yes'),
(1831, 'wcj_checkout_fields_shipping_address_1_class', 'default', 'yes'),
(1832, 'wcj_checkout_fields_shipping_address_1_priority', '0', 'yes'),
(1833, 'wcj_checkout_fields_shipping_address_1_cats_incl', 'a:0:{}', 'yes'),
(1834, 'wcj_checkout_fields_shipping_address_1_cats_excl', 'a:0:{}', 'yes'),
(1835, 'wcj_checkout_fields_shipping_address_2_is_enabled', 'default', 'yes'),
(1836, 'wcj_checkout_fields_shipping_address_2_is_required', 'default', 'yes'),
(1837, 'wcj_checkout_fields_shipping_address_2_label', '', 'yes'),
(1838, 'wcj_checkout_fields_shipping_address_2_placeholder', '', 'yes'),
(1839, 'wcj_checkout_fields_shipping_address_2_description', '', 'yes'),
(1840, 'wcj_checkout_fields_shipping_address_2_class', 'default', 'yes'),
(1841, 'wcj_checkout_fields_shipping_address_2_priority', '0', 'yes'),
(1842, 'wcj_checkout_fields_shipping_address_2_cats_incl', 'a:0:{}', 'yes'),
(1843, 'wcj_checkout_fields_shipping_address_2_cats_excl', 'a:0:{}', 'yes'),
(1844, 'wcj_checkout_fields_shipping_city_is_enabled', 'default', 'yes'),
(1845, 'wcj_checkout_fields_shipping_city_is_required', 'default', 'yes'),
(1846, 'wcj_checkout_fields_shipping_city_label', '', 'yes'),
(1847, 'wcj_checkout_fields_shipping_city_placeholder', '', 'yes'),
(1848, 'wcj_checkout_fields_shipping_city_description', '', 'yes'),
(1849, 'wcj_checkout_fields_shipping_city_class', 'default', 'yes'),
(1850, 'wcj_checkout_fields_shipping_city_priority', '0', 'yes'),
(1851, 'wcj_checkout_fields_shipping_city_cats_incl', 'a:0:{}', 'yes'),
(1852, 'wcj_checkout_fields_shipping_city_cats_excl', 'a:0:{}', 'yes'),
(1853, 'wcj_checkout_fields_shipping_state_is_enabled', 'default', 'yes'),
(1854, 'wcj_checkout_fields_shipping_state_is_required', 'default', 'yes'),
(1855, 'wcj_checkout_fields_shipping_state_label', '', 'yes'),
(1856, 'wcj_checkout_fields_shipping_state_placeholder', '', 'yes'),
(1857, 'wcj_checkout_fields_shipping_state_description', '', 'yes'),
(1858, 'wcj_checkout_fields_shipping_state_class', 'default', 'yes'),
(1859, 'wcj_checkout_fields_shipping_state_priority', '0', 'yes'),
(1860, 'wcj_checkout_fields_shipping_state_cats_incl', 'a:0:{}', 'yes'),
(1861, 'wcj_checkout_fields_shipping_state_cats_excl', 'a:0:{}', 'yes'),
(1862, 'wcj_checkout_fields_shipping_postcode_is_enabled', 'default', 'yes'),
(1863, 'wcj_checkout_fields_shipping_postcode_is_required', 'default', 'yes'),
(1864, 'wcj_checkout_fields_shipping_postcode_label', '', 'yes'),
(1865, 'wcj_checkout_fields_shipping_postcode_placeholder', '', 'yes'),
(1866, 'wcj_checkout_fields_shipping_postcode_description', '', 'yes'),
(1867, 'wcj_checkout_fields_shipping_postcode_class', 'default', 'yes'),
(1868, 'wcj_checkout_fields_shipping_postcode_priority', '0', 'yes'),
(1869, 'wcj_checkout_fields_shipping_postcode_cats_incl', 'a:0:{}', 'yes'),
(1870, 'wcj_checkout_fields_shipping_postcode_cats_excl', 'a:0:{}', 'yes'),
(1871, 'wcj_checkout_fields_account_username_is_enabled', 'default', 'yes'),
(1872, 'wcj_checkout_fields_account_username_is_required', 'default', 'yes'),
(1873, 'wcj_checkout_fields_account_username_label', '', 'yes'),
(1874, 'wcj_checkout_fields_account_username_placeholder', '', 'yes'),
(1875, 'wcj_checkout_fields_account_username_description', '', 'yes'),
(1876, 'wcj_checkout_fields_account_username_class', 'default', 'yes'),
(1877, 'wcj_checkout_fields_account_username_priority', '0', 'yes'),
(1878, 'wcj_checkout_fields_account_username_cats_incl', 'a:0:{}', 'yes'),
(1879, 'wcj_checkout_fields_account_username_cats_excl', 'a:0:{}', 'yes'),
(1880, 'wcj_checkout_fields_account_password_is_enabled', 'default', 'yes'),
(1881, 'wcj_checkout_fields_account_password_is_required', 'default', 'yes'),
(1882, 'wcj_checkout_fields_account_password_label', '', 'yes'),
(1883, 'wcj_checkout_fields_account_password_placeholder', '', 'yes'),
(1884, 'wcj_checkout_fields_account_password_description', '', 'yes'),
(1885, 'wcj_checkout_fields_account_password_class', 'default', 'yes'),
(1886, 'wcj_checkout_fields_account_password_priority', '0', 'yes'),
(1887, 'wcj_checkout_fields_account_password_cats_incl', 'a:0:{}', 'yes'),
(1888, 'wcj_checkout_fields_account_password_cats_excl', 'a:0:{}', 'yes'),
(1889, 'wcj_checkout_fields_account_password-2_is_enabled', 'default', 'yes'),
(1890, 'wcj_checkout_fields_account_password-2_is_required', 'default', 'yes'),
(1891, 'wcj_checkout_fields_account_password-2_label', '', 'yes'),
(1892, 'wcj_checkout_fields_account_password-2_placeholder', '', 'yes'),
(1893, 'wcj_checkout_fields_account_password-2_description', '', 'yes'),
(1894, 'wcj_checkout_fields_account_password-2_class', 'default', 'yes'),
(1895, 'wcj_checkout_fields_account_password-2_priority', '0', 'yes'),
(1896, 'wcj_checkout_fields_account_password-2_cats_incl', 'a:0:{}', 'yes'),
(1897, 'wcj_checkout_fields_account_password-2_cats_excl', 'a:0:{}', 'yes'),
(1898, 'wcj_checkout_fields_order_comments_is_enabled', 'default', 'yes'),
(1899, 'wcj_checkout_fields_order_comments_is_required', 'default', 'yes'),
(1900, 'wcj_checkout_fields_order_comments_label', '', 'yes'),
(1901, 'wcj_checkout_fields_order_comments_placeholder', '', 'yes'),
(1902, 'wcj_checkout_fields_order_comments_description', '', 'yes'),
(1903, 'wcj_checkout_fields_order_comments_class', 'default', 'yes'),
(1904, 'wcj_checkout_fields_order_comments_priority', '0', 'yes'),
(1905, 'wcj_checkout_fields_order_comments_cats_incl', 'a:0:{}', 'yes'),
(1906, 'wcj_checkout_fields_order_comments_cats_excl', 'a:0:{}', 'yes'),
(1946, 'wc_gateway_ppec_prompt_to_connect_message_dismissed', 'yes', 'yes'),
(1970, 'wcj_currency_per_product_cart_checkout', 'convert_shop_default', 'yes'),
(1971, 'wcj_currency_per_product_cart_checkout_leave_one_product', 'Only one product can be added to the cart. Clear the cart or finish the order, before adding another product to the cart.', 'yes'),
(1972, 'wcj_currency_per_product_cart_checkout_leave_same_currency', 'Only products with same currency can be added to the cart. Clear the cart or finish the order, before adding products with another currency to the cart.', 'yes'),
(1973, 'wcj_currency_per_product_per_product', 'yes', 'yes'),
(1974, 'wcj_currency_per_product_by_users_enabled', 'no', 'yes'),
(1975, 'wcj_currency_per_product_by_user_roles_enabled', 'no', 'yes'),
(1976, 'wcj_currency_per_product_by_product_cats_enabled', 'no', 'yes'),
(1977, 'wcj_currency_per_product_by_product_tags_enabled', 'no', 'yes'),
(1978, 'wcj_currency_per_product_exchange_rate_update', 'manual', 'yes'),
(1979, 'wcj_currency_per_product_total_number', '1', 'yes'),
(1980, 'wcj_currency_per_product_currency_1', 'USD', 'yes'),
(1981, 'wcj_currency_per_product_exchange_rate_1', '356', 'yes'),
(1982, 'wcj_currency_per_product_save_prices', 'no', 'yes'),
(1983, 'wcj_currency_exchange_rates_auto', 'daily', 'yes'),
(1984, 'wcj_currency_exchange_rates_server', 'ecb', 'yes'),
(1985, 'wcj_currency_exchange_rates_rounding_enabled', 'no', 'yes'),
(1986, 'wcj_currency_exchange_rates_rounding_precision', '0', 'yes'),
(1987, 'wcj_currency_exchange_rates_offset_percent', '0', 'yes'),
(1988, 'wcj_currency_exchange_rates_offset_fixed', '0', 'yes'),
(1989, 'wcj_currency_exchange_rates_calculate_by_invert', 'no', 'yes'),
(1990, 'wcj_currency_exchange_rates_always_curl', 'no', 'yes'),
(1991, 'wcj_currency_exchange_custom_currencies_total_number', '1', 'yes'),
(1992, 'wcj_currency_exchange_custom_currencies_1', 'disabled', 'yes'),
(1994, 'wcj_currency_exchange_rate_cron_time', '1533969141', 'yes'),
(1996, 'wcj_multicurrency_exchange_rate_update_auto', 'manual', 'yes'),
(1997, 'wcj_multicurrency_per_product_enabled', 'yes', 'yes'),
(1998, 'wcj_multicurrency_per_product_list_available_variations_only', 'yes', 'yes'),
(1999, 'wcj_multicurrency_per_product_make_empty', 'no', 'yes'),
(2000, 'wcj_multicurrency_revert', 'no', 'yes'),
(2001, 'wcj_multicurrency_rounding', 'no_round', 'yes'),
(2002, 'wcj_multicurrency_rounding_precision', '0', 'yes'),
(2003, 'wcj_multicurrency_switcher_template', '%currency_name% (%currency_symbol%)', 'yes'),
(2004, 'wcj_multicurrency_switcher_additional_price_filters', '', 'yes'),
(2005, 'wcj_multicurrency_advanced_price_hooks_priority', '0', 'yes'),
(2006, 'wcj_multicurrency_total_number', '2', 'yes'),
(2007, 'wcj_multicurrency_currency_1', 'KZT', 'yes'),
(2008, 'wcj_multicurrency_exchange_rate_1', '1', 'yes'),
(2009, 'wcj_multicurrency_currency_2', 'RUB', 'yes'),
(2010, 'wcj_multicurrency_exchange_rate_2', '0.17', 'yes'),
(2011, 'wcj_multicurrency_role_defaults_roles', 'a:0:{}', 'yes'),
(2014, 'wcj_price_by_country_customer_country_detection_method', 'by_ip_then_by_user_selection', 'yes'),
(2015, 'wcj_price_by_country_override_on_checkout_with_billing_country', 'no', 'yes'),
(2016, 'wcj_price_by_country_override_scope', 'all', 'yes'),
(2017, 'wcj_price_by_country_revert', 'no', 'yes'),
(2018, 'wcj_price_by_country_rounding', 'ceil', 'yes'),
(2019, 'wcj_price_by_country_make_pretty', 'no', 'yes'),
(2020, 'wcj_price_by_country_make_pretty_min_amount_multiplier', '1', 'yes'),
(2021, 'wcj_price_by_country_local_enabled', 'no', 'yes'),
(2022, 'wcj_price_by_country_local_options_style', 'inline', 'yes'),
(2023, 'wcj_price_by_country_backend_user_roles', 'a:0:{}', 'yes'),
(2024, 'wcj_price_by_country_price_filter_widget_support_enabled', 'no', 'yes'),
(2025, 'wcj_price_by_country_jquery_wselect_enabled', 'no', 'yes'),
(2026, 'wcj_price_by_country_for_bots_disabled', 'no', 'yes'),
(2027, 'wcj_price_by_country_advanced_price_hooks_priority', '0', 'yes'),
(2028, 'wcj_price_by_country_ip_detection_method', 'wc', 'yes'),
(2029, 'wcj_price_by_country_selection', 'multiselect', 'yes'),
(2030, 'wcj_price_by_country_total_groups_number', '1', 'yes'),
(2031, 'wcj_price_by_country_exchange_rate_countries_group_1', '', 'yes'),
(2032, 'wcj_price_by_country_exchange_rate_currency_group_1', 'RUB', 'yes'),
(2033, 'wcj_price_by_country_countries_group_admin_title_1', 'Группа #1', 'yes'),
(2034, 'wcj_price_by_country_auto_exchange_rates', 'manual', 'yes'),
(2035, 'wcj_price_by_country_exchange_rate_group_1', '0.17', 'yes'),
(2036, 'wcj_price_by_country_make_empty_price_group_1', 'no', 'yes'),
(2037, 'wcj_price_by_country_countries_group_chosen_select_1', 'a:0:{}', 'yes'),
(2039, 'wcj_price_by_country_countries_group_1', 'a:244:{i:0;s:2:\"AF\";i:1;s:2:\"AX\";i:2;s:2:\"AL\";i:3;s:2:\"DZ\";i:4;s:2:\"AD\";i:5;s:2:\"AO\";i:6;s:2:\"AI\";i:7;s:2:\"AQ\";i:8;s:2:\"AG\";i:9;s:2:\"AR\";i:10;s:2:\"AM\";i:11;s:2:\"AW\";i:12;s:2:\"AU\";i:13;s:2:\"AT\";i:14;s:2:\"AZ\";i:15;s:2:\"BS\";i:16;s:2:\"BH\";i:17;s:2:\"BD\";i:18;s:2:\"BB\";i:19;s:2:\"BY\";i:20;s:2:\"BE\";i:21;s:2:\"PW\";i:22;s:2:\"BZ\";i:23;s:2:\"BJ\";i:24;s:2:\"BM\";i:25;s:2:\"BT\";i:26;s:2:\"BO\";i:27;s:2:\"BQ\";i:28;s:2:\"BA\";i:29;s:2:\"BW\";i:30;s:2:\"BV\";i:31;s:2:\"BR\";i:32;s:2:\"IO\";i:33;s:2:\"VG\";i:34;s:2:\"BN\";i:35;s:2:\"BG\";i:36;s:2:\"BF\";i:37;s:2:\"BI\";i:38;s:2:\"KH\";i:39;s:2:\"CM\";i:40;s:2:\"CA\";i:41;s:2:\"CV\";i:42;s:2:\"KY\";i:43;s:2:\"CF\";i:44;s:2:\"TD\";i:45;s:2:\"CL\";i:46;s:2:\"CN\";i:47;s:2:\"CX\";i:48;s:2:\"CC\";i:49;s:2:\"CO\";i:50;s:2:\"KM\";i:51;s:2:\"CG\";i:52;s:2:\"CD\";i:53;s:2:\"CK\";i:54;s:2:\"CR\";i:55;s:2:\"HR\";i:56;s:2:\"CU\";i:57;s:2:\"CW\";i:58;s:2:\"CY\";i:59;s:2:\"CZ\";i:60;s:2:\"DK\";i:61;s:2:\"DJ\";i:62;s:2:\"DM\";i:63;s:2:\"DO\";i:64;s:2:\"EC\";i:65;s:2:\"EG\";i:66;s:2:\"SV\";i:67;s:2:\"GQ\";i:68;s:2:\"ER\";i:69;s:2:\"EE\";i:70;s:2:\"ET\";i:71;s:2:\"FK\";i:72;s:2:\"FO\";i:73;s:2:\"FJ\";i:74;s:2:\"FI\";i:75;s:2:\"FR\";i:76;s:2:\"GF\";i:77;s:2:\"PF\";i:78;s:2:\"TF\";i:79;s:2:\"GA\";i:80;s:2:\"GM\";i:81;s:2:\"GE\";i:82;s:2:\"DE\";i:83;s:2:\"GH\";i:84;s:2:\"GI\";i:85;s:2:\"GR\";i:86;s:2:\"GL\";i:87;s:2:\"GD\";i:88;s:2:\"GP\";i:89;s:2:\"GT\";i:90;s:2:\"GG\";i:91;s:2:\"GN\";i:92;s:2:\"GW\";i:93;s:2:\"GY\";i:94;s:2:\"HT\";i:95;s:2:\"HM\";i:96;s:2:\"HN\";i:97;s:2:\"HK\";i:98;s:2:\"HU\";i:99;s:2:\"IS\";i:100;s:2:\"IN\";i:101;s:2:\"ID\";i:102;s:2:\"IR\";i:103;s:2:\"IQ\";i:104;s:2:\"IE\";i:105;s:2:\"IM\";i:106;s:2:\"IL\";i:107;s:2:\"IT\";i:108;s:2:\"CI\";i:109;s:2:\"JM\";i:110;s:2:\"JP\";i:111;s:2:\"JE\";i:112;s:2:\"JO\";i:113;s:2:\"KE\";i:114;s:2:\"KI\";i:115;s:2:\"KW\";i:116;s:2:\"KG\";i:117;s:2:\"LA\";i:118;s:2:\"LV\";i:119;s:2:\"LB\";i:120;s:2:\"LS\";i:121;s:2:\"LR\";i:122;s:2:\"LY\";i:123;s:2:\"LI\";i:124;s:2:\"LT\";i:125;s:2:\"LU\";i:126;s:2:\"MO\";i:127;s:2:\"MK\";i:128;s:2:\"MG\";i:129;s:2:\"MW\";i:130;s:2:\"MY\";i:131;s:2:\"MV\";i:132;s:2:\"ML\";i:133;s:2:\"MT\";i:134;s:2:\"MH\";i:135;s:2:\"MQ\";i:136;s:2:\"MR\";i:137;s:2:\"MU\";i:138;s:2:\"YT\";i:139;s:2:\"MX\";i:140;s:2:\"FM\";i:141;s:2:\"MD\";i:142;s:2:\"MC\";i:143;s:2:\"MN\";i:144;s:2:\"ME\";i:145;s:2:\"MS\";i:146;s:2:\"MA\";i:147;s:2:\"MZ\";i:148;s:2:\"MM\";i:149;s:2:\"NA\";i:150;s:2:\"NR\";i:151;s:2:\"NP\";i:152;s:2:\"NL\";i:153;s:2:\"AN\";i:154;s:2:\"NC\";i:155;s:2:\"NZ\";i:156;s:2:\"NI\";i:157;s:2:\"NE\";i:158;s:2:\"NG\";i:159;s:2:\"NU\";i:160;s:2:\"NF\";i:161;s:2:\"KP\";i:162;s:2:\"NO\";i:163;s:2:\"OM\";i:164;s:2:\"PK\";i:165;s:2:\"PS\";i:166;s:2:\"PA\";i:167;s:2:\"PG\";i:168;s:2:\"PY\";i:169;s:2:\"PE\";i:170;s:2:\"PH\";i:171;s:2:\"PN\";i:172;s:2:\"PL\";i:173;s:2:\"PT\";i:174;s:2:\"QA\";i:175;s:2:\"RE\";i:176;s:2:\"RO\";i:177;s:2:\"RU\";i:178;s:2:\"RW\";i:179;s:2:\"BL\";i:180;s:2:\"SH\";i:181;s:2:\"KN\";i:182;s:2:\"LC\";i:183;s:2:\"MF\";i:184;s:2:\"SX\";i:185;s:2:\"PM\";i:186;s:2:\"VC\";i:187;s:2:\"SM\";i:188;s:2:\"ST\";i:189;s:2:\"SA\";i:190;s:2:\"SN\";i:191;s:2:\"RS\";i:192;s:2:\"SC\";i:193;s:2:\"SL\";i:194;s:2:\"SG\";i:195;s:2:\"SK\";i:196;s:2:\"SI\";i:197;s:2:\"SB\";i:198;s:2:\"SO\";i:199;s:2:\"ZA\";i:200;s:2:\"GS\";i:201;s:2:\"KR\";i:202;s:2:\"SS\";i:203;s:2:\"ES\";i:204;s:2:\"LK\";i:205;s:2:\"SD\";i:206;s:2:\"SR\";i:207;s:2:\"SJ\";i:208;s:2:\"SZ\";i:209;s:2:\"SE\";i:210;s:2:\"CH\";i:211;s:2:\"SY\";i:212;s:2:\"TW\";i:213;s:2:\"TJ\";i:214;s:2:\"TZ\";i:215;s:2:\"TH\";i:216;s:2:\"TL\";i:217;s:2:\"TG\";i:218;s:2:\"TK\";i:219;s:2:\"TO\";i:220;s:2:\"TT\";i:221;s:2:\"TN\";i:222;s:2:\"TR\";i:223;s:2:\"TM\";i:224;s:2:\"TC\";i:225;s:2:\"TV\";i:226;s:2:\"UG\";i:227;s:2:\"UA\";i:228;s:2:\"AE\";i:229;s:2:\"GB\";i:230;s:2:\"US\";i:231;s:2:\"UY\";i:232;s:2:\"UZ\";i:233;s:2:\"VU\";i:234;s:2:\"VA\";i:235;s:2:\"VE\";i:236;s:2:\"VN\";i:237;s:2:\"WF\";i:238;s:2:\"EH\";i:239;s:2:\"WS\";i:240;s:2:\"YE\";i:241;s:2:\"ZM\";i:242;s:2:\"ZW\";i:243;s:2:\"EU\";}', 'yes'),
(2174, '_transient_orders-transient-version', '1552824939', 'yes'),
(2197, '_transient_wc_attribute_taxonomies', 'a:2:{i:0;O:8:\"stdClass\":6:{s:12:\"attribute_id\";s:1:\"2\";s:14:\"attribute_name\";s:5:\"color\";s:15:\"attribute_label\";s:8:\"Цвет\";s:14:\"attribute_type\";s:6:\"select\";s:17:\"attribute_orderby\";s:10:\"menu_order\";s:16:\"attribute_public\";s:1:\"0\";}i:1;O:8:\"stdClass\":6:{s:12:\"attribute_id\";s:1:\"1\";s:14:\"attribute_name\";s:8:\"material\";s:15:\"attribute_label\";s:16:\"Метериал\";s:14:\"attribute_type\";s:6:\"select\";s:17:\"attribute_orderby\";s:10:\"menu_order\";s:16:\"attribute_public\";s:1:\"0\";}}', 'yes'),
(2228, 'custom_product_tabs_onesixone_data_update', '1', 'yes'),
(2229, 'yikes_woo_reusable_products_tabs', 'a:1:{i:1;a:7:{s:9:\"tab_title\";s:40:\"Рекомендации по уходу\";s:8:\"tab_name\";s:17:\"recomendation_tab\";s:11:\"tab_content\";s:731:\"<p>Consul laudem usu ne, eos error primis neglegentur id. Commune delicata deterruisset per ut. Sed officiis honestatis eu. Timeam graecis no vim, ad congue ornatus quo, sea ad salutatus iracundia assueverit. Nam sensibus adversarium no. Id est fuisset sapientem conceptam. Adipiscing sadipscing ad usu.</p><p>No nominavi tractatos definitiones mel, prompta neglegentur ad ius, homero inermis his an. Te atqui quaeque nec, an laoreet urbanitas posidonium his. No sea veniam delenit, sit ut numquam pericula. Vocent tacimates est ex, illud appellantur ea duo, eam discere partiendo dignissim at. Eum ridens apeirian ut, dicta putant no vis, has an primis impetus theophrastus. Nisl summo libris at eos, an eos vivendo principes.</p>\";s:6:\"tab_id\";i:1;s:10:\"taxonomies\";a:0:{}s:8:\"tab_slug\";s:40:\"рекомендации-по-уходу\";s:10:\"global_tab\";b:0;}}', 'yes'),
(2230, 'yikes_woo_reusable_products_tabs_applied', 'a:1:{i:95;a:1:{i:1;a:3:{s:7:\"post_id\";i:95;s:15:\"reusable_tab_id\";s:1:\"1\";s:6:\"tab_id\";s:40:\"рекомендации-по-уходу\";}}}', 'yes'),
(2283, 'category_children', 'a:0:{}', 'yes'),
(2356, 'product_cat_children', 'a:3:{i:18;a:8:{i:0;i:19;i:1;i:22;i:2;i:23;i:3;i:24;i:4;i:25;i:5;i:26;i:6;i:27;i:7;i:28;}i:20;a:7:{i:0;i:21;i:1;i:37;i:2;i:43;i:3;i:44;i:4;i:45;i:5;i:46;i:6;i:47;}i:48;a:2:{i:0;i:49;i:1;i:50;}}', 'yes'),
(2377, 'wpcf7', 'a:2:{s:7:\"version\";s:5:\"5.0.3\";s:13:\"bulk_validate\";a:4:{s:9:\"timestamp\";i:1534163202;s:7:\"version\";s:5:\"5.0.3\";s:11:\"count_valid\";i:1;s:13:\"count_invalid\";i:0;}}', 'yes');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(10703, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:5:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:7:\"upgrade\";s:8:\"download\";s:63:\"https://downloads.wordpress.org/release/ru_RU/wordpress-5.2.zip\";s:6:\"locale\";s:5:\"ru_RU\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:63:\"https://downloads.wordpress.org/release/ru_RU/wordpress-5.2.zip\";s:10:\"no_content\";b:0;s:11:\"new_bundled\";b:0;s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:3:\"5.2\";s:7:\"version\";s:3:\"5.2\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:0:\"\";}i:1;O:8:\"stdClass\":10:{s:8:\"response\";s:7:\"upgrade\";s:8:\"download\";s:57:\"https://downloads.wordpress.org/release/wordpress-5.2.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:57:\"https://downloads.wordpress.org/release/wordpress-5.2.zip\";s:10:\"no_content\";s:68:\"https://downloads.wordpress.org/release/wordpress-5.2-no-content.zip\";s:11:\"new_bundled\";s:69:\"https://downloads.wordpress.org/release/wordpress-5.2-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:3:\"5.2\";s:7:\"version\";s:3:\"5.2\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:0:\"\";}i:2;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:57:\"https://downloads.wordpress.org/release/wordpress-5.2.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:57:\"https://downloads.wordpress.org/release/wordpress-5.2.zip\";s:10:\"no_content\";s:68:\"https://downloads.wordpress.org/release/wordpress-5.2-no-content.zip\";s:11:\"new_bundled\";s:69:\"https://downloads.wordpress.org/release/wordpress-5.2-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:3:\"5.2\";s:7:\"version\";s:3:\"5.2\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}i:3;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.1.1.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.1.1.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.1.1-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.1.1-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.1.1\";s:7:\"version\";s:5:\"5.1.1\";s:11:\"php_version\";s:5:\"5.2.4\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}i:4;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.0.4.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.0.4.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.0.4-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.0.4-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.0.4\";s:7:\"version\";s:5:\"5.0.4\";s:11:\"php_version\";s:5:\"5.2.4\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}}s:12:\"last_checked\";i:1558350839;s:15:\"version_checked\";s:6:\"4.9.10\";s:12:\"translations\";a:0:{}}', 'no'),
(11126, '_transient_wc_count_comments', 'O:8:\"stdClass\":7:{s:14:\"total_comments\";i:2;s:3:\"all\";i:2;s:8:\"approved\";s:1:\"2\";s:9:\"moderated\";i:0;s:4:\"spam\";i:0;s:5:\"trash\";i:0;s:12:\"post-trashed\";i:0;}', 'yes'),
(14421, '_transient_timeout_wc_term_counts', '1560524993', 'no'),
(14422, '_transient_wc_term_counts', 'a:21:{i:17;s:1:\"0\";i:20;s:1:\"4\";i:26;s:0:\"\";i:45;s:0:\"\";i:21;s:0:\"\";i:23;s:0:\"\";i:49;s:0:\"\";i:47;s:0:\"\";i:25;s:1:\"1\";i:50;s:0:\"\";i:28;s:0:\"\";i:19;s:0:\"\";i:18;s:1:\"1\";i:24;s:0:\"\";i:44;s:0:\"\";i:43;s:0:\"\";i:22;s:0:\"\";i:27;s:0:\"\";i:48;s:0:\"\";i:37;s:1:\"4\";i:46;s:0:\"\";}', 'no'),
(14621, '_transient_timeout_wc_related_95', '1558368406', 'no'),
(14622, '_transient_wc_related_95', 'a:1:{s:50:\"limit=3&exclude_ids%5B0%5D=0&exclude_ids%5B1%5D=95\";a:3:{i:0;s:2:\"63\";i:1;s:2:\"96\";i:2;s:2:\"97\";}}', 'no'),
(14623, '_transient_timeout_wc_related_97', '1558368410', 'no'),
(14624, '_transient_wc_related_97', 'a:1:{s:50:\"limit=3&exclude_ids%5B0%5D=0&exclude_ids%5B1%5D=97\";a:3:{i:0;s:2:\"63\";i:1;s:2:\"95\";i:2;s:2:\"96\";}}', 'no'),
(14625, '_transient_timeout_wc_related_63', '1558368420', 'no'),
(14626, '_transient_wc_related_63', 'a:1:{s:50:\"limit=3&exclude_ids%5B0%5D=0&exclude_ids%5B1%5D=63\";a:3:{i:0;s:2:\"95\";i:1;s:2:\"96\";i:2;s:2:\"97\";}}', 'no'),
(14627, '_transient_timeout_wc_related_96', '1558368430', 'no'),
(14628, '_transient_wc_related_96', 'a:1:{s:50:\"limit=3&exclude_ids%5B0%5D=0&exclude_ids%5B1%5D=96\";a:3:{i:0;s:2:\"63\";i:1;s:2:\"95\";i:2;s:2:\"97\";}}', 'no'),
(14646, '_transient_timeout_external_ip_address_35.204.60.176', '1558925525', 'no'),
(14647, '_transient_external_ip_address_35.204.60.176', '93.185.67.179', 'no'),
(14667, '_site_transient_timeout_theme_roots', '1558352637', 'no'),
(14668, '_site_transient_theme_roots', 'a:4:{s:8:\"os_theme\";s:7:\"/themes\";s:13:\"twentyfifteen\";s:7:\"/themes\";s:15:\"twentyseventeen\";s:7:\"/themes\";s:13:\"twentysixteen\";s:7:\"/themes\";}', 'no'),
(14670, '_site_transient_update_themes', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1558350841;s:7:\"checked\";a:4:{s:8:\"os_theme\";s:0:\"\";s:13:\"twentyfifteen\";s:3:\"2.0\";s:15:\"twentyseventeen\";s:3:\"1.7\";s:13:\"twentysixteen\";s:3:\"1.5\";}s:8:\"response\";a:3:{s:13:\"twentyfifteen\";a:4:{s:5:\"theme\";s:13:\"twentyfifteen\";s:11:\"new_version\";s:3:\"2.5\";s:3:\"url\";s:43:\"https://wordpress.org/themes/twentyfifteen/\";s:7:\"package\";s:59:\"https://downloads.wordpress.org/theme/twentyfifteen.2.5.zip\";}s:15:\"twentyseventeen\";a:4:{s:5:\"theme\";s:15:\"twentyseventeen\";s:11:\"new_version\";s:3:\"2.2\";s:3:\"url\";s:45:\"https://wordpress.org/themes/twentyseventeen/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/theme/twentyseventeen.2.2.zip\";}s:13:\"twentysixteen\";a:4:{s:5:\"theme\";s:13:\"twentysixteen\";s:11:\"new_version\";s:3:\"2.0\";s:3:\"url\";s:43:\"https://wordpress.org/themes/twentysixteen/\";s:7:\"package\";s:59:\"https://downloads.wordpress.org/theme/twentysixteen.2.0.zip\";}}s:12:\"translations\";a:0:{}}', 'no'),
(14671, '_site_transient_update_plugins', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1558350841;s:8:\"response\";a:8:{s:19:\"akismet/akismet.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:21:\"w.org/plugins/akismet\";s:4:\"slug\";s:7:\"akismet\";s:6:\"plugin\";s:19:\"akismet/akismet.php\";s:11:\"new_version\";s:5:\"4.1.2\";s:3:\"url\";s:38:\"https://wordpress.org/plugins/akismet/\";s:7:\"package\";s:56:\"https://downloads.wordpress.org/plugin/akismet.4.1.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:59:\"https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272\";s:2:\"1x\";s:59:\"https://ps.w.org/akismet/assets/icon-128x128.png?rev=969272\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:61:\"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:3:\"5.2\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:43:\"woocommerce-jetpack/woocommerce-jetpack.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:33:\"w.org/plugins/woocommerce-jetpack\";s:4:\"slug\";s:19:\"woocommerce-jetpack\";s:6:\"plugin\";s:43:\"woocommerce-jetpack/woocommerce-jetpack.php\";s:11:\"new_version\";s:5:\"4.3.1\";s:3:\"url\";s:50:\"https://wordpress.org/plugins/woocommerce-jetpack/\";s:7:\"package\";s:68:\"https://downloads.wordpress.org/plugin/woocommerce-jetpack.4.3.1.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:72:\"https://ps.w.org/woocommerce-jetpack/assets/icon-256x256.png?rev=1813426\";s:2:\"1x\";s:72:\"https://ps.w.org/woocommerce-jetpack/assets/icon-128x128.png?rev=1813426\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:75:\"https://ps.w.org/woocommerce-jetpack/assets/banner-1544x500.jpg?rev=1636218\";s:2:\"1x\";s:74:\"https://ps.w.org/woocommerce-jetpack/assets/banner-772x250.jpg?rev=1636218\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.1.1\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:36:\"contact-form-7/wp-contact-form-7.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:28:\"w.org/plugins/contact-form-7\";s:4:\"slug\";s:14:\"contact-form-7\";s:6:\"plugin\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:11:\"new_version\";s:5:\"5.1.3\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/contact-form-7/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/contact-form-7.5.1.3.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:66:\"https://ps.w.org/contact-form-7/assets/icon-256x256.png?rev=984007\";s:2:\"1x\";s:66:\"https://ps.w.org/contact-form-7/assets/icon-128x128.png?rev=984007\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/contact-form-7/assets/banner-1544x500.png?rev=860901\";s:2:\"1x\";s:68:\"https://ps.w.org/contact-form-7/assets/banner-772x250.png?rev=880427\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:3:\"5.2\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:97:\"yikes-inc-easy-custom-woocommerce-product-tabs/yikes-inc-easy-custom-woocommerce-product-tabs.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:60:\"w.org/plugins/yikes-inc-easy-custom-woocommerce-product-tabs\";s:4:\"slug\";s:46:\"yikes-inc-easy-custom-woocommerce-product-tabs\";s:6:\"plugin\";s:97:\"yikes-inc-easy-custom-woocommerce-product-tabs/yikes-inc-easy-custom-woocommerce-product-tabs.php\";s:11:\"new_version\";s:6:\"1.6.10\";s:3:\"url\";s:77:\"https://wordpress.org/plugins/yikes-inc-easy-custom-woocommerce-product-tabs/\";s:7:\"package\";s:96:\"https://downloads.wordpress.org/plugin/yikes-inc-easy-custom-woocommerce-product-tabs.1.6.10.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:99:\"https://ps.w.org/yikes-inc-easy-custom-woocommerce-product-tabs/assets/icon-256x256.png?rev=1558461\";s:2:\"1x\";s:99:\"https://ps.w.org/yikes-inc-easy-custom-woocommerce-product-tabs/assets/icon-128x128.png?rev=1558461\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:102:\"https://ps.w.org/yikes-inc-easy-custom-woocommerce-product-tabs/assets/banner-1544x500.png?rev=1558461\";s:2:\"1x\";s:101:\"https://ps.w.org/yikes-inc-easy-custom-woocommerce-product-tabs/assets/banner-772x250.png?rev=1558478\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.2.0\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:51:\"mailchimp-for-woocommerce/mailchimp-woocommerce.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:39:\"w.org/plugins/mailchimp-for-woocommerce\";s:4:\"slug\";s:25:\"mailchimp-for-woocommerce\";s:6:\"plugin\";s:51:\"mailchimp-for-woocommerce/mailchimp-woocommerce.php\";s:11:\"new_version\";s:6:\"2.1.16\";s:3:\"url\";s:56:\"https://wordpress.org/plugins/mailchimp-for-woocommerce/\";s:7:\"package\";s:75:\"https://downloads.wordpress.org/plugin/mailchimp-for-woocommerce.2.1.16.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:78:\"https://ps.w.org/mailchimp-for-woocommerce/assets/icon-256x256.png?rev=1509501\";s:2:\"1x\";s:78:\"https://ps.w.org/mailchimp-for-woocommerce/assets/icon-256x256.png?rev=1509501\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:81:\"https://ps.w.org/mailchimp-for-woocommerce/assets/banner-1544x500.png?rev=1950415\";s:2:\"1x\";s:80:\"https://ps.w.org/mailchimp-for-woocommerce/assets/banner-772x250.jpg?rev=1950415\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.1.1\";s:12:\"requires_php\";s:3:\"7.0\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:27:\"woocommerce/woocommerce.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:25:\"w.org/plugins/woocommerce\";s:4:\"slug\";s:11:\"woocommerce\";s:6:\"plugin\";s:27:\"woocommerce/woocommerce.php\";s:11:\"new_version\";s:5:\"3.6.3\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/woocommerce/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/woocommerce.3.6.3.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:64:\"https://ps.w.org/woocommerce/assets/icon-256x256.png?rev=2075035\";s:2:\"1x\";s:64:\"https://ps.w.org/woocommerce/assets/icon-128x128.png?rev=2075035\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/woocommerce/assets/banner-1544x500.png?rev=2075035\";s:2:\"1x\";s:66:\"https://ps.w.org/woocommerce/assets/banner-772x250.png?rev=2075035\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:3:\"5.2\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:51:\"ti-woocommerce-wishlist/ti-woocommerce-wishlist.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:37:\"w.org/plugins/ti-woocommerce-wishlist\";s:4:\"slug\";s:23:\"ti-woocommerce-wishlist\";s:6:\"plugin\";s:51:\"ti-woocommerce-wishlist/ti-woocommerce-wishlist.php\";s:11:\"new_version\";s:6:\"1.12.3\";s:3:\"url\";s:54:\"https://wordpress.org/plugins/ti-woocommerce-wishlist/\";s:7:\"package\";s:73:\"https://downloads.wordpress.org/plugin/ti-woocommerce-wishlist.1.12.3.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:76:\"https://ps.w.org/ti-woocommerce-wishlist/assets/icon-256x256.png?rev=2071101\";s:2:\"1x\";s:76:\"https://ps.w.org/ti-woocommerce-wishlist/assets/icon-128x128.png?rev=2071101\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:79:\"https://ps.w.org/ti-woocommerce-wishlist/assets/banner-1544x500.png?rev=2071101\";s:2:\"1x\";s:78:\"https://ps.w.org/ti-woocommerce-wishlist/assets/banner-772x250.png?rev=2071101\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:3:\"5.2\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:41:\"yith-woocommerce-ajax-navigation/init.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:46:\"w.org/plugins/yith-woocommerce-ajax-navigation\";s:4:\"slug\";s:32:\"yith-woocommerce-ajax-navigation\";s:6:\"plugin\";s:41:\"yith-woocommerce-ajax-navigation/init.php\";s:11:\"new_version\";s:5:\"3.6.3\";s:3:\"url\";s:63:\"https://wordpress.org/plugins/yith-woocommerce-ajax-navigation/\";s:7:\"package\";s:81:\"https://downloads.wordpress.org/plugin/yith-woocommerce-ajax-navigation.3.6.3.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:85:\"https://ps.w.org/yith-woocommerce-ajax-navigation/assets/icon-128x128.jpg?rev=1460901\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:88:\"https://ps.w.org/yith-woocommerce-ajax-navigation/assets/banner-1544x500.jpg?rev=1460901\";s:2:\"1x\";s:87:\"https://ps.w.org/yith-woocommerce-ajax-navigation/assets/banner-772x250.jpg?rev=1460901\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.1.1\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:1:{s:9:\"hello.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/hello-dolly\";s:4:\"slug\";s:11:\"hello-dolly\";s:6:\"plugin\";s:9:\"hello.php\";s:11:\"new_version\";s:3:\"1.6\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/hello-dolly/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/plugin/hello-dolly.1.6.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:64:\"https://ps.w.org/hello-dolly/assets/icon-256x256.jpg?rev=2052855\";s:2:\"1x\";s:64:\"https://ps.w.org/hello-dolly/assets/icon-128x128.jpg?rev=2052855\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:66:\"https://ps.w.org/hello-dolly/assets/banner-772x250.jpg?rev=2052855\";}s:11:\"banners_rtl\";a:0:{}}}}', 'no');

-- --------------------------------------------------------

--
-- Структура таблицы `wp_postmeta`
--

CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `wp_postmeta`
--

INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 3, '_wp_page_template', 'default'),
(3, 5, '_wp_attached_file', '2018/07/background.jpg'),
(4, 5, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1920;s:6:\"height\";i:1280;s:4:\"file\";s:22:\"2018/07/background.jpg\";s:5:\"sizes\";a:7:{s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:22:\"background-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:1;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:22:\"background-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:22:\"background-600x400.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"background-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"background-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:22:\"background-768x512.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:512;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:23:\"background-1024x683.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:683;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(5, 5, '_wp_attachment_is_custom_background', 'os_theme'),
(18, 12, '_wp_attached_file', '2018/07/logo.png'),
(19, 12, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:377;s:6:\"height\";i:75;s:4:\"file\";s:16:\"2018/07/logo.png\";s:5:\"sizes\";a:4:{s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:15:\"logo-300x75.png\";s:5:\"width\";i:300;s:6:\"height\";i:75;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:1;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:15:\"logo-100x75.png\";s:5:\"width\";i:100;s:6:\"height\";i:75;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:15:\"logo-150x75.png\";s:5:\"width\";i:150;s:6:\"height\";i:75;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:15:\"logo-300x60.png\";s:5:\"width\";i:300;s:6:\"height\";i:60;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(20, 13, '_wp_attached_file', '2018/07/cropped-logo.png'),
(21, 13, '_wp_attachment_context', 'site-icon'),
(22, 13, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:512;s:6:\"height\";i:512;s:4:\"file\";s:24:\"2018/07/cropped-logo.png\";s:5:\"sizes\";a:6:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:24:\"cropped-logo-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:24:\"cropped-logo-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"site_icon-270\";a:4:{s:4:\"file\";s:24:\"cropped-logo-270x270.png\";s:5:\"width\";i:270;s:6:\"height\";i:270;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"site_icon-192\";a:4:{s:4:\"file\";s:24:\"cropped-logo-192x192.png\";s:5:\"width\";i:192;s:6:\"height\";i:192;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"site_icon-180\";a:4:{s:4:\"file\";s:24:\"cropped-logo-180x180.png\";s:5:\"width\";i:180;s:6:\"height\";i:180;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"site_icon-32\";a:4:{s:4:\"file\";s:22:\"cropped-logo-32x32.png\";s:5:\"width\";i:32;s:6:\"height\";i:32;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(28, 2, '_edit_lock', '1533879508:1'),
(29, 2, '_edit_last', '1'),
(43, 23, '_edit_last', '1'),
(44, 23, '_edit_lock', '1530686556:1'),
(45, 23, '_wp_page_template', 'default'),
(46, 25, '_edit_last', '1'),
(47, 25, '_edit_lock', '1530686584:1'),
(48, 25, '_wp_page_template', 'default'),
(49, 27, '_edit_last', '1'),
(50, 27, '_edit_lock', '1530686620:1'),
(51, 27, '_wp_page_template', 'default'),
(52, 29, '_edit_last', '1'),
(53, 29, '_edit_lock', '1530686652:1'),
(54, 29, '_wp_page_template', 'default'),
(55, 31, '_edit_last', '1'),
(56, 31, '_edit_lock', '1530686686:1'),
(57, 31, '_wp_page_template', 'default'),
(58, 33, '_edit_last', '1'),
(59, 33, '_edit_lock', '1530686713:1'),
(60, 33, '_wp_page_template', 'default'),
(61, 3, '_edit_last', '1'),
(62, 3, '_edit_lock', '1530686807:1'),
(63, 37, '_menu_item_type', 'post_type'),
(64, 37, '_menu_item_menu_item_parent', '0'),
(65, 37, '_menu_item_object_id', '33'),
(66, 37, '_menu_item_object', 'page'),
(67, 37, '_menu_item_target', ''),
(68, 37, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(69, 37, '_menu_item_xfn', ''),
(70, 37, '_menu_item_url', ''),
(72, 38, '_menu_item_type', 'post_type'),
(73, 38, '_menu_item_menu_item_parent', '0'),
(74, 38, '_menu_item_object_id', '23'),
(75, 38, '_menu_item_object', 'page'),
(76, 38, '_menu_item_target', ''),
(77, 38, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(78, 38, '_menu_item_xfn', ''),
(79, 38, '_menu_item_url', ''),
(81, 39, '_menu_item_type', 'post_type'),
(82, 39, '_menu_item_menu_item_parent', '0'),
(83, 39, '_menu_item_object_id', '3'),
(84, 39, '_menu_item_object', 'page'),
(85, 39, '_menu_item_target', ''),
(86, 39, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(87, 39, '_menu_item_xfn', ''),
(88, 39, '_menu_item_url', ''),
(90, 40, '_menu_item_type', 'post_type'),
(91, 40, '_menu_item_menu_item_parent', '0'),
(92, 40, '_menu_item_object_id', '29'),
(93, 40, '_menu_item_object', 'page'),
(94, 40, '_menu_item_target', ''),
(95, 40, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(96, 40, '_menu_item_xfn', ''),
(97, 40, '_menu_item_url', ''),
(99, 41, '_menu_item_type', 'post_type'),
(100, 41, '_menu_item_menu_item_parent', '0'),
(101, 41, '_menu_item_object_id', '27'),
(102, 41, '_menu_item_object', 'page'),
(103, 41, '_menu_item_target', ''),
(104, 41, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(105, 41, '_menu_item_xfn', ''),
(106, 41, '_menu_item_url', ''),
(108, 42, '_menu_item_type', 'post_type'),
(109, 42, '_menu_item_menu_item_parent', '0'),
(110, 42, '_menu_item_object_id', '25'),
(111, 42, '_menu_item_object', 'page'),
(112, 42, '_menu_item_target', ''),
(113, 42, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(114, 42, '_menu_item_xfn', ''),
(115, 42, '_menu_item_url', ''),
(117, 43, '_menu_item_type', 'post_type'),
(118, 43, '_menu_item_menu_item_parent', '0'),
(119, 43, '_menu_item_object_id', '31'),
(120, 43, '_menu_item_object', 'page'),
(121, 43, '_menu_item_target', ''),
(122, 43, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(123, 43, '_menu_item_xfn', ''),
(124, 43, '_menu_item_url', ''),
(126, 48, '_menu_item_type', 'taxonomy'),
(127, 48, '_menu_item_menu_item_parent', '0'),
(128, 48, '_menu_item_object_id', '26'),
(129, 48, '_menu_item_object', 'product_cat'),
(130, 48, '_menu_item_target', ''),
(131, 48, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(132, 48, '_menu_item_xfn', ''),
(133, 48, '_menu_item_url', ''),
(144, 50, '_menu_item_type', 'taxonomy'),
(145, 50, '_menu_item_menu_item_parent', '0'),
(146, 50, '_menu_item_object_id', '23'),
(147, 50, '_menu_item_object', 'product_cat'),
(148, 50, '_menu_item_target', ''),
(149, 50, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(150, 50, '_menu_item_xfn', ''),
(151, 50, '_menu_item_url', ''),
(153, 51, '_menu_item_type', 'taxonomy'),
(154, 51, '_menu_item_menu_item_parent', '0'),
(155, 51, '_menu_item_object_id', '25'),
(156, 51, '_menu_item_object', 'product_cat'),
(157, 51, '_menu_item_target', ''),
(158, 51, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(159, 51, '_menu_item_xfn', ''),
(160, 51, '_menu_item_url', ''),
(162, 52, '_menu_item_type', 'taxonomy'),
(163, 52, '_menu_item_menu_item_parent', '0'),
(164, 52, '_menu_item_object_id', '28'),
(165, 52, '_menu_item_object', 'product_cat'),
(166, 52, '_menu_item_target', ''),
(167, 52, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(168, 52, '_menu_item_xfn', ''),
(169, 52, '_menu_item_url', ''),
(171, 53, '_menu_item_type', 'taxonomy'),
(172, 53, '_menu_item_menu_item_parent', '0'),
(173, 53, '_menu_item_object_id', '24'),
(174, 53, '_menu_item_object', 'product_cat'),
(175, 53, '_menu_item_target', ''),
(176, 53, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(177, 53, '_menu_item_xfn', ''),
(178, 53, '_menu_item_url', ''),
(180, 54, '_menu_item_type', 'taxonomy'),
(181, 54, '_menu_item_menu_item_parent', '0'),
(182, 54, '_menu_item_object_id', '22'),
(183, 54, '_menu_item_object', 'product_cat'),
(184, 54, '_menu_item_target', ''),
(185, 54, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(186, 54, '_menu_item_xfn', ''),
(187, 54, '_menu_item_url', ''),
(189, 55, '_menu_item_type', 'taxonomy'),
(190, 55, '_menu_item_menu_item_parent', '0'),
(191, 55, '_menu_item_object_id', '27'),
(192, 55, '_menu_item_object', 'product_cat'),
(193, 55, '_menu_item_target', ''),
(194, 55, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(195, 55, '_menu_item_xfn', ''),
(196, 55, '_menu_item_url', ''),
(243, 62, '_wp_attached_file', '2018/07/romb.png'),
(244, 62, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:27;s:6:\"height\";i:27;s:4:\"file\";s:16:\"2018/07/romb.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(245, 63, '_wc_review_count', '0'),
(246, 63, '_wc_rating_count', 'a:0:{}'),
(247, 63, '_wc_average_rating', '0'),
(248, 63, '_edit_last', '1'),
(249, 63, '_edit_lock', '1532085045:1'),
(250, 64, '_wp_attached_file', '2018/07/eleganzza-Z5344-4898.jpg'),
(251, 64, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:480;s:6:\"height\";i:480;s:4:\"file\";s:32:\"2018/07/eleganzza-Z5344-4898.jpg\";s:5:\"sizes\";a:6:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:32:\"eleganzza-Z5344-4898-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:32:\"eleganzza-Z5344-4898-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:32:\"eleganzza-Z5344-4898-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:1;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:32:\"eleganzza-Z5344-4898-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:32:\"eleganzza-Z5344-4898-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:1;}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:32:\"eleganzza-Z5344-4898-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(252, 65, '_wp_attached_file', '2018/07/eleganzza-Z5344-4898-1.jpg'),
(253, 65, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:768;s:6:\"height\";i:768;s:4:\"file\";s:34:\"2018/07/eleganzza-Z5344-4898-1.jpg\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:34:\"eleganzza-Z5344-4898-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:34:\"eleganzza-Z5344-4898-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:34:\"eleganzza-Z5344-4898-1-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:34:\"eleganzza-Z5344-4898-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:1;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:34:\"eleganzza-Z5344-4898-1-600x600.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:600;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:34:\"eleganzza-Z5344-4898-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:34:\"eleganzza-Z5344-4898-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:1;}s:11:\"shop_single\";a:4:{s:4:\"file\";s:34:\"eleganzza-Z5344-4898-1-600x600.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:600;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:34:\"eleganzza-Z5344-4898-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(254, 66, '_wp_attached_file', '2018/07/eleganzza-Z5344-4898-2.jpg'),
(255, 66, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:768;s:6:\"height\";i:1024;s:4:\"file\";s:34:\"2018/07/eleganzza-Z5344-4898-2.jpg\";s:5:\"sizes\";a:10:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:34:\"eleganzza-Z5344-4898-2-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:34:\"eleganzza-Z5344-4898-2-225x300.jpg\";s:5:\"width\";i:225;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:35:\"eleganzza-Z5344-4898-2-768x1024.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:35:\"eleganzza-Z5344-4898-2-768x1024.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:34:\"eleganzza-Z5344-4898-2-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:1;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:34:\"eleganzza-Z5344-4898-2-600x800.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:800;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:34:\"eleganzza-Z5344-4898-2-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:34:\"eleganzza-Z5344-4898-2-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:1;}s:11:\"shop_single\";a:4:{s:4:\"file\";s:34:\"eleganzza-Z5344-4898-2-600x800.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:800;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:34:\"eleganzza-Z5344-4898-2-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(256, 63, '_thumbnail_id', '64'),
(257, 63, '_sku', ''),
(258, 63, '_regular_price', '55000'),
(259, 63, '_sale_price', '40000'),
(260, 63, '_sale_price_dates_from', ''),
(261, 63, '_sale_price_dates_to', ''),
(262, 63, 'total_sales', '5'),
(263, 63, '_tax_status', 'taxable'),
(264, 63, '_tax_class', ''),
(265, 63, '_manage_stock', 'no'),
(266, 63, '_backorders', 'no'),
(267, 63, '_sold_individually', 'no'),
(268, 63, '_weight', ''),
(269, 63, '_length', ''),
(270, 63, '_width', ''),
(271, 63, '_height', ''),
(272, 63, '_upsell_ids', 'a:0:{}'),
(273, 63, '_crosssell_ids', 'a:0:{}'),
(274, 63, '_purchase_note', ''),
(275, 63, '_default_attributes', 'a:0:{}'),
(276, 63, '_virtual', 'no'),
(277, 63, '_downloadable', 'no'),
(278, 63, '_product_image_gallery', '66,65'),
(279, 63, '_download_limit', '-1'),
(280, 63, '_download_expiry', '-1'),
(281, 63, '_stock', NULL),
(282, 63, '_stock_status', 'instock'),
(283, 63, '_product_version', '3.4.3'),
(284, 63, '_price', '40000'),
(444, 87, '_edit_last', '1'),
(445, 87, '_wp_page_template', 'default'),
(446, 87, '_edit_lock', '1531809429:1'),
(449, 92, '_menu_item_type', 'taxonomy'),
(450, 92, '_menu_item_menu_item_parent', '0'),
(451, 92, '_menu_item_object_id', '37'),
(452, 92, '_menu_item_object', 'product_cat'),
(453, 92, '_menu_item_target', ''),
(454, 92, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(455, 92, '_menu_item_xfn', ''),
(456, 92, '_menu_item_url', ''),
(460, 95, '_wc_review_count', '1'),
(461, 95, '_wc_rating_count', 'a:0:{}'),
(462, 95, '_wc_average_rating', '0'),
(463, 95, '_edit_last', '1'),
(464, 95, '_edit_lock', '1534136313:1'),
(465, 96, '_wc_review_count', '0'),
(466, 96, '_wc_rating_count', 'a:0:{}'),
(467, 96, '_wc_average_rating', '0'),
(468, 96, '_edit_last', '1'),
(469, 96, '_edit_lock', '1533530659:1'),
(470, 97, '_wc_review_count', '0'),
(471, 97, '_wc_rating_count', 'a:0:{}'),
(472, 97, '_wc_average_rating', '0'),
(473, 97, '_edit_last', '1'),
(474, 97, '_edit_lock', '1533530684:1'),
(475, 98, '_wp_attached_file', '2018/08/6096938-2.jpg'),
(476, 98, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:750;s:6:\"height\";i:750;s:4:\"file\";s:21:\"2018/08/6096938-2.jpg\";s:5:\"sizes\";a:8:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"6096938-2-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"6096938-2-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:21:\"6096938-2-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:1;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:21:\"6096938-2-600x600.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:600;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:21:\"6096938-2-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:21:\"6096938-2-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:21:\"6096938-2-600x600.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:600;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:21:\"6096938-2-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(477, 99, '_wp_attached_file', '2018/08/6096938-3.jpg'),
(478, 99, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:750;s:6:\"height\";i:750;s:4:\"file\";s:21:\"2018/08/6096938-3.jpg\";s:5:\"sizes\";a:8:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"6096938-3-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"6096938-3-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:21:\"6096938-3-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:1;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:21:\"6096938-3-600x600.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:600;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:21:\"6096938-3-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:21:\"6096938-3-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:21:\"6096938-3-600x600.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:600;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:21:\"6096938-3-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(479, 100, '_wp_attached_file', '2018/08/6096938-1.jpg'),
(480, 100, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:750;s:6:\"height\";i:750;s:4:\"file\";s:21:\"2018/08/6096938-1.jpg\";s:5:\"sizes\";a:8:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"6096938-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"6096938-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:21:\"6096938-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:1;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:21:\"6096938-1-600x600.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:600;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:21:\"6096938-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:21:\"6096938-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:21:\"6096938-1-600x600.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:600;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:21:\"6096938-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(481, 95, '_thumbnail_id', '100'),
(482, 95, '_sku', ''),
(483, 95, '_regular_price', '137000'),
(484, 95, '_sale_price', ''),
(485, 95, '_sale_price_dates_from', ''),
(486, 95, '_sale_price_dates_to', ''),
(487, 95, 'total_sales', '0'),
(488, 95, '_tax_status', 'taxable'),
(489, 95, '_tax_class', ''),
(490, 95, '_manage_stock', 'no'),
(491, 95, '_backorders', 'no'),
(492, 95, '_sold_individually', 'no'),
(493, 95, '_weight', '0.3'),
(494, 95, '_length', '20'),
(495, 95, '_width', '2'),
(496, 95, '_height', '11'),
(497, 95, '_upsell_ids', 'a:0:{}'),
(498, 95, '_crosssell_ids', 'a:0:{}'),
(499, 95, '_purchase_note', ''),
(500, 95, '_default_attributes', 'a:0:{}'),
(501, 95, '_virtual', 'no'),
(502, 95, '_downloadable', 'no'),
(503, 95, '_product_image_gallery', '99,98'),
(504, 95, '_download_limit', '-1'),
(505, 95, '_download_expiry', '-1'),
(506, 95, '_stock', NULL),
(507, 95, '_stock_status', 'outofstock'),
(508, 95, '_product_version', '3.4.4'),
(509, 95, '_price', '137000'),
(510, 101, '_wp_attached_file', '2018/08/2492737-2.jpg'),
(511, 101, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:750;s:6:\"height\";i:750;s:4:\"file\";s:21:\"2018/08/2492737-2.jpg\";s:5:\"sizes\";a:8:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"2492737-2-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"2492737-2-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:21:\"2492737-2-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:1;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:21:\"2492737-2-600x600.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:600;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:21:\"2492737-2-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:21:\"2492737-2-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:21:\"2492737-2-600x600.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:600;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:21:\"2492737-2-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(512, 102, '_wp_attached_file', '2018/08/2492737-3.jpg'),
(513, 102, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:750;s:6:\"height\";i:750;s:4:\"file\";s:21:\"2018/08/2492737-3.jpg\";s:5:\"sizes\";a:8:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"2492737-3-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"2492737-3-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:21:\"2492737-3-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:1;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:21:\"2492737-3-600x600.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:600;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:21:\"2492737-3-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:21:\"2492737-3-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:21:\"2492737-3-600x600.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:600;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:21:\"2492737-3-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(514, 103, '_wp_attached_file', '2018/08/2492737-1.jpg'),
(515, 103, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:750;s:6:\"height\";i:750;s:4:\"file\";s:21:\"2018/08/2492737-1.jpg\";s:5:\"sizes\";a:8:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"2492737-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"2492737-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:21:\"2492737-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:1;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:21:\"2492737-1-600x600.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:600;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:21:\"2492737-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:21:\"2492737-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:21:\"2492737-1-600x600.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:600;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:21:\"2492737-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(516, 96, '_thumbnail_id', '103'),
(517, 96, '_sku', ''),
(518, 96, '_regular_price', '74000'),
(519, 96, '_sale_price', ''),
(520, 96, '_sale_price_dates_from', ''),
(521, 96, '_sale_price_dates_to', ''),
(522, 96, 'total_sales', '1'),
(523, 96, '_tax_status', 'taxable'),
(524, 96, '_tax_class', ''),
(525, 96, '_manage_stock', 'no'),
(526, 96, '_backorders', 'no'),
(527, 96, '_sold_individually', 'no'),
(528, 96, '_weight', ''),
(529, 96, '_length', ''),
(530, 96, '_width', ''),
(531, 96, '_height', ''),
(532, 96, '_upsell_ids', 'a:0:{}'),
(533, 96, '_crosssell_ids', 'a:0:{}'),
(534, 96, '_purchase_note', ''),
(535, 96, '_default_attributes', 'a:0:{}'),
(536, 96, '_virtual', 'no'),
(537, 96, '_downloadable', 'no'),
(538, 96, '_product_image_gallery', '102,101'),
(539, 96, '_download_limit', '-1'),
(540, 96, '_download_expiry', '-1'),
(541, 96, '_stock', NULL),
(542, 96, '_stock_status', 'instock'),
(543, 96, '_product_version', '3.4.4'),
(544, 96, '_price', '74000'),
(545, 104, '_wp_attached_file', '2018/08/6096989-3.jpg'),
(546, 104, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:750;s:6:\"height\";i:750;s:4:\"file\";s:21:\"2018/08/6096989-3.jpg\";s:5:\"sizes\";a:8:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"6096989-3-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"6096989-3-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:21:\"6096989-3-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:1;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:21:\"6096989-3-600x600.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:600;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:21:\"6096989-3-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:21:\"6096989-3-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:21:\"6096989-3-600x600.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:600;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:21:\"6096989-3-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(547, 105, '_wp_attached_file', '2018/08/6096989-1.jpg'),
(548, 105, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:750;s:6:\"height\";i:750;s:4:\"file\";s:21:\"2018/08/6096989-1.jpg\";s:5:\"sizes\";a:8:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"6096989-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"6096989-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:21:\"6096989-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:1;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:21:\"6096989-1-600x600.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:600;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:21:\"6096989-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:21:\"6096989-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:21:\"6096989-1-600x600.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:600;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:21:\"6096989-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(549, 106, '_wp_attached_file', '2018/08/6096989-2.jpg'),
(550, 106, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:750;s:6:\"height\";i:750;s:4:\"file\";s:21:\"2018/08/6096989-2.jpg\";s:5:\"sizes\";a:8:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"6096989-2-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"6096989-2-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:21:\"6096989-2-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:1;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:21:\"6096989-2-600x600.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:600;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:21:\"6096989-2-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:21:\"6096989-2-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:21:\"6096989-2-600x600.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:600;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:21:\"6096989-2-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(551, 97, '_thumbnail_id', '105'),
(552, 97, '_sku', ''),
(553, 97, '_regular_price', '81000'),
(554, 97, '_sale_price', ''),
(555, 97, '_sale_price_dates_from', ''),
(556, 97, '_sale_price_dates_to', ''),
(557, 97, 'total_sales', '3'),
(558, 97, '_tax_status', 'taxable'),
(559, 97, '_tax_class', ''),
(560, 97, '_manage_stock', 'no'),
(561, 97, '_backorders', 'no'),
(562, 97, '_sold_individually', 'no'),
(563, 97, '_weight', ''),
(564, 97, '_length', ''),
(565, 97, '_width', ''),
(566, 97, '_height', ''),
(567, 97, '_upsell_ids', 'a:0:{}'),
(568, 97, '_crosssell_ids', 'a:0:{}'),
(569, 97, '_purchase_note', ''),
(570, 97, '_default_attributes', 'a:0:{}'),
(571, 97, '_virtual', 'no'),
(572, 97, '_downloadable', 'no'),
(573, 97, '_product_image_gallery', '106,104'),
(574, 97, '_download_limit', '-1'),
(575, 97, '_download_expiry', '-1'),
(576, 97, '_stock', NULL),
(577, 97, '_stock_status', 'instock'),
(578, 97, '_product_version', '3.4.4'),
(579, 97, '_price', '81000'),
(582, 108, '_wp_attached_file', '2018/08/logo.png'),
(583, 108, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:269;s:6:\"height\";i:75;s:4:\"file\";s:16:\"2018/08/logo.png\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:15:\"logo-150x75.png\";s:5:\"width\";i:150;s:6:\"height\";i:75;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:15:\"logo-100x75.png\";s:5:\"width\";i:100;s:6:\"height\";i:75;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:15:\"logo-100x75.png\";s:5:\"width\";i:100;s:6:\"height\";i:75;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(586, 110, '_wp_attached_file', '2018/08/background.jpg'),
(587, 110, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1920;s:6:\"height\";i:1280;s:4:\"file\";s:22:\"2018/08/background.jpg\";s:5:\"sizes\";a:10:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"background-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"background-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:22:\"background-768x512.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:512;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:23:\"background-1024x683.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:683;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:22:\"background-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:1;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:22:\"background-600x400.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:22:\"background-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:22:\"background-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:22:\"background-600x400.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:22:\"background-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(588, 110, '_wp_attachment_is_custom_background', 'os_theme'),
(591, 115, '_menu_item_type', 'custom'),
(592, 115, '_menu_item_menu_item_parent', '0'),
(593, 115, '_menu_item_object_id', '115'),
(594, 115, '_menu_item_object', 'custom'),
(595, 115, '_menu_item_target', ''),
(596, 115, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(597, 115, '_menu_item_xfn', ''),
(598, 115, '_menu_item_url', 'http://bodyfest.kz/my-account/orders/'),
(600, 116, '_menu_item_type', 'custom'),
(601, 116, '_menu_item_menu_item_parent', '0'),
(602, 116, '_menu_item_object_id', '116'),
(603, 116, '_menu_item_object', 'custom'),
(604, 116, '_menu_item_target', ''),
(605, 116, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(606, 116, '_menu_item_xfn', ''),
(607, 116, '_menu_item_url', 'http://bodyfest.kz/my-account/edit-account/'),
(609, 117, '_menu_item_type', 'custom'),
(610, 117, '_menu_item_menu_item_parent', '0'),
(611, 117, '_menu_item_object_id', '117'),
(612, 117, '_menu_item_object', 'custom'),
(613, 117, '_menu_item_target', ''),
(614, 117, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(615, 117, '_menu_item_xfn', ''),
(616, 117, '_menu_item_url', 'http://bodyfest.kz/my-account/lost-password/'),
(618, 118, '_menu_item_type', 'custom'),
(619, 118, '_menu_item_menu_item_parent', '0'),
(620, 118, '_menu_item_object_id', '118'),
(621, 118, '_menu_item_object', 'custom'),
(622, 118, '_menu_item_target', ''),
(623, 118, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(624, 118, '_menu_item_xfn', ''),
(625, 118, '_menu_item_url', '/my_account'),
(627, 95, '_product_attributes', 'a:2:{s:8:\"pa_color\";a:6:{s:4:\"name\";s:8:\"pa_color\";s:5:\"value\";s:0:\"\";s:8:\"position\";i:0;s:10:\"is_visible\";i:1;s:12:\"is_variation\";i:0;s:11:\"is_taxonomy\";i:1;}s:11:\"pa_material\";a:6:{s:4:\"name\";s:11:\"pa_material\";s:5:\"value\";s:0:\"\";s:8:\"position\";i:1;s:10:\"is_visible\";i:1;s:12:\"is_variation\";i:0;s:11:\"is_taxonomy\";i:1;}}'),
(628, 95, 'yikes_woo_products_tabs', 'a:3:{i:0;a:3:{s:5:\"title\";s:40:\"Рекомендации по уходу\";s:2:\"id\";s:40:\"рекомендации-по-уходу\";s:7:\"content\";s:721:\"Consul laudem usu ne, eos error primis neglegentur id. Commune delicata deterruisset per ut. Sed officiis honestatis eu. Timeam graecis no vim, ad congue ornatus quo, sea ad salutatus iracundia assueverit. Nam sensibus adversarium no. Id est fuisset sapientem conceptam. Adipiscing sadipscing ad usu.\r\n\r\nNo nominavi tractatos definitiones mel, prompta neglegentur ad ius, homero inermis his an. Te atqui quaeque nec, an laoreet urbanitas posidonium his. No sea veniam delenit, sit ut numquam pericula. Vocent tacimates est ex, illud appellantur ea duo, eam discere partiendo dignissim at. Eum ridens apeirian ut, dicta putant no vis, has an primis impetus theophrastus. Nisl summo libris at eos, an eos vivendo principes.\";}i:1;a:3:{s:5:\"title\";s:30:\"Как снять размер\";s:2:\"id\";s:30:\"как-снять-размер\";s:7:\"content\";s:560:\"Lorem ipsum dolor sit amet, nibh vocent sea te. No per exerci utamur omnesque. An invidunt hendrerit abhorreant est, sententiae adipiscing voluptatibus ei per. Viris vidisse omittam mei te, vis cibo sonet tollit ut. Vis omnis iisque te. Nibh clita detraxit ut mea.\r\n\r\nQuidam vocent facilis eam te, no primis torquatos duo. Brute fugit recusabo ea eum, cu accumsan albucius liberavisse eos, et dolore dolorem quo. Impedit ceteros eloquentiam ut mel. Et cum alia officiis. Quo ad delectus volutpat, eum nonumy tritani nostrum ad. Malorum volumus scaevola ut per.\";}i:2;a:3:{s:5:\"title\";s:16:\"Доставка\";s:2:\"id\";s:16:\"доставка\";s:7:\"content\";s:295:\"Quidam vocent facilis eam te, no primis torquatos duo. Brute fugit recusabo ea eum, cu accumsan albucius liberavisse eos, et dolore dolorem quo. Impedit ceteros eloquentiam ut mel.\r\n\r\nEt cum alia officiis. Quo ad delectus volutpat, eum nonumy tritani nostrum ad. Malorum volumus scaevola ut per.\";}}'),
(629, 119, '_menu_item_type', 'taxonomy'),
(630, 119, '_menu_item_menu_item_parent', '0'),
(631, 119, '_menu_item_object_id', '42'),
(632, 119, '_menu_item_object', 'category'),
(633, 119, '_menu_item_target', ''),
(634, 119, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(635, 119, '_menu_item_xfn', ''),
(636, 119, '_menu_item_url', ''),
(638, 120, '_edit_last', '1'),
(639, 120, '_edit_lock', '1534137068:1'),
(642, 122, '_edit_last', '1'),
(643, 122, '_edit_lock', '1552733779:1'),
(644, 122, '_wp_page_template', 'default'),
(645, 125, '_menu_item_type', 'post_type'),
(646, 125, '_menu_item_menu_item_parent', '0'),
(647, 125, '_menu_item_object_id', '122'),
(648, 125, '_menu_item_object', 'page'),
(649, 125, '_menu_item_target', ''),
(650, 125, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(651, 125, '_menu_item_xfn', ''),
(652, 125, '_menu_item_url', ''),
(654, 126, '_menu_item_type', 'taxonomy'),
(655, 126, '_menu_item_menu_item_parent', '0'),
(656, 126, '_menu_item_object_id', '45'),
(657, 126, '_menu_item_object', 'product_cat'),
(658, 126, '_menu_item_target', ''),
(659, 126, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(660, 126, '_menu_item_xfn', ''),
(661, 126, '_menu_item_url', ''),
(663, 127, '_menu_item_type', 'taxonomy'),
(664, 127, '_menu_item_menu_item_parent', '0'),
(665, 127, '_menu_item_object_id', '21'),
(666, 127, '_menu_item_object', 'product_cat'),
(667, 127, '_menu_item_target', ''),
(668, 127, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(669, 127, '_menu_item_xfn', ''),
(670, 127, '_menu_item_url', ''),
(672, 128, '_menu_item_type', 'taxonomy'),
(673, 128, '_menu_item_menu_item_parent', '0'),
(674, 128, '_menu_item_object_id', '47'),
(675, 128, '_menu_item_object', 'product_cat'),
(676, 128, '_menu_item_target', ''),
(677, 128, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(678, 128, '_menu_item_xfn', ''),
(679, 128, '_menu_item_url', ''),
(681, 129, '_menu_item_type', 'taxonomy'),
(682, 129, '_menu_item_menu_item_parent', '0'),
(683, 129, '_menu_item_object_id', '44'),
(684, 129, '_menu_item_object', 'product_cat'),
(685, 129, '_menu_item_target', ''),
(686, 129, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(687, 129, '_menu_item_xfn', ''),
(688, 129, '_menu_item_url', ''),
(690, 130, '_menu_item_type', 'taxonomy'),
(691, 130, '_menu_item_menu_item_parent', '0'),
(692, 130, '_menu_item_object_id', '43'),
(693, 130, '_menu_item_object', 'product_cat'),
(694, 130, '_menu_item_target', ''),
(695, 130, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(696, 130, '_menu_item_xfn', ''),
(697, 130, '_menu_item_url', ''),
(699, 131, '_menu_item_type', 'taxonomy'),
(700, 131, '_menu_item_menu_item_parent', '0'),
(701, 131, '_menu_item_object_id', '46'),
(702, 131, '_menu_item_object', 'product_cat'),
(703, 131, '_menu_item_target', ''),
(704, 131, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(705, 131, '_menu_item_xfn', ''),
(706, 131, '_menu_item_url', ''),
(708, 132, '_menu_item_type', 'taxonomy'),
(709, 132, '_menu_item_menu_item_parent', '0'),
(710, 132, '_menu_item_object_id', '49'),
(711, 132, '_menu_item_object', 'product_cat'),
(712, 132, '_menu_item_target', ''),
(713, 132, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(714, 132, '_menu_item_xfn', ''),
(715, 132, '_menu_item_url', ''),
(717, 133, '_menu_item_type', 'taxonomy'),
(718, 133, '_menu_item_menu_item_parent', '0'),
(719, 133, '_menu_item_object_id', '50'),
(720, 133, '_menu_item_object', 'product_cat'),
(721, 133, '_menu_item_target', ''),
(722, 133, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(723, 133, '_menu_item_xfn', ''),
(724, 133, '_menu_item_url', ''),
(726, 134, '_menu_item_type', 'post_type'),
(727, 134, '_menu_item_menu_item_parent', '0'),
(728, 134, '_menu_item_object_id', '45'),
(729, 134, '_menu_item_object', 'page'),
(730, 134, '_menu_item_target', ''),
(731, 134, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(732, 134, '_menu_item_xfn', ''),
(733, 134, '_menu_item_url', ''),
(735, 135, '_menu_item_type', 'post_type'),
(736, 135, '_menu_item_menu_item_parent', '0'),
(737, 135, '_menu_item_object_id', '44'),
(738, 135, '_menu_item_object', 'page'),
(739, 135, '_menu_item_target', ''),
(740, 135, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(741, 135, '_menu_item_xfn', ''),
(742, 135, '_menu_item_url', ''),
(753, 137, '_edit_last', '1'),
(754, 137, 'discount_type', 'percent'),
(755, 137, 'coupon_amount', '10'),
(756, 137, 'individual_use', 'no'),
(757, 137, 'product_ids', ''),
(758, 137, 'exclude_product_ids', ''),
(759, 137, 'usage_limit', '1'),
(760, 137, 'usage_limit_per_user', '0'),
(761, 137, 'limit_usage_to_x_items', '0'),
(762, 137, 'usage_count', '0'),
(763, 137, 'date_expires', '1534194000'),
(764, 137, 'expiry_date', '2018-08-14'),
(765, 137, 'free_shipping', 'no'),
(766, 137, 'product_categories', 'a:1:{i:0;i:37;}'),
(767, 137, 'exclude_product_categories', 'a:0:{}'),
(768, 137, 'exclude_sale_items', 'yes'),
(769, 137, 'minimum_amount', '1000'),
(770, 137, 'maximum_amount', '200000'),
(771, 137, 'customer_email', 'a:0:{}'),
(772, 137, '_edit_lock', '1534147086:1'),
(773, 138, '_wp_attached_file', '2018/08/logo-1.png');
INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(774, 138, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:328;s:6:\"height\";i:75;s:4:\"file\";s:18:\"2018/08/logo-1.png\";s:5:\"sizes\";a:6:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"logo-1-150x75.png\";s:5:\"width\";i:150;s:6:\"height\";i:75;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"logo-1-300x69.png\";s:5:\"width\";i:300;s:6:\"height\";i:69;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:17:\"logo-1-300x75.png\";s:5:\"width\";i:300;s:6:\"height\";i:75;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:1;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:17:\"logo-1-100x75.png\";s:5:\"width\";i:100;s:6:\"height\";i:75;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:17:\"logo-1-300x75.png\";s:5:\"width\";i:300;s:6:\"height\";i:75;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:17:\"logo-1-100x75.png\";s:5:\"width\";i:100;s:6:\"height\";i:75;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(786, 142, '_menu_item_type', 'taxonomy'),
(787, 142, '_menu_item_menu_item_parent', '0'),
(788, 142, '_menu_item_object_id', '19'),
(789, 142, '_menu_item_object', 'product_cat'),
(790, 142, '_menu_item_target', ''),
(791, 142, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(792, 142, '_menu_item_xfn', ''),
(793, 142, '_menu_item_url', ''),
(795, 143, '_form', '[text* your-name class:form-control \"Ваше имя\"][email* your-email class:form-control \"Ваша почта\"][tel your-tel class:form-control \"Ваш телефон\"][textarea your-message class:form-control \"Сообщение...\"]\n[submit  class:btn class:btn-default \"Отправить\"]'),
(796, 143, '_mail', 'a:9:{s:6:\"active\";b:1;s:7:\"subject\";s:36:\"Обратная связь Bodyfest\";s:6:\"sender\";s:35:\"[your-name] <wordpress@bodyfest.kz>\";s:9:\"recipient\";s:15:\"tshirts@mail.kz\";s:4:\"body\";s:191:\"От: [your-name] <[your-email]>\nТелефон: [your-tel]\n\nСообщение:\n[your-message]\n\n-- \nЭто сообщение отправлено с сайта Bodyfest (http://bodyfest.kz)\";s:18:\"additional_headers\";s:22:\"Reply-To: [your-email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:1;}'),
(797, 143, '_mail_2', 'a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:25:\"Bodyfest \"[your-subject]\"\";s:6:\"sender\";s:32:\"Bodyfest <wordpress@bodyfest.kz>\";s:9:\"recipient\";s:12:\"[your-email]\";s:4:\"body\";s:130:\"Сообщение:\n[your-message]\n\n-- \nЭто сообщение отправлено с сайта Bodyfest (http://bodyfest.kz)\";s:18:\"additional_headers\";s:20:\"Reply-To: help@ai.kz\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(798, 143, '_messages', 'a:23:{s:12:\"mail_sent_ok\";s:92:\"Спасибо за Ваше сообщение. Оно успешно отправлено.\";s:12:\"mail_sent_ng\";s:144:\"При отправке сообщения произошла ошибка. Пожалуйста, попробуйте ещё раз позже.\";s:16:\"validation_error\";s:180:\"Одно или несколько полей содержат ошибочные данные. Пожалуйста, проверьте их и попробуйте ещё раз.\";s:4:\"spam\";s:144:\"При отправке сообщения произошла ошибка. Пожалуйста, попробуйте ещё раз позже.\";s:12:\"accept_terms\";s:132:\"Вы должны принять условия и положения перед отправкой вашего сообщения.\";s:16:\"invalid_required\";s:60:\"Поле обязательно для заполнения.\";s:16:\"invalid_too_long\";s:39:\"Поле слишком длинное.\";s:17:\"invalid_too_short\";s:41:\"Поле слишком короткое.\";s:12:\"invalid_date\";s:45:\"Формат даты некорректен.\";s:14:\"date_too_early\";s:74:\"Введённая дата слишком далеко в прошлом.\";s:13:\"date_too_late\";s:74:\"Введённая дата слишком далеко в будущем.\";s:13:\"upload_failed\";s:90:\"При загрузке файла произошла неизвестная ошибка.\";s:24:\"upload_file_type_invalid\";s:81:\"Вам не разрешено загружать файлы этого типа.\";s:21:\"upload_file_too_large\";s:39:\"Файл слишком большой.\";s:23:\"upload_failed_php_error\";s:67:\"При загрузке файла произошла ошибка.\";s:14:\"invalid_number\";s:47:\"Формат числа некорректен.\";s:16:\"number_too_small\";s:68:\"Число меньше минимально допустимого.\";s:16:\"number_too_large\";s:70:\"Число больше максимально допустимого.\";s:23:\"quiz_answer_not_correct\";s:69:\"Неверный ответ на проверочный вопрос.\";s:17:\"captcha_not_match\";s:35:\"Код введен неверно.\";s:13:\"invalid_email\";s:62:\"Неверно введён электронный адрес.\";s:11:\"invalid_url\";s:53:\"Введён некорректный URL адрес.\";s:11:\"invalid_tel\";s:70:\"Введён некорректный телефонный номер.\";}'),
(799, 143, '_additional_settings', ''),
(800, 143, '_locale', 'ru_RU'),
(801, 149, '_order_key', 'wc_order_5b7a3240b66c9'),
(802, 149, '_customer_user', '3'),
(803, 149, '_payment_method', 'cod'),
(804, 149, '_payment_method_title', 'Оплата при доставке'),
(805, 149, '_transaction_id', ''),
(806, 149, '_customer_ip_address', '212.96.77.176'),
(807, 149, '_customer_user_agent', 'mozilla/5.0 (windows nt 6.1; win64; x64) applewebkit/537.36 (khtml, like gecko) chrome/68.0.3440.106 safari/537.36'),
(808, 149, '_created_via', 'checkout'),
(809, 149, '_date_completed', ''),
(810, 149, '_completed_date', ''),
(811, 149, '_date_paid', ''),
(812, 149, '_paid_date', ''),
(813, 149, '_cart_hash', '248a2a673f7cdf6bedb0af793890430c'),
(814, 149, '_billing_first_name', 'ncvn v n'),
(815, 149, '_billing_last_name', ''),
(816, 149, '_billing_company', ''),
(817, 149, '_billing_address_1', 'vn bn'),
(818, 149, '_billing_address_2', ''),
(819, 149, '_billing_city', 'актобе'),
(820, 149, '_billing_state', ''),
(821, 149, '_billing_postcode', ''),
(822, 149, '_billing_country', 'KZ'),
(823, 149, '_billing_email', 'kuzmina_231@mail.ru'),
(824, 149, '_billing_phone', '211915'),
(825, 149, '_shipping_first_name', ''),
(826, 149, '_shipping_last_name', ''),
(827, 149, '_shipping_company', ''),
(828, 149, '_shipping_address_1', ''),
(829, 149, '_shipping_address_2', ''),
(830, 149, '_shipping_city', ''),
(831, 149, '_shipping_state', ''),
(832, 149, '_shipping_postcode', ''),
(833, 149, '_shipping_country', ''),
(834, 149, '_order_currency', 'RUB'),
(835, 149, '_cart_discount', '0'),
(836, 149, '_cart_discount_tax', '0'),
(837, 149, '_order_shipping', '0'),
(838, 149, '_order_shipping_tax', '0'),
(839, 149, '_order_tax', '0'),
(840, 149, '_order_total', '20572'),
(841, 149, '_order_version', '3.4.4'),
(842, 149, '_prices_include_tax', 'no'),
(843, 149, '_billing_address_index', 'ncvn v n   vn bn  актобе   KZ kuzmina_231@mail.ru 211915'),
(844, 149, '_shipping_address_index', '        '),
(845, 149, '_download_permissions_granted', 'yes'),
(846, 149, '_recorded_sales', 'yes'),
(847, 149, '_recorded_coupon_usage_counts', 'yes'),
(848, 149, '_order_stock_reduced', 'yes'),
(849, 150, '_order_key', 'wc_order_5b7aae22dde93'),
(850, 150, '_customer_user', '3'),
(851, 150, '_payment_method', 'cod'),
(852, 150, '_payment_method_title', 'Оплата при доставке'),
(853, 150, '_transaction_id', ''),
(854, 150, '_customer_ip_address', '212.96.77.32'),
(855, 150, '_customer_user_agent', 'mozilla/5.0 (windows nt 6.1; win64; x64) applewebkit/537.36 (khtml, like gecko) chrome/68.0.3440.106 safari/537.36'),
(856, 150, '_created_via', 'checkout'),
(857, 150, '_date_completed', ''),
(858, 150, '_completed_date', ''),
(859, 150, '_date_paid', ''),
(860, 150, '_paid_date', ''),
(861, 150, '_cart_hash', '564c927dfff502dd48810dad93a616b4'),
(862, 150, '_billing_first_name', 'ncvn v n'),
(863, 150, '_billing_last_name', ''),
(864, 150, '_billing_company', ''),
(865, 150, '_billing_address_1', 'vn bn'),
(866, 150, '_billing_address_2', ''),
(867, 150, '_billing_city', 'актобе'),
(868, 150, '_billing_state', ''),
(869, 150, '_billing_postcode', ''),
(870, 150, '_billing_country', 'KZ'),
(871, 150, '_billing_email', 'kuzmina_231@mail.ru'),
(872, 150, '_billing_phone', '211915'),
(873, 150, '_shipping_first_name', ''),
(874, 150, '_shipping_last_name', ''),
(875, 150, '_shipping_company', ''),
(876, 150, '_shipping_address_1', ''),
(877, 150, '_shipping_address_2', ''),
(878, 150, '_shipping_city', ''),
(879, 150, '_shipping_state', ''),
(880, 150, '_shipping_postcode', ''),
(881, 150, '_shipping_country', ''),
(882, 150, '_order_currency', 'RUB'),
(883, 150, '_cart_discount', '0'),
(884, 150, '_cart_discount_tax', '0'),
(885, 150, '_order_shipping', '0'),
(886, 150, '_order_shipping_tax', '0'),
(887, 150, '_order_tax', '0'),
(888, 150, '_order_total', '6801'),
(889, 150, '_order_version', '3.4.4'),
(890, 150, '_prices_include_tax', 'no'),
(891, 150, '_billing_address_index', 'ncvn v n   vn bn  актобе   KZ kuzmina_231@mail.ru 211915'),
(892, 150, '_shipping_address_index', '        '),
(893, 150, '_download_permissions_granted', 'yes'),
(894, 150, '_recorded_sales', 'yes'),
(895, 150, '_recorded_coupon_usage_counts', 'yes'),
(896, 150, '_order_stock_reduced', 'yes'),
(897, 151, '_order_key', 'wc_order_5b7bece409007'),
(898, 151, '_customer_user', '2'),
(899, 151, '_payment_method', 'paybox'),
(900, 151, '_payment_method_title', 'PayBox'),
(901, 151, '_transaction_id', ''),
(902, 151, '_customer_ip_address', '176.223.184.71'),
(903, 151, '_customer_user_agent', 'mozilla/5.0 (windows nt 6.1; wow64) applewebkit/537.36 (khtml, like gecko) chrome/67.0.3396.87 safari/537.36 opr/54.0.2952.71'),
(904, 151, '_created_via', 'checkout'),
(905, 151, '_date_completed', ''),
(906, 151, '_completed_date', ''),
(907, 151, '_date_paid', ''),
(908, 151, '_paid_date', ''),
(909, 151, '_cart_hash', '54e4938c36fba9f329b91434a1af3397'),
(910, 151, '_billing_first_name', '123'),
(911, 151, '_billing_last_name', ''),
(912, 151, '_billing_company', ''),
(913, 151, '_billing_address_1', '123'),
(914, 151, '_billing_address_2', ''),
(915, 151, '_billing_city', '123'),
(916, 151, '_billing_state', ''),
(917, 151, '_billing_postcode', ''),
(918, 151, '_billing_country', 'KZ'),
(919, 151, '_billing_email', 'melan_19_86@mail.ru'),
(920, 151, '_billing_phone', '123'),
(921, 151, '_shipping_first_name', ''),
(922, 151, '_shipping_last_name', ''),
(923, 151, '_shipping_company', ''),
(924, 151, '_shipping_address_1', ''),
(925, 151, '_shipping_address_2', ''),
(926, 151, '_shipping_city', ''),
(927, 151, '_shipping_state', ''),
(928, 151, '_shipping_postcode', ''),
(929, 151, '_shipping_country', ''),
(930, 151, '_order_currency', 'KZT'),
(931, 151, '_cart_discount', '0'),
(932, 151, '_cart_discount_tax', '0'),
(933, 151, '_order_shipping', '0'),
(934, 151, '_order_shipping_tax', '0'),
(935, 151, '_order_tax', '0'),
(936, 151, '_order_total', '40000'),
(937, 151, '_order_version', '3.4.4'),
(938, 151, '_prices_include_tax', 'no'),
(939, 151, '_billing_address_index', '123   123  123   KZ melan_19_86@mail.ru 123'),
(940, 151, '_shipping_address_index', '        '),
(941, 153, '_order_key', 'wc_order_5b97ee0df0766'),
(942, 153, '_customer_user', '2'),
(943, 153, '_payment_method', 'cod'),
(944, 153, '_payment_method_title', 'Оплата при доставке'),
(945, 153, '_transaction_id', ''),
(946, 153, '_customer_ip_address', '176.223.103.22'),
(947, 153, '_customer_user_agent', 'mozilla/5.0 (windows nt 6.1; wow64) applewebkit/537.36 (khtml, like gecko) chrome/68.0.3440.106 safari/537.36 opr/55.0.2994.44'),
(948, 153, '_created_via', 'checkout'),
(949, 153, '_date_completed', ''),
(950, 153, '_completed_date', ''),
(951, 153, '_date_paid', ''),
(952, 153, '_paid_date', ''),
(953, 153, '_cart_hash', '28f833f74d2a66f554fa773f3c2331f3'),
(954, 153, '_billing_first_name', 'Азамат'),
(955, 153, '_billing_last_name', ''),
(956, 153, '_billing_company', ''),
(957, 153, '_billing_address_1', 'город Шымкет ул. Кобланды батыра 12'),
(958, 153, '_billing_address_2', ''),
(959, 153, '_billing_city', 'Шымкент'),
(960, 153, '_billing_state', ''),
(961, 153, '_billing_postcode', ''),
(962, 153, '_billing_country', 'KZ'),
(963, 153, '_billing_email', 'melan_19_86@mail.ru'),
(964, 153, '_billing_phone', '87014028065'),
(965, 153, '_shipping_first_name', ''),
(966, 153, '_shipping_last_name', ''),
(967, 153, '_shipping_company', ''),
(968, 153, '_shipping_address_1', ''),
(969, 153, '_shipping_address_2', ''),
(970, 153, '_shipping_city', ''),
(971, 153, '_shipping_state', ''),
(972, 153, '_shipping_postcode', ''),
(973, 153, '_shipping_country', ''),
(974, 153, '_order_currency', 'KZT'),
(975, 153, '_cart_discount', '0'),
(976, 153, '_cart_discount_tax', '0'),
(977, 153, '_order_shipping', '0'),
(978, 153, '_order_shipping_tax', '0'),
(979, 153, '_order_tax', '0'),
(980, 153, '_order_total', '80000'),
(981, 153, '_order_version', '3.4.4'),
(982, 153, '_prices_include_tax', 'no'),
(983, 153, '_billing_address_index', 'Азамат   город Шымкет ул. Кобланды батыра 12  Шымкент   KZ melan_19_86@mail.ru 87014028065'),
(984, 153, '_shipping_address_index', '        '),
(985, 153, '_download_permissions_granted', 'yes'),
(986, 153, '_recorded_sales', 'yes'),
(987, 153, '_recorded_coupon_usage_counts', 'yes'),
(988, 153, '_order_stock_reduced', 'yes'),
(989, 153, '_edit_lock', '1536823045:1'),
(990, 1, '_edit_lock', '1536823324:1'),
(991, 155, '_order_key', 'wc_order_5c6975c37276e'),
(992, 155, '_customer_user', '0'),
(993, 155, '_payment_method', 'cod'),
(994, 155, '_payment_method_title', 'Оплата при доставке'),
(995, 155, '_transaction_id', ''),
(996, 155, '_customer_ip_address', '37.151.204.246'),
(997, 155, '_customer_user_agent', 'mozilla/5.0 (windows nt 6.1; win64; x64) applewebkit/537.36 (khtml, like gecko) chrome/71.0.3578.98 safari/537.36'),
(998, 155, '_created_via', 'checkout'),
(999, 155, '_date_completed', ''),
(1000, 155, '_completed_date', ''),
(1001, 155, '_date_paid', ''),
(1002, 155, '_paid_date', ''),
(1003, 155, '_cart_hash', 'e21aacda5d94e0dc8223d0440af6e9b0'),
(1004, 155, '_billing_first_name', 'ncvn v n'),
(1005, 155, '_billing_last_name', ''),
(1006, 155, '_billing_company', ''),
(1007, 155, '_billing_address_1', 'vn bn'),
(1008, 155, '_billing_address_2', ''),
(1009, 155, '_billing_city', 'актобе'),
(1010, 155, '_billing_state', ''),
(1011, 155, '_billing_postcode', ''),
(1012, 155, '_billing_country', 'KZ'),
(1013, 155, '_billing_email', 'kuzmina_231@mail.ru'),
(1014, 155, '_billing_phone', '211915'),
(1015, 155, '_shipping_first_name', ''),
(1016, 155, '_shipping_last_name', ''),
(1017, 155, '_shipping_company', ''),
(1018, 155, '_shipping_address_1', ''),
(1019, 155, '_shipping_address_2', ''),
(1020, 155, '_shipping_city', ''),
(1021, 155, '_shipping_state', ''),
(1022, 155, '_shipping_postcode', ''),
(1023, 155, '_shipping_country', ''),
(1024, 155, '_order_currency', 'KZT'),
(1025, 155, '_cart_discount', '0'),
(1026, 155, '_cart_discount_tax', '0'),
(1027, 155, '_order_shipping', '0'),
(1028, 155, '_order_shipping_tax', '0'),
(1029, 155, '_order_tax', '0'),
(1030, 155, '_order_total', '195000'),
(1031, 155, '_order_version', '3.4.4'),
(1032, 155, '_prices_include_tax', 'no'),
(1033, 155, '_billing_address_index', 'ncvn v n   vn bn  актобе   KZ kuzmina_231@mail.ru 211915'),
(1034, 155, '_shipping_address_index', '        '),
(1035, 155, '_download_permissions_granted', 'yes'),
(1036, 155, '_recorded_sales', 'yes'),
(1037, 155, '_recorded_coupon_usage_counts', 'yes'),
(1038, 155, '_order_stock_reduced', 'yes'),
(1039, 156, '_order_key', 'wc_order_5c8bfe37c003a'),
(1040, 156, '_customer_user', '8'),
(1041, 156, '_payment_method', 'cod'),
(1042, 156, '_payment_method_title', 'Оплата при доставке'),
(1043, 156, '_transaction_id', ''),
(1044, 156, '_customer_ip_address', '90.143.14.168'),
(1045, 156, '_customer_user_agent', 'mozilla/5.0 (windows nt 10.0; win64; x64; rv:65.0) gecko/20100101 firefox/65.0'),
(1046, 156, '_created_via', 'checkout'),
(1047, 156, '_date_completed', ''),
(1048, 156, '_completed_date', ''),
(1049, 156, '_date_paid', ''),
(1050, 156, '_paid_date', ''),
(1051, 156, '_cart_hash', 'c6bfdc88eefcacfeda9eab4dc3fa15d0'),
(1052, 156, '_billing_first_name', 'ыфывф фывфыв'),
(1053, 156, '_billing_last_name', ''),
(1054, 156, '_billing_company', ''),
(1055, 156, '_billing_address_1', 'Некрасова 99'),
(1056, 156, '_billing_address_2', ''),
(1057, 156, '_billing_city', 'Актобе'),
(1058, 156, '_billing_state', ''),
(1059, 156, '_billing_postcode', ''),
(1060, 156, '_billing_country', 'KZ'),
(1061, 156, '_billing_email', 'dev@domain.com'),
(1062, 156, '_billing_phone', '+77715206222'),
(1063, 156, '_shipping_first_name', ''),
(1064, 156, '_shipping_last_name', ''),
(1065, 156, '_shipping_company', ''),
(1066, 156, '_shipping_address_1', ''),
(1067, 156, '_shipping_address_2', ''),
(1068, 156, '_shipping_city', ''),
(1069, 156, '_shipping_state', ''),
(1070, 156, '_shipping_postcode', ''),
(1071, 156, '_shipping_country', ''),
(1072, 156, '_order_currency', 'KZT'),
(1073, 156, '_cart_discount', '0'),
(1074, 156, '_cart_discount_tax', '0'),
(1075, 156, '_order_shipping', '0'),
(1076, 156, '_order_shipping_tax', '0'),
(1077, 156, '_order_tax', '0'),
(1078, 156, '_order_total', '81000'),
(1079, 156, '_order_version', '3.4.4'),
(1080, 156, '_prices_include_tax', 'no'),
(1081, 156, '_billing_address_index', 'ыфывф фывфыв   Некрасова 99  Актобе   KZ dev@domain.com +77715206222'),
(1082, 156, '_shipping_address_index', '        '),
(1083, 156, '_download_permissions_granted', 'yes'),
(1084, 156, '_recorded_sales', 'yes'),
(1085, 156, '_recorded_coupon_usage_counts', 'yes'),
(1086, 156, '_order_stock_reduced', 'yes'),
(1087, 157, '_order_key', 'wc_order_5c8c0153a365a'),
(1088, 157, '_customer_user', '8'),
(1089, 157, '_payment_method', 'paybox'),
(1090, 157, '_payment_method_title', 'PayBox'),
(1091, 157, '_transaction_id', ''),
(1092, 157, '_customer_ip_address', '90.143.14.168'),
(1093, 157, '_customer_user_agent', 'mozilla/5.0 (windows nt 10.0; win64; x64; rv:65.0) gecko/20100101 firefox/65.0'),
(1094, 157, '_created_via', 'checkout'),
(1095, 157, '_date_completed', ''),
(1096, 157, '_completed_date', ''),
(1097, 157, '_date_paid', ''),
(1098, 157, '_paid_date', ''),
(1099, 157, '_cart_hash', '95b84a74b05dfb483b5c3ff10616a9c2'),
(1100, 157, '_billing_first_name', 'ыфывф фывфыв'),
(1101, 157, '_billing_last_name', ''),
(1102, 157, '_billing_company', ''),
(1103, 157, '_billing_address_1', 'Некрасова 99'),
(1104, 157, '_billing_address_2', ''),
(1105, 157, '_billing_city', 'Актобе'),
(1106, 157, '_billing_state', ''),
(1107, 157, '_billing_postcode', ''),
(1108, 157, '_billing_country', 'KZ'),
(1109, 157, '_billing_email', 'dev@domain.com'),
(1110, 157, '_billing_phone', '+77715206222'),
(1111, 157, '_shipping_first_name', ''),
(1112, 157, '_shipping_last_name', ''),
(1113, 157, '_shipping_company', ''),
(1114, 157, '_shipping_address_1', ''),
(1115, 157, '_shipping_address_2', ''),
(1116, 157, '_shipping_city', ''),
(1117, 157, '_shipping_state', ''),
(1118, 157, '_shipping_postcode', ''),
(1119, 157, '_shipping_country', ''),
(1120, 157, '_order_currency', 'KZT'),
(1121, 157, '_cart_discount', '0'),
(1122, 157, '_cart_discount_tax', '0'),
(1123, 157, '_order_shipping', '0'),
(1124, 157, '_order_shipping_tax', '0'),
(1125, 157, '_order_tax', '0'),
(1126, 157, '_order_total', '162000'),
(1127, 157, '_order_version', '3.4.4'),
(1128, 157, '_prices_include_tax', 'no'),
(1129, 157, '_billing_address_index', 'ыфывф фывфыв   Некрасова 99  Актобе   KZ dev@domain.com +77715206222'),
(1130, 157, '_shipping_address_index', '        '),
(1131, 156, '_edit_lock', '1552712154:1'),
(1132, 157, '_edit_lock', '1552714402:1'),
(1133, 159, '_order_key', 'wc_order_5c8ccd93d5363'),
(1134, 159, '_customer_user', '1'),
(1135, 159, '_payment_method', 'paybox'),
(1136, 159, '_payment_method_title', 'PayBox'),
(1137, 159, '_transaction_id', ''),
(1138, 159, '_customer_ip_address', '90.143.14.168'),
(1139, 159, '_customer_user_agent', 'mozilla/5.0 (windows nt 10.0; win64; x64; rv:65.0) gecko/20100101 firefox/65.0'),
(1140, 159, '_created_via', 'checkout'),
(1141, 159, '_date_completed', ''),
(1142, 159, '_completed_date', ''),
(1143, 159, '_date_paid', ''),
(1144, 159, '_paid_date', ''),
(1145, 159, '_cart_hash', '28f833f74d2a66f554fa773f3c2331f3'),
(1146, 159, '_billing_first_name', 'ыфывф фывфыв'),
(1147, 159, '_billing_last_name', ''),
(1148, 159, '_billing_company', ''),
(1149, 159, '_billing_address_1', 'ZXZXZX'),
(1150, 159, '_billing_address_2', ''),
(1151, 159, '_billing_city', 'Актобе'),
(1152, 159, '_billing_state', ''),
(1153, 159, '_billing_postcode', ''),
(1154, 159, '_billing_country', 'KZ'),
(1155, 159, '_billing_email', 'bessonoff_a_v@mail.ru'),
(1156, 159, '_billing_phone', '+77715206222'),
(1157, 159, '_shipping_first_name', ''),
(1158, 159, '_shipping_last_name', ''),
(1159, 159, '_shipping_company', ''),
(1160, 159, '_shipping_address_1', ''),
(1161, 159, '_shipping_address_2', ''),
(1162, 159, '_shipping_city', ''),
(1163, 159, '_shipping_state', ''),
(1164, 159, '_shipping_postcode', ''),
(1165, 159, '_shipping_country', ''),
(1166, 159, '_order_currency', 'KZT'),
(1167, 159, '_cart_discount', '0'),
(1168, 159, '_cart_discount_tax', '0'),
(1169, 159, '_order_shipping', '0'),
(1170, 159, '_order_shipping_tax', '0'),
(1171, 159, '_order_tax', '0'),
(1172, 159, '_order_total', '80000'),
(1173, 159, '_order_version', '3.4.4'),
(1174, 159, '_prices_include_tax', 'no'),
(1175, 159, '_billing_address_index', 'ыфывф фывфыв   ZXZXZX  Актобе   KZ bessonoff_a_v@mail.ru +77715206222'),
(1176, 159, '_shipping_address_index', '        '),
(1177, 160, '_wc_review_count', '0'),
(1178, 160, '_wc_rating_count', 'a:0:{}'),
(1179, 160, '_wc_average_rating', '0'),
(1180, 160, '_edit_lock', '1552732655:1'),
(1181, 160, '_sku', '2221111'),
(1182, 160, '_regular_price', '2000'),
(1183, 160, '_sale_price', ''),
(1184, 160, '_sale_price_dates_from', ''),
(1185, 160, '_sale_price_dates_to', ''),
(1186, 160, 'total_sales', '1'),
(1187, 160, '_tax_status', 'taxable'),
(1188, 160, '_tax_class', ''),
(1189, 160, '_manage_stock', 'yes'),
(1190, 160, '_backorders', 'no'),
(1191, 160, '_sold_individually', 'no'),
(1192, 160, '_weight', ''),
(1193, 160, '_length', ''),
(1194, 160, '_width', ''),
(1195, 160, '_height', ''),
(1196, 160, '_upsell_ids', 'a:0:{}'),
(1197, 160, '_crosssell_ids', 'a:0:{}'),
(1198, 160, '_purchase_note', ''),
(1199, 160, '_default_attributes', 'a:0:{}'),
(1200, 160, '_virtual', 'no'),
(1201, 160, '_downloadable', 'no'),
(1202, 160, '_product_image_gallery', ''),
(1203, 160, '_download_limit', '-1'),
(1204, 160, '_download_expiry', '-1'),
(1205, 160, '_stock', '9'),
(1206, 160, '_stock_status', 'instock'),
(1207, 160, '_product_attributes', 'a:2:{s:8:\"pa_color\";a:6:{s:4:\"name\";s:8:\"pa_color\";s:5:\"value\";s:0:\"\";s:8:\"position\";i:0;s:10:\"is_visible\";i:1;s:12:\"is_variation\";i:0;s:11:\"is_taxonomy\";i:1;}s:11:\"pa_material\";a:6:{s:4:\"name\";s:11:\"pa_material\";s:5:\"value\";s:0:\"\";s:8:\"position\";i:1;s:10:\"is_visible\";i:1;s:12:\"is_variation\";i:0;s:11:\"is_taxonomy\";i:1;}}'),
(1208, 160, '_product_version', '3.4.4'),
(1209, 160, '_price', '2000'),
(1210, 160, '_edit_last', '1'),
(1211, 160, 'yikes_woo_products_tabs', 'a:0:{}'),
(1212, 161, '_order_key', 'wc_order_5c8cd1e24ed83'),
(1213, 161, '_customer_user', '1'),
(1214, 161, '_payment_method', 'cod'),
(1215, 161, '_payment_method_title', 'Оплата при доставке'),
(1216, 161, '_transaction_id', ''),
(1217, 161, '_customer_ip_address', '90.143.14.168'),
(1218, 161, '_customer_user_agent', 'mozilla/5.0 (windows nt 10.0; win64; x64; rv:65.0) gecko/20100101 firefox/65.0'),
(1219, 161, '_created_via', 'checkout'),
(1220, 161, '_date_completed', ''),
(1221, 161, '_completed_date', ''),
(1222, 161, '_date_paid', ''),
(1223, 161, '_paid_date', ''),
(1224, 161, '_cart_hash', 'b00251e389e1238ef0d630c08357e4fc'),
(1225, 161, '_billing_first_name', 'ыфывф фывфыв'),
(1226, 161, '_billing_last_name', ''),
(1227, 161, '_billing_company', ''),
(1228, 161, '_billing_address_1', 'ZXZXZX'),
(1229, 161, '_billing_address_2', ''),
(1230, 161, '_billing_city', 'Актобе'),
(1231, 161, '_billing_state', ''),
(1232, 161, '_billing_postcode', ''),
(1233, 161, '_billing_country', 'KZ'),
(1234, 161, '_billing_email', 'bessonoff_a_v@mail.ru'),
(1235, 161, '_billing_phone', '+77715206222'),
(1236, 161, '_shipping_first_name', ''),
(1237, 161, '_shipping_last_name', ''),
(1238, 161, '_shipping_company', ''),
(1239, 161, '_shipping_address_1', ''),
(1240, 161, '_shipping_address_2', ''),
(1241, 161, '_shipping_city', ''),
(1242, 161, '_shipping_state', ''),
(1243, 161, '_shipping_postcode', ''),
(1244, 161, '_shipping_country', ''),
(1245, 161, '_order_currency', 'KZT'),
(1246, 161, '_cart_discount', '0'),
(1247, 161, '_cart_discount_tax', '0'),
(1248, 161, '_order_shipping', '0'),
(1249, 161, '_order_shipping_tax', '0'),
(1250, 161, '_order_tax', '0'),
(1251, 161, '_order_total', '2000'),
(1252, 161, '_order_version', '3.4.4'),
(1253, 161, '_prices_include_tax', 'no'),
(1254, 161, '_billing_address_index', 'ыфывф фывфыв   ZXZXZX  Актобе   KZ bessonoff_a_v@mail.ru +77715206222'),
(1255, 161, '_shipping_address_index', '        '),
(1256, 161, '_download_permissions_granted', 'yes'),
(1257, 161, '_recorded_sales', 'yes'),
(1258, 161, '_recorded_coupon_usage_counts', 'yes'),
(1259, 161, '_order_stock_reduced', 'yes');

-- --------------------------------------------------------

--
-- Структура таблицы `wp_posts`
--

CREATE TABLE `wp_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext NOT NULL,
  `post_title` text NOT NULL,
  `post_excerpt` text NOT NULL,
  `post_status` varchar(20) NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) NOT NULL DEFAULT 'open',
  `post_password` varchar(255) NOT NULL DEFAULT '',
  `post_name` varchar(200) NOT NULL DEFAULT '',
  `to_ping` text NOT NULL,
  `pinged` text NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `wp_posts`
--

INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2018-07-03 14:05:24', '2018-07-03 11:05:24', 'Добро пожаловать в WordPress. Это ваша первая запись. Отредактируйте или удалите её, затем пишите!', 'Привет, мир!', '', 'publish', 'open', 'open', '', '%d0%bf%d1%80%d0%b8%d0%b2%d0%b5%d1%82-%d0%bc%d0%b8%d1%80', '', '', '2018-07-03 14:05:24', '2018-07-03 11:05:24', '', 0, 'http://bodyfest.kz/?p=1', 0, 'post', '', 1),
(2, 1, '2018-07-03 14:05:24', '2018-07-03 11:05:24', '<span class=\"h1 font-weight-bold text-uppercase d-block w-100\">Создай свой образ вместе с нами</span>\r\n\r\nЧто такое элегантность? Мы часто произносим это слово, характеризуя человека или предмет, руководствуясь каким-то внутренним ощущением, точно описать которое мы порой не в силах. Слово «элегантность» происходит от французского gant – утонченный, изысканный, изящный.\r\n\r\n<div id=\"collapseText\" class=\"collapse\">\r\nЧто же делает нашу одежду элегантной? Элегантно – не тождественно моде: элегантность всегда в моде, она вне контекста времени. Элегантность не равнозначна стилю: стиль может быть эпатажным, вызывающим, дерзким и вообще весьма далеким от элегантности. Не стоит путать также элегантность с красотой: наличие одного совершенно не обязательно предполагает другое.\r\n\r\nЭлегантный стиль это – совершенный покрой, отсутствие любых излишеств, безукоризненное качество, сдержанность и четкий силуэт. Не имеет значения, какой ценник висел на вашем польто – оно должно просто выглядеть дорого. Вы должны выглядеть так, будто вас в любой момент могут пригласить на чай к королеве Англии.\r\n\r\nЭлегантная одежда: больше, чем стиль, выше, чем мода\r\n\r\nКлассика актуальна во все времена, особенно это утверждение относится к мужской одежде. Классический стиль подчеркивает элегантность, хороший вкус и социальное положение мужчины. Такой стиль является одновременно простым и стильным. Простота проявляется в традиционном однобортном или двубортном пиджаке, неяркой и аккуратной верхней одежде, ботинках и рубашке в классическом стиле. Но если всё это хорошо скомбинировать и добавить стильные мужские аксессуары, такое сочетание позволит мужчине выглядеть модно и выдержанно.\r\n</div>\r\n<a class=\"collapseBtn font-weight-bold text-white text-uppercase d-block w-100 text-right\" data-toggle=\"collapse\" href=\"#collapseText\" role=\"button\" aria-expanded=\"false\" aria-controls=\"collapseExample\">Узнать больше</a>', 'Главная', '', 'publish', 'closed', 'open', '', 'main-page', '', '', '2018-08-10 08:39:49', '2018-08-10 05:39:49', '', 0, 'http://bodyfest.kz/?page_id=2', 0, 'page', '', 0),
(3, 1, '2018-07-03 14:05:24', '2018-07-03 11:05:24', 'Кто мы\r\n\r\nНаш адрес сайта: http://bodyfest.kz.\r\n\r\nКакие персональные данные мы собираем и с какой целью\r\n\r\nКомментарии\r\n\r\nЕсли посетитель оставляет комментарий на сайте, мы собираем данные указанные в форме комментария, а также IP адрес посетителя и данные user-agent браузера с целью определения спама.\r\n\r\nАнонимизированная строка создаваемая из вашего адреса email (\"хеш\") может предоставляться сервису Gravatar, чтобы определить используете ли вы его. Политика конфиденциальности Gravatar доступна здесь: https://automattic.com/privacy/ . После одобрения комментария ваше изображение профиля будет видимым публично в контексте вашего комментария.\r\n\r\nМедиафайлы\r\n\r\nЕсли вы зарегистрированный пользователь и загружаете фотографии на сайт, вам возможно следует избегать загрузки изображений с метаданными EXIF, так как они могут содержать данные вашего месторасположения по GPS. Посетители могут извлечь эту информацию скачав изображения с сайта.\r\n\r\nФормы контактов\r\n\r\nКуки\r\n\r\nЕсли вы оставляете комментарий на нашем сайте, вы можете включить сохранение вашего имени, адреса email и вебсайта в куки. Это делается для вашего удобства, чтобы не заполнять данные снова при повторном комментировании. Эти куки хранятся в течение одного года.\r\n\r\nЕсли у вас есть учетная запись на сайте и вы войдете в неё, мы установим временный куки для определения поддержки куки вашим браузером, куки не содержит никакой личной информации и удаляется при закрытии вашего браузера.\r\n\r\nПри входе в учетную запись мы также устанавливаем несколько куки с данными входа и настройками экрана. Куки входа хранятся в течение двух дней, куки с настройками экрана - год. Если вы выберете возможность \"Запомнить меня\", данные о входе будут сохраняться в течение двух недель. При выходе из учетной записи куки входа будут удалены.\r\n\r\nПри редактировании или публикации статьи в браузере будет сохранен дополнительный куки, он не содержит персональных данных и содержит только ID записи отредактированной вами, истекает через 1 день.\r\n\r\nВстраиваемое содержимое других вебсайтов\r\n\r\nСтатьи на этом сайте могут включать встраиваемое содержимое (например видео, изображения, статьи и др.), подобное содержимое ведет себя так же, как если бы посетитель зашел на другой сайт.\r\n\r\nЭти сайты могут собирать ваши данные, использовать куки, внедрять дополнительное отслеживание третьей стороной и следить за вашим взаимодействием с внедренным содержимым, включая отслеживание взаимодействия, если у вас есть учетная запись и вы авторизовались на том сайте.\r\n\r\nВеб-аналитика\r\n\r\nС кем мы делимся вашими данными\r\n\r\nКак долго мы храним ваши данные\r\n\r\nЕсли вы оставляете комментарий, то сам комментарий и его метаданные сохраняются неопределенно долго. Это делается для того, чтобы определять и одобрять последующие комментарии автоматически, вместо помещения их в очередь на одобрение.\r\n\r\nДля пользователей с регистрацией на нашем сайте мы храним ту личную информацию, которую они указывают в своем профиле. Все пользователи могут видеть, редактировать или удалить свою информацию из профиля в любое время (кроме имени пользователя). Администрация вебсайта также может видеть и изменять эту информацию.\r\n\r\nКакие у вас права на ваши данные\r\n\r\nПри наличии учетной записи на сайте или если вы оставляли комментарии, то вы можете запросить файл экспорта персональных данных, которые мы сохранили о вас, включая предоставленные вами данные. Вы также можете запросить удаление этих данных, это не включает данные, которые мы обязаны хранить в административных целях, по закону или целях безопасности.\r\n\r\nКуда мы отправляем ваши данные\r\n\r\nКомментарии пользователей могут проверяться автоматическим сервисом определения спама.\r\n\r\nВаша контактная информация\r\n\r\nДополнительная информация\r\n\r\nКак мы защищаем ваши данные\r\n\r\nКакие принимаются процедуры против взлома данных\r\n\r\nОт каких третьих сторон мы получаем данные\r\n\r\nКакие автоматические решения принимаются на основе данных пользователей\r\n\r\nТребования к раскрытию отраслевых нормативных требований', 'Политика конфиденциальности', '', 'publish', 'closed', 'closed', '', 'privacy-policy', '', '', '2018-07-04 09:49:03', '2018-07-04 06:49:03', '', 0, 'http://bodyfest.kz/?page_id=3', 0, 'page', '', 0),
(5, 1, '2018-07-03 14:07:05', '2018-07-03 11:07:05', '', 'background', '', 'inherit', 'open', 'closed', '', 'background', '', '', '2018-07-03 14:07:05', '2018-07-03 11:07:05', '', 0, 'http://bodyfest.kz/wp-content/uploads/2018/07/background.jpg', 0, 'attachment', 'image/jpeg', 0),
(12, 1, '2018-07-03 14:13:52', '2018-07-03 11:13:52', '', 'logo', '', 'inherit', 'open', 'closed', '', 'logo', '', '', '2018-07-03 14:13:52', '2018-07-03 11:13:52', '', 0, 'http://bodyfest.kz/wp-content/uploads/2018/07/logo.png', 0, 'attachment', 'image/png', 0),
(13, 1, '2018-07-03 14:14:08', '2018-07-03 11:14:08', 'http://bodyfest.kz/wp-content/uploads/2018/07/cropped-logo.png', 'cropped-logo.png', '', 'inherit', 'open', 'closed', '', 'cropped-logo-png', '', '', '2018-07-03 14:14:08', '2018-07-03 11:14:08', '', 0, 'http://bodyfest.kz/wp-content/uploads/2018/07/cropped-logo.png', 0, 'attachment', 'image/png', 0),
(17, 1, '2018-07-03 14:16:15', '2018-07-03 11:16:15', 'Что такое элегантность? Мы часто произносим это слово, характеризуя человека или предмет, руководствуясь каким-то внутренним ощущением, точно описать которое мы порой не в силах. Слово «элегантность» происходит от французского gant – утонченный, изысканный, изящный.\r\nЧто же делает нашу одежду элегантной? Элегантно – не тождественно моде: элегантность всегда в моде, она вне контекста времени. Элегантность не равнозначна стилю: стиль может быть эпатажным, вызывающим, дерзким и вообще весьма далеким от элегантности. Не стоит путать также элегантность с красотой: наличие одного совершенно не обязательно предполагает другое.\r\nЭлегантный стиль это – совершенный покрой, отсутствие любых излишеств, безукоризненное качество, сдержанность и четкий силуэт. Не имеет значения, какой ценник висел на вашем польто – оно должно просто выглядеть дорого. Вы должны выглядеть так, будто вас в любой момент могут пригласить на чай к королеве Англии.\r\nЭлегантная одежда: больше, чем стиль, выше, чем мода\r\n\r\nКлассика актуальна во все времена, особенно это утверждение относится к мужской одежде. Классический стиль подчеркивает элегантность, хороший вкус и социальное положение мужчины. Такой стиль является одновременно простым и стильным. Простота проявляется в традиционном однобортном или двубортном пиджаке, неяркой и аккуратной верхней одежде, ботинках и рубашке в классическом стиле. Но если всё это хорошо скомбинировать и добавить стильные мужские аксессуары, такое сочетание позволит мужчине выглядеть модно и выдержанно. \r\n', 'Главная', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2018-07-03 14:16:15', '2018-07-03 11:16:15', '', 2, 'http://bodyfest.kz/2018/07/03/2-revision-v1/', 0, 'revision', '', 0),
(18, 1, '2018-08-10 08:37:15', '2018-08-10 05:37:15', '<span class=\"h1 font-weight-bold text-uppercase d-block w-100\">Создай свой образ вместе с нами</span>\n\nЧто такое элегантность? Мы часто произносим это слово, характеризуя человека или предмет, руководствуясь каким-то внутренним ощущением, точно описать которое мы порой не в силах. Слово «элегантность» происходит от французского gant – утонченный, изысканный, изящный.\n\n<div id=\"collapseText\">\nЧто же делает нашу одежду элегантной? Элегантно – не тождественно моде: элегантность всегда в моде, она вне контекста времени. Элегантность не равнозначна стилю: стиль может быть эпатажным, вызывающим, дерзким и вообще весьма далеким от элегантности. Не стоит путать также элегантность с красотой: наличие одного совершенно не обязательно предполагает другое.\n\nЭлегантный стиль это – совершенный покрой, отсутствие любых излишеств, безукоризненное качество, сдержанность и четкий силуэт. Не имеет значения, какой ценник висел на вашем польто – оно должно просто выглядеть дорого. Вы должны выглядеть так, будто вас в любой момент могут пригласить на чай к королеве Англии.\n\nЭлегантная одежда: больше, чем стиль, выше, чем мода\n\nКлассика актуальна во все времена, особенно это утверждение относится к мужской одежде. Классический стиль подчеркивает элегантность, хороший вкус и социальное положение мужчины. Такой стиль является одновременно простым и стильным. Простота проявляется в традиционном однобортном или двубортном пиджаке, неяркой и аккуратной верхней одежде, ботинках и рубашке в классическом стиле. Но если всё это хорошо скомбинировать и добавить стильные мужские аксессуары, такое сочетание позволит мужчине выглядеть модно и выдержанно.\n</div>\n', 'Главная', '', 'inherit', 'closed', 'closed', '', '2-autosave-v1', '', '', '2018-08-10 08:37:15', '2018-08-10 05:37:15', '', 2, 'http://bodyfest.kz/2018/07/03/2-autosave-v1/', 0, 'revision', '', 0),
(19, 1, '2018-07-03 14:17:50', '2018-07-03 11:17:50', '<span class=\"display-3 font-weight-bold text-uppercase d-block w-100\">Создай свой образ вместе с нами</span>\r\n\r\nЧто такое элегантность? Мы часто произносим это слово, характеризуя человека или предмет, руководствуясь каким-то внутренним ощущением, точно описать которое мы порой не в силах. Слово «элегантность» происходит от французского gant – утонченный, изысканный, изящный.\r\nЧто же делает нашу одежду элегантной? Элегантно – не тождественно моде: элегантность всегда в моде, она вне контекста времени. Элегантность не равнозначна стилю: стиль может быть эпатажным, вызывающим, дерзким и вообще весьма далеким от элегантности. Не стоит путать также элегантность с красотой: наличие одного совершенно не обязательно предполагает другое.\r\nЭлегантный стиль это – совершенный покрой, отсутствие любых излишеств, безукоризненное качество, сдержанность и четкий силуэт. Не имеет значения, какой ценник висел на вашем польто – оно должно просто выглядеть дорого. Вы должны выглядеть так, будто вас в любой момент могут пригласить на чай к королеве Англии.\r\nЭлегантная одежда: больше, чем стиль, выше, чем мода\r\n\r\nКлассика актуальна во все времена, особенно это утверждение относится к мужской одежде. Классический стиль подчеркивает элегантность, хороший вкус и социальное положение мужчины. Такой стиль является одновременно простым и стильным. Простота проявляется в традиционном однобортном или двубортном пиджаке, неяркой и аккуратной верхней одежде, ботинках и рубашке в классическом стиле. Но если всё это хорошо скомбинировать и добавить стильные мужские аксессуары, такое сочетание позволит мужчине выглядеть модно и выдержанно. \r\n', 'Главная', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2018-07-03 14:17:50', '2018-07-03 11:17:50', '', 2, 'http://bodyfest.kz/2018/07/03/2-revision-v1/', 0, 'revision', '', 0),
(21, 1, '2018-07-03 14:25:09', '2018-07-03 11:25:09', '<span class=\"h1 font-weight-bold text-uppercase d-block w-100\">Создай свой образ вместе с нами</span>\r\n\r\nЧто такое элегантность? Мы часто произносим это слово, характеризуя человека или предмет, руководствуясь каким-то внутренним ощущением, точно описать которое мы порой не в силах. Слово «элегантность» происходит от французского gant – утонченный, изысканный, изящный.\r\nЧто же делает нашу одежду элегантной? Элегантно – не тождественно моде: элегантность всегда в моде, она вне контекста времени. Элегантность не равнозначна стилю: стиль может быть эпатажным, вызывающим, дерзким и вообще весьма далеким от элегантности. Не стоит путать также элегантность с красотой: наличие одного совершенно не обязательно предполагает другое.\r\nЭлегантный стиль это – совершенный покрой, отсутствие любых излишеств, безукоризненное качество, сдержанность и четкий силуэт. Не имеет значения, какой ценник висел на вашем польто – оно должно просто выглядеть дорого. Вы должны выглядеть так, будто вас в любой момент могут пригласить на чай к королеве Англии.\r\nЭлегантная одежда: больше, чем стиль, выше, чем мода\r\n\r\nКлассика актуальна во все времена, особенно это утверждение относится к мужской одежде. Классический стиль подчеркивает элегантность, хороший вкус и социальное положение мужчины. Такой стиль является одновременно простым и стильным. Простота проявляется в традиционном однобортном или двубортном пиджаке, неяркой и аккуратной верхней одежде, ботинках и рубашке в классическом стиле. Но если всё это хорошо скомбинировать и добавить стильные мужские аксессуары, такое сочетание позволит мужчине выглядеть модно и выдержанно. \r\n', 'Главная', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2018-07-03 14:25:09', '2018-07-03 11:25:09', '', 2, 'http://bodyfest.kz/2018/07/03/2-revision-v1/', 0, 'revision', '', 0),
(23, 1, '2018-07-04 09:44:49', '2018-07-04 06:44:49', '', 'Как сделать заказа', '', 'publish', 'closed', 'closed', '', 'kak-sdelat-zakaz', '', '', '2018-07-04 09:44:49', '2018-07-04 06:44:49', '', 0, 'http://bodyfest.kz/?page_id=23', 0, 'page', '', 0),
(24, 1, '2018-07-04 09:44:49', '2018-07-04 06:44:49', '', 'Как сделать заказа', '', 'inherit', 'closed', 'closed', '', '23-revision-v1', '', '', '2018-07-04 09:44:49', '2018-07-04 06:44:49', '', 23, 'http://bodyfest.kz/2018/07/04/23-revision-v1/', 0, 'revision', '', 0),
(25, 1, '2018-07-04 09:45:23', '2018-07-04 06:45:23', '', 'Условия доставки', '', 'publish', 'closed', 'closed', '', 'usloviya-dostavki', '', '', '2018-07-04 09:45:23', '2018-07-04 06:45:23', '', 0, 'http://bodyfest.kz/?page_id=25', 0, 'page', '', 0),
(26, 1, '2018-07-04 09:45:23', '2018-07-04 06:45:23', '', 'Условия доставки', '', 'inherit', 'closed', 'closed', '', '25-revision-v1', '', '', '2018-07-04 09:45:23', '2018-07-04 06:45:23', '', 25, 'http://bodyfest.kz/2018/07/04/25-revision-v1/', 0, 'revision', '', 0),
(27, 1, '2018-07-04 09:45:56', '2018-07-04 06:45:56', '', 'Условия возврата', '', 'publish', 'closed', 'closed', '', 'usloviya-vozvrata', '', '', '2018-07-04 09:45:56', '2018-07-04 06:45:56', '', 0, 'http://bodyfest.kz/?page_id=27', 0, 'page', '', 0),
(28, 1, '2018-07-04 09:45:56', '2018-07-04 06:45:56', '', 'Условия возврата', '', 'inherit', 'closed', 'closed', '', '27-revision-v1', '', '', '2018-07-04 09:45:56', '2018-07-04 06:45:56', '', 27, 'http://bodyfest.kz/2018/07/04/27-revision-v1/', 0, 'revision', '', 0),
(29, 1, '2018-07-04 09:46:34', '2018-07-04 06:46:34', '', 'Публичная оферта', '', 'publish', 'closed', 'closed', '', 'piblichnaya-oferta', '', '', '2018-07-04 09:46:34', '2018-07-04 06:46:34', '', 0, 'http://bodyfest.kz/?page_id=29', 0, 'page', '', 0),
(30, 1, '2018-07-04 09:46:34', '2018-07-04 06:46:34', '', 'Публичная оферта', '', 'inherit', 'closed', 'closed', '', '29-revision-v1', '', '', '2018-07-04 09:46:34', '2018-07-04 06:46:34', '', 29, 'http://bodyfest.kz/2018/07/04/29-revision-v1/', 0, 'revision', '', 0),
(31, 1, '2018-07-04 09:47:02', '2018-07-04 06:47:02', '', 'Условия использования', '', 'publish', 'closed', 'closed', '', 'usloviya-ispolzovaniya', '', '', '2018-07-04 09:47:02', '2018-07-04 06:47:02', '', 0, 'http://bodyfest.kz/?page_id=31', 0, 'page', '', 0),
(32, 1, '2018-07-04 09:47:02', '2018-07-04 06:47:02', '', 'Условия использования', '', 'inherit', 'closed', 'closed', '', '31-revision-v1', '', '', '2018-07-04 09:47:02', '2018-07-04 06:47:02', '', 31, 'http://bodyfest.kz/2018/07/04/31-revision-v1/', 0, 'revision', '', 0),
(33, 1, '2018-07-04 09:47:36', '2018-07-04 06:47:36', '', 'Как оплатить', '', 'publish', 'closed', 'closed', '', 'kak-oplatit', '', '', '2018-07-04 09:47:36', '2018-07-04 06:47:36', '', 0, 'http://bodyfest.kz/?page_id=33', 0, 'page', '', 0),
(34, 1, '2018-07-04 09:47:36', '2018-07-04 06:47:36', '', 'Как оплатить', '', 'inherit', 'closed', 'closed', '', '33-revision-v1', '', '', '2018-07-04 09:47:36', '2018-07-04 06:47:36', '', 33, 'http://bodyfest.kz/2018/07/04/33-revision-v1/', 0, 'revision', '', 0),
(35, 1, '2018-07-04 09:47:53', '2018-07-04 06:47:53', '<h2>Кто мы</h2><p>Наш адрес сайта: http://bodyfest.kz.</p><h2>Какие персональные данные мы собираем и с какой целью</h2><h3>Комментарии</h3><p>Если посетитель оставляет комментарий на сайте, мы собираем данные указанные в форме комментария, а также IP адрес посетителя и данные user-agent браузера с целью определения спама.</p><p>Анонимизированная строка создаваемая из вашего адреса email (\"хеш\") может предоставляться сервису Gravatar, чтобы определить используете ли вы его. Политика конфиденциальности Gravatar доступна здесь: https://automattic.com/privacy/ . После одобрения комментария ваше изображение профиля будет видимым публично в контексте вашего комментария.</p><h3>Медиафайлы</h3><p>Если вы зарегистрированный пользователь и загружаете фотографии на сайт, вам возможно следует избегать загрузки изображений с метаданными EXIF, так как они могут содержать данные вашего месторасположения по GPS. Посетители могут извлечь эту информацию скачав изображения с сайта.</p><h3>Формы контактов</h3><h3>Куки</h3><p>Если вы оставляете комментарий на нашем сайте, вы можете включить сохранение вашего имени, адреса email и вебсайта в куки. Это делается для вашего удобства, чтобы не заполнять данные снова при повторном комментировании. Эти куки хранятся в течение одного года.</p><p>Если у вас есть учетная запись на сайте и вы войдете в неё, мы установим временный куки для определения поддержки куки вашим браузером, куки не содержит никакой личной информации и удаляется при закрытии вашего браузера.</p><p>При входе в учетную запись мы также устанавливаем несколько куки с данными входа и настройками экрана. Куки входа хранятся в течение двух дней, куки с настройками экрана - год. Если вы выберете возможность \"Запомнить меня\", данные о входе будут сохраняться в течение двух недель. При выходе из учетной записи куки входа будут удалены.</p><p>При редактировании или публикации статьи в браузере будет сохранен дополнительный куки, он не содержит персональных данных и содержит только ID записи отредактированной вами, истекает через 1 день.</p><h3>Встраиваемое содержимое других вебсайтов</h3><p>Статьи на этом сайте могут включать встраиваемое содержимое (например видео, изображения, статьи и др.), подобное содержимое ведет себя так же, как если бы посетитель зашел на другой сайт.</p><p>Эти сайты могут собирать ваши данные, использовать куки, внедрять дополнительное отслеживание третьей стороной и следить за вашим взаимодействием с внедренным содержимым, включая отслеживание взаимодействия, если у вас есть учетная запись и вы авторизовались на том сайте.</p><h3>Веб-аналитика</h3><h2>С кем мы делимся вашими данными</h2><h2>Как долго мы храним ваши данные</h2><p>Если вы оставляете комментарий, то сам комментарий и его метаданные сохраняются неопределенно долго. Это делается для того, чтобы определять и одобрять последующие комментарии автоматически, вместо помещения их в очередь на одобрение.</p><p>Для пользователей с регистрацией на нашем сайте мы храним ту личную информацию, которую они указывают в своем профиле. Все пользователи могут видеть, редактировать или удалить свою информацию из профиля в любое время (кроме имени пользователя). Администрация вебсайта также может видеть и изменять эту информацию.</p><h2>Какие у вас права на ваши данные</h2><p>При наличии учетной записи на сайте или если вы оставляли комментарии, то вы можете запросить файл экспорта персональных данных, которые мы сохранили о вас, включая предоставленные вами данные. Вы также можете запросить удаление этих данных, это не включает данные, которые мы обязаны хранить в административных целях, по закону или целях безопасности.</p><h2>Куда мы отправляем ваши данные</h2><p>Комментарии пользователей могут проверяться автоматическим сервисом определения спама.</p><h2>Ваша контактная информация</h2><h2>Дополнительная информация</h2><h3>Как мы защищаем ваши данные</h3><h3>Какие принимаются процедуры против взлома данных</h3><h3>От каких третьих сторон мы получаем данные</h3><h3>Какие автоматические решения принимаются на основе данных пользователей</h3><h3>Требования к раскрытию отраслевых нормативных требований</h3>', 'Политика конфиденциальности', '', 'inherit', 'closed', 'closed', '', '3-revision-v1', '', '', '2018-07-04 09:47:53', '2018-07-04 06:47:53', '', 3, 'http://bodyfest.kz/2018/07/04/3-revision-v1/', 0, 'revision', '', 0),
(36, 1, '2018-07-04 09:49:03', '2018-07-04 06:49:03', 'Кто мы\r\n\r\nНаш адрес сайта: http://bodyfest.kz.\r\n\r\nКакие персональные данные мы собираем и с какой целью\r\n\r\nКомментарии\r\n\r\nЕсли посетитель оставляет комментарий на сайте, мы собираем данные указанные в форме комментария, а также IP адрес посетителя и данные user-agent браузера с целью определения спама.\r\n\r\nАнонимизированная строка создаваемая из вашего адреса email (\"хеш\") может предоставляться сервису Gravatar, чтобы определить используете ли вы его. Политика конфиденциальности Gravatar доступна здесь: https://automattic.com/privacy/ . После одобрения комментария ваше изображение профиля будет видимым публично в контексте вашего комментария.\r\n\r\nМедиафайлы\r\n\r\nЕсли вы зарегистрированный пользователь и загружаете фотографии на сайт, вам возможно следует избегать загрузки изображений с метаданными EXIF, так как они могут содержать данные вашего месторасположения по GPS. Посетители могут извлечь эту информацию скачав изображения с сайта.\r\n\r\nФормы контактов\r\n\r\nКуки\r\n\r\nЕсли вы оставляете комментарий на нашем сайте, вы можете включить сохранение вашего имени, адреса email и вебсайта в куки. Это делается для вашего удобства, чтобы не заполнять данные снова при повторном комментировании. Эти куки хранятся в течение одного года.\r\n\r\nЕсли у вас есть учетная запись на сайте и вы войдете в неё, мы установим временный куки для определения поддержки куки вашим браузером, куки не содержит никакой личной информации и удаляется при закрытии вашего браузера.\r\n\r\nПри входе в учетную запись мы также устанавливаем несколько куки с данными входа и настройками экрана. Куки входа хранятся в течение двух дней, куки с настройками экрана - год. Если вы выберете возможность \"Запомнить меня\", данные о входе будут сохраняться в течение двух недель. При выходе из учетной записи куки входа будут удалены.\r\n\r\nПри редактировании или публикации статьи в браузере будет сохранен дополнительный куки, он не содержит персональных данных и содержит только ID записи отредактированной вами, истекает через 1 день.\r\n\r\nВстраиваемое содержимое других вебсайтов\r\n\r\nСтатьи на этом сайте могут включать встраиваемое содержимое (например видео, изображения, статьи и др.), подобное содержимое ведет себя так же, как если бы посетитель зашел на другой сайт.\r\n\r\nЭти сайты могут собирать ваши данные, использовать куки, внедрять дополнительное отслеживание третьей стороной и следить за вашим взаимодействием с внедренным содержимым, включая отслеживание взаимодействия, если у вас есть учетная запись и вы авторизовались на том сайте.\r\n\r\nВеб-аналитика\r\n\r\nС кем мы делимся вашими данными\r\n\r\nКак долго мы храним ваши данные\r\n\r\nЕсли вы оставляете комментарий, то сам комментарий и его метаданные сохраняются неопределенно долго. Это делается для того, чтобы определять и одобрять последующие комментарии автоматически, вместо помещения их в очередь на одобрение.\r\n\r\nДля пользователей с регистрацией на нашем сайте мы храним ту личную информацию, которую они указывают в своем профиле. Все пользователи могут видеть, редактировать или удалить свою информацию из профиля в любое время (кроме имени пользователя). Администрация вебсайта также может видеть и изменять эту информацию.\r\n\r\nКакие у вас права на ваши данные\r\n\r\nПри наличии учетной записи на сайте или если вы оставляли комментарии, то вы можете запросить файл экспорта персональных данных, которые мы сохранили о вас, включая предоставленные вами данные. Вы также можете запросить удаление этих данных, это не включает данные, которые мы обязаны хранить в административных целях, по закону или целях безопасности.\r\n\r\nКуда мы отправляем ваши данные\r\n\r\nКомментарии пользователей могут проверяться автоматическим сервисом определения спама.\r\n\r\nВаша контактная информация\r\n\r\nДополнительная информация\r\n\r\nКак мы защищаем ваши данные\r\n\r\nКакие принимаются процедуры против взлома данных\r\n\r\nОт каких третьих сторон мы получаем данные\r\n\r\nКакие автоматические решения принимаются на основе данных пользователей\r\n\r\nТребования к раскрытию отраслевых нормативных требований', 'Политика конфиденциальности', '', 'inherit', 'closed', 'closed', '', '3-revision-v1', '', '', '2018-07-04 09:49:03', '2018-07-04 06:49:03', '', 3, 'http://bodyfest.kz/2018/07/04/3-revision-v1/', 0, 'revision', '', 0),
(37, 1, '2018-07-04 09:49:52', '2018-07-04 06:49:52', ' ', '', '', 'publish', 'closed', 'closed', '', '37', '', '', '2018-07-04 09:49:52', '2018-07-04 06:49:52', '', 0, 'http://bodyfest.kz/?p=37', 5, 'nav_menu_item', '', 0),
(38, 1, '2018-07-04 09:49:51', '2018-07-04 06:49:51', ' ', '', '', 'publish', 'closed', 'closed', '', '38', '', '', '2018-07-04 09:49:51', '2018-07-04 06:49:51', '', 0, 'http://bodyfest.kz/?p=38', 1, 'nav_menu_item', '', 0),
(39, 1, '2018-07-04 09:49:52', '2018-07-04 06:49:52', ' ', '', '', 'publish', 'closed', 'closed', '', '39', '', '', '2018-07-04 09:49:52', '2018-07-04 06:49:52', '', 0, 'http://bodyfest.kz/?p=39', 7, 'nav_menu_item', '', 0),
(40, 1, '2018-07-04 09:49:51', '2018-07-04 06:49:51', ' ', '', '', 'publish', 'closed', 'closed', '', '40', '', '', '2018-07-04 09:49:51', '2018-07-04 06:49:51', '', 0, 'http://bodyfest.kz/?p=40', 4, 'nav_menu_item', '', 0),
(41, 1, '2018-07-04 09:49:51', '2018-07-04 06:49:51', ' ', '', '', 'publish', 'closed', 'closed', '', '41', '', '', '2018-07-04 09:49:51', '2018-07-04 06:49:51', '', 0, 'http://bodyfest.kz/?p=41', 3, 'nav_menu_item', '', 0),
(42, 1, '2018-07-04 09:49:51', '2018-07-04 06:49:51', ' ', '', '', 'publish', 'closed', 'closed', '', '42', '', '', '2018-07-04 09:49:51', '2018-07-04 06:49:51', '', 0, 'http://bodyfest.kz/?p=42', 2, 'nav_menu_item', '', 0),
(43, 1, '2018-07-04 09:49:52', '2018-07-04 06:49:52', ' ', '', '', 'publish', 'closed', 'closed', '', '43', '', '', '2018-07-04 09:49:52', '2018-07-04 06:49:52', '', 0, 'http://bodyfest.kz/?p=43', 6, 'nav_menu_item', '', 0),
(44, 1, '2018-07-04 10:02:04', '2018-07-04 07:02:04', '', 'Магазин', '', 'publish', 'closed', 'closed', '', 'shop', '', '', '2018-07-04 10:02:04', '2018-07-04 07:02:04', '', 0, 'http://bodyfest.kz/shop/', 0, 'page', '', 0),
(45, 1, '2018-07-04 10:02:04', '2018-07-04 07:02:04', '[woocommerce_cart]', 'Корзина', '', 'publish', 'closed', 'closed', '', 'cart', '', '', '2018-07-04 10:02:04', '2018-07-04 07:02:04', '', 0, 'http://bodyfest.kz/cart/', 0, 'page', '', 0),
(46, 1, '2018-07-04 10:02:04', '2018-07-04 07:02:04', '[woocommerce_checkout]', 'Оформление заказа', '', 'publish', 'closed', 'closed', '', 'checkout', '', '', '2018-07-04 10:02:04', '2018-07-04 07:02:04', '', 0, 'http://bodyfest.kz/checkout/', 0, 'page', '', 0),
(47, 1, '2018-07-04 10:02:04', '2018-07-04 07:02:04', '[woocommerce_my_account]', 'Мой аккаунт', '', 'publish', 'closed', 'closed', '', 'my-account', '', '', '2018-07-04 10:02:04', '2018-07-04 07:02:04', '', 0, 'http://bodyfest.kz/my-account/', 0, 'page', '', 0),
(48, 1, '2018-07-05 10:30:28', '2018-07-05 07:30:28', ' ', '', '', 'publish', 'closed', 'closed', '', '48', '', '', '2018-08-13 12:09:50', '2018-08-13 09:09:50', '', 18, 'http://bodyfest.kz/?p=48', 4, 'nav_menu_item', '', 0),
(50, 1, '2018-07-05 10:30:28', '2018-07-05 07:30:28', ' ', '', '', 'publish', 'closed', 'closed', '', '50', '', '', '2018-08-13 12:09:50', '2018-08-13 09:09:50', '', 18, 'http://bodyfest.kz/?p=50', 3, 'nav_menu_item', '', 0),
(51, 1, '2018-07-05 10:30:28', '2018-07-05 07:30:28', ' ', '', '', 'publish', 'closed', 'closed', '', '51', '', '', '2018-08-13 12:09:50', '2018-08-13 09:09:50', '', 18, 'http://bodyfest.kz/?p=51', 6, 'nav_menu_item', '', 0),
(52, 1, '2018-07-05 10:30:28', '2018-07-05 07:30:28', ' ', '', '', 'publish', 'closed', 'closed', '', '52', '', '', '2018-08-13 12:09:50', '2018-08-13 09:09:50', '', 18, 'http://bodyfest.kz/?p=52', 1, 'nav_menu_item', '', 0),
(53, 1, '2018-07-05 10:30:28', '2018-07-05 07:30:28', ' ', '', '', 'publish', 'closed', 'closed', '', '53', '', '', '2018-08-13 12:09:50', '2018-08-13 09:09:50', '', 18, 'http://bodyfest.kz/?p=53', 5, 'nav_menu_item', '', 0),
(54, 1, '2018-07-05 10:30:28', '2018-07-05 07:30:28', ' ', '', '', 'publish', 'closed', 'closed', '', '54', '', '', '2018-08-13 12:09:50', '2018-08-13 09:09:50', '', 18, 'http://bodyfest.kz/?p=54', 2, 'nav_menu_item', '', 0),
(55, 1, '2018-07-05 10:30:28', '2018-07-05 07:30:28', ' ', '', '', 'publish', 'closed', 'closed', '', '55', '', '', '2018-08-13 12:09:50', '2018-08-13 09:09:50', '', 18, 'http://bodyfest.kz/?p=55', 7, 'nav_menu_item', '', 0),
(62, 1, '2018-07-12 07:22:09', '2018-07-12 04:22:09', '', 'romb', '', 'inherit', 'open', 'closed', '', 'romb', '', '', '2018-07-12 07:22:09', '2018-07-12 04:22:09', '', 0, 'http://bodyfest.kz/wp-content/uploads/2018/07/romb.png', 0, 'attachment', 'image/png', 0),
(63, 1, '2018-07-12 08:33:40', '2018-07-12 05:33:40', '', 'Кошелек мужской', 'Мужской кошелёк-органайзер торговой марки ELEGANZZA из натуральной кожи. Модель закрывается на металлическую молнию. Отделение для мелочи находится внутри, имеет 1 отделение и закрывается на молнию. В кошельке есть 2 отделения для крупных купюр, 2 открытых кармана и 20 отделений для кредитных и дисконтных карт. На задней стенке модели есть удобная ручка.', 'publish', 'open', 'closed', '', '%d0%ba%d0%be%d1%88%d0%b5%d0%bb%d0%b5%d0%ba-%d0%bc%d1%83%d0%b6%d1%81%d0%ba%d0%be%d0%b9', '', '', '2018-07-12 09:41:45', '2018-07-12 06:41:45', '', 0, 'http://bodyfest.kz/?post_type=product&#038;p=63', 0, 'product', '', 0),
(64, 1, '2018-07-12 08:33:08', '2018-07-12 05:33:08', '', 'eleganzza-Z5344-4898', '', 'inherit', 'open', 'closed', '', 'eleganzza-z5344-4898', '', '', '2018-07-12 08:33:08', '2018-07-12 05:33:08', '', 63, 'http://bodyfest.kz/wp-content/uploads/2018/07/eleganzza-Z5344-4898.jpg', 0, 'attachment', 'image/jpeg', 0),
(65, 1, '2018-07-12 08:33:09', '2018-07-12 05:33:09', '', 'eleganzza-Z5344-4898 (1)', '', 'inherit', 'open', 'closed', '', 'eleganzza-z5344-4898-1', '', '', '2018-07-12 08:33:09', '2018-07-12 05:33:09', '', 63, 'http://bodyfest.kz/wp-content/uploads/2018/07/eleganzza-Z5344-4898-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(66, 1, '2018-07-12 08:33:11', '2018-07-12 05:33:11', '', 'eleganzza-Z5344-4898 (2)', '', 'inherit', 'open', 'closed', '', 'eleganzza-z5344-4898-2', '', '', '2018-07-12 08:33:11', '2018-07-12 05:33:11', '', 63, 'http://bodyfest.kz/wp-content/uploads/2018/07/eleganzza-Z5344-4898-2.jpg', 0, 'attachment', 'image/jpeg', 0),
(87, 1, '2018-07-17 09:33:55', '2018-07-17 06:33:55', '[ti_wishlistsview]', 'Гардероб', '', 'publish', 'closed', 'closed', '', 'wishlist', '', '', '2018-07-17 09:37:09', '2018-07-17 06:37:09', '', 0, 'http://bodyfest.kz/?page_id=87', 0, 'page', '', 0),
(88, 1, '2018-07-17 09:33:55', '2018-07-17 06:33:55', '[ti_wishlistsview]', 'Список желаний', '', 'inherit', 'closed', 'closed', '', '87-revision-v1', '', '', '2018-07-17 09:33:55', '2018-07-17 06:33:55', '', 87, 'http://bodyfest.kz/2018/07/17/87-revision-v1/', 0, 'revision', '', 0),
(89, 1, '2018-07-17 09:37:09', '2018-07-17 06:37:09', '[ti_wishlistsview]', 'Гардероб', '', 'inherit', 'closed', 'closed', '', '87-revision-v1', '', '', '2018-07-17 09:37:09', '2018-07-17 06:37:09', '', 87, 'http://bodyfest.kz/2018/07/17/87-revision-v1/', 0, 'revision', '', 0),
(92, 1, '2018-07-20 07:15:08', '2018-07-20 04:15:08', ' ', '', '', 'publish', 'closed', 'closed', '', '92', '', '', '2018-08-13 12:09:32', '2018-08-13 09:09:32', '', 20, 'http://bodyfest.kz/?p=92', 7, 'nav_menu_item', '', 0),
(95, 1, '2018-08-06 07:46:10', '2018-08-06 04:46:10', 'Lorem ipsum dolor sit amet, nibh vocent sea te. No per exerci utamur omnesque. An invidunt hendrerit abhorreant est, sententiae adipiscing voluptatibus ei per. Viris vidisse omittam mei te, vis cibo sonet tollit ut. Vis omnis iisque te. Nibh clita detraxit ut mea.\r\n\r\nQuidam vocent facilis eam te, no primis torquatos duo. Brute fugit recusabo ea eum, cu accumsan albucius liberavisse eos, et dolore dolorem quo. Impedit ceteros eloquentiam ut mel. Et cum alia officiis. Quo ad delectus volutpat, eum nonumy tritani nostrum ad. Malorum volumus scaevola ut per.\r\n\r\nConsul laudem usu ne, eos error primis neglegentur id. Commune delicata deterruisset per ut. Sed officiis honestatis eu. Timeam graecis no vim, ad congue ornatus quo, sea ad salutatus iracundia assueverit. Nam sensibus adversarium no. Id est fuisset sapientem conceptam. Adipiscing sadipscing ad usu.\r\n\r\nNo nominavi tractatos definitiones mel, prompta neglegentur ad ius, homero inermis his an. Te atqui quaeque nec, an laoreet urbanitas posidonium his. No sea veniam delenit, sit ut numquam pericula. Vocent tacimates est ex, illud appellantur ea duo, eam discere partiendo dignissim at. Eum ridens apeirian ut, dicta putant no vis, has an primis impetus theophrastus. Nisl summo libris at eos, an eos vivendo principes.\r\n\r\nId pro oratio voluptatibus, nisl consul lucilius quo cu. Verear audire viderer sed at. Vix sonet possit ea, cu dictas persius usu, eum ea omittam blandit. Dolor probatus referrentur te mea, inani intellegam id per.', 'Кошелек из кожи крокодила, ExoticLUX', 'Lorem ipsum dolor sit amet, nibh vocent sea te. No per exerci utamur omnesque. An invidunt hendrerit abhorreant est, sententiae adipiscing voluptatibus ei per.\r\n\r\nViris vidisse omittam mei te, vis cibo sonet tollit ut. Vis omnis iisque te. Nibh clita detraxit ut mea.', 'publish', 'open', 'closed', '', '%d0%ba%d0%be%d1%88%d0%b5%d0%bb%d0%b5%d0%ba-%d0%b8%d0%b7-%d0%ba%d0%be%d0%b6%d0%b8-%d0%ba%d1%80%d0%be%d0%ba%d0%be%d0%b4%d0%b8%d0%bb%d0%b0-exoticlux', '', '', '2018-08-13 07:59:05', '2018-08-13 04:59:05', '', 0, 'http://bodyfest.kz/?post_type=product&#038;p=95', 0, 'product', '', 1),
(96, 1, '2018-08-06 07:46:38', '2018-08-06 04:46:38', '', 'Портмоне, Dr. Koffer', '', 'publish', 'open', 'closed', '', '%d0%bf%d0%be%d1%80%d1%82%d0%bc%d0%be%d0%bd%d0%b5-dr-koffer', '', '', '2018-08-06 07:46:38', '2018-08-06 04:46:38', '', 0, 'http://bodyfest.kz/?post_type=product&#038;p=96', 0, 'product', '', 0),
(97, 1, '2018-08-06 07:47:03', '2018-08-06 04:47:03', '', 'Кошелек из кожи крокодила, ExoticLUX', '', 'publish', 'open', 'closed', '', '%d0%ba%d0%be%d1%88%d0%b5%d0%bb%d0%b5%d0%ba-%d0%b8%d0%b7-%d0%ba%d0%be%d0%b6%d0%b8-%d0%ba%d1%80%d0%be%d0%ba%d0%be%d0%b4%d0%b8%d0%bb%d0%b0-exoticlux-2', '', '', '2018-08-06 07:47:03', '2018-08-06 04:47:03', '', 0, 'http://bodyfest.kz/?post_type=product&#038;p=97', 0, 'product', '', 0),
(98, 1, '2018-08-06 07:45:54', '2018-08-06 04:45:54', '', '6096938-2', '', 'inherit', 'open', 'closed', '', '6096938-2', '', '', '2018-08-06 07:45:54', '2018-08-06 04:45:54', '', 95, 'http://bodyfest.kz/wp-content/uploads/2018/08/6096938-2.jpg', 0, 'attachment', 'image/jpeg', 0),
(99, 1, '2018-08-06 07:45:56', '2018-08-06 04:45:56', '', '6096938-3', '', 'inherit', 'open', 'closed', '', '6096938-3', '', '', '2018-08-06 07:45:56', '2018-08-06 04:45:56', '', 95, 'http://bodyfest.kz/wp-content/uploads/2018/08/6096938-3.jpg', 0, 'attachment', 'image/jpeg', 0),
(100, 1, '2018-08-06 07:45:57', '2018-08-06 04:45:57', '', '6096938-1', '', 'inherit', 'open', 'closed', '', '6096938-1', '', '', '2018-08-06 07:45:57', '2018-08-06 04:45:57', '', 95, 'http://bodyfest.kz/wp-content/uploads/2018/08/6096938-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(101, 1, '2018-08-06 07:46:25', '2018-08-06 04:46:25', '', '2492737-2', '', 'inherit', 'open', 'closed', '', '2492737-2', '', '', '2018-08-06 07:46:25', '2018-08-06 04:46:25', '', 96, 'http://bodyfest.kz/wp-content/uploads/2018/08/2492737-2.jpg', 0, 'attachment', 'image/jpeg', 0),
(102, 1, '2018-08-06 07:46:26', '2018-08-06 04:46:26', '', '2492737-3', '', 'inherit', 'open', 'closed', '', '2492737-3', '', '', '2018-08-06 07:46:26', '2018-08-06 04:46:26', '', 96, 'http://bodyfest.kz/wp-content/uploads/2018/08/2492737-3.jpg', 0, 'attachment', 'image/jpeg', 0),
(103, 1, '2018-08-06 07:46:27', '2018-08-06 04:46:27', '', '2492737-1', '', 'inherit', 'open', 'closed', '', '2492737-1', '', '', '2018-08-06 07:46:27', '2018-08-06 04:46:27', '', 96, 'http://bodyfest.kz/wp-content/uploads/2018/08/2492737-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(104, 1, '2018-08-06 07:46:51', '2018-08-06 04:46:51', '', '6096989-3', '', 'inherit', 'open', 'closed', '', '6096989-3', '', '', '2018-08-06 07:46:51', '2018-08-06 04:46:51', '', 97, 'http://bodyfest.kz/wp-content/uploads/2018/08/6096989-3.jpg', 0, 'attachment', 'image/jpeg', 0),
(105, 1, '2018-08-06 07:46:52', '2018-08-06 04:46:52', '', '6096989-1', '', 'inherit', 'open', 'closed', '', '6096989-1', '', '', '2018-08-06 07:46:52', '2018-08-06 04:46:52', '', 97, 'http://bodyfest.kz/wp-content/uploads/2018/08/6096989-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(106, 1, '2018-08-06 07:46:53', '2018-08-06 04:46:53', '', '6096989-2', '', 'inherit', 'open', 'closed', '', '6096989-2', '', '', '2018-08-06 07:46:53', '2018-08-06 04:46:53', '', 97, 'http://bodyfest.kz/wp-content/uploads/2018/08/6096989-2.jpg', 0, 'attachment', 'image/jpeg', 0),
(108, 1, '2018-08-10 08:25:41', '2018-08-10 05:25:41', '', 'logo', '', 'inherit', 'open', 'closed', '', 'logo-2', '', '', '2018-08-10 08:25:41', '2018-08-10 05:25:41', '', 0, 'http://bodyfest.kz/wp-content/uploads/2018/08/logo.png', 0, 'attachment', 'image/png', 0),
(110, 1, '2018-08-10 08:33:15', '2018-08-10 05:33:15', '', 'background', '', 'inherit', 'open', 'closed', '', 'background-2', '', '', '2018-08-10 08:33:15', '2018-08-10 05:33:15', '', 0, 'http://bodyfest.kz/wp-content/uploads/2018/08/background.jpg', 0, 'attachment', 'image/jpeg', 0),
(112, 1, '2018-08-10 08:38:34', '2018-08-10 05:38:34', '<span class=\"h1 font-weight-bold text-uppercase d-block w-100\">Создай свой образ вместе с нами</span>\r\n\r\nЧто такое элегантность? Мы часто произносим это слово, характеризуя человека или предмет, руководствуясь каким-то внутренним ощущением, точно описать которое мы порой не в силах. Слово «элегантность» происходит от французского gant – утонченный, изысканный, изящный.\r\n\r\n<div id=\"collapseText\" class=\"collapse\">\r\nЧто же делает нашу одежду элегантной? Элегантно – не тождественно моде: элегантность всегда в моде, она вне контекста времени. Элегантность не равнозначна стилю: стиль может быть эпатажным, вызывающим, дерзким и вообще весьма далеким от элегантности. Не стоит путать также элегантность с красотой: наличие одного совершенно не обязательно предполагает другое.\r\n\r\nЭлегантный стиль это – совершенный покрой, отсутствие любых излишеств, безукоризненное качество, сдержанность и четкий силуэт. Не имеет значения, какой ценник висел на вашем польто – оно должно просто выглядеть дорого. Вы должны выглядеть так, будто вас в любой момент могут пригласить на чай к королеве Англии.\r\n\r\nЭлегантная одежда: больше, чем стиль, выше, чем мода\r\n\r\nКлассика актуальна во все времена, особенно это утверждение относится к мужской одежде. Классический стиль подчеркивает элегантность, хороший вкус и социальное положение мужчины. Такой стиль является одновременно простым и стильным. Простота проявляется в традиционном однобортном или двубортном пиджаке, неяркой и аккуратной верхней одежде, ботинках и рубашке в классическом стиле. Но если всё это хорошо скомбинировать и добавить стильные мужские аксессуары, такое сочетание позволит мужчине выглядеть модно и выдержанно.\r\n</div>\r\n<a class=\"collapseBtn\" data-toggle=\"collapse\" href=\"#collapseText\" role=\"button\" aria-expanded=\"false\" aria-controls=\"collapseExample\">Узнать больше</a>', 'Главная', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2018-08-10 08:38:34', '2018-08-10 05:38:34', '', 2, 'http://bodyfest.kz/2018/08/10/2-revision-v1/', 0, 'revision', '', 0),
(113, 1, '2018-08-10 08:39:29', '2018-08-10 05:39:29', '<span class=\"h1 font-weight-bold text-uppercase d-block w-100\">Создай свой образ вместе с нами</span>\r\n\r\nЧто такое элегантность? Мы часто произносим это слово, характеризуя человека или предмет, руководствуясь каким-то внутренним ощущением, точно описать которое мы порой не в силах. Слово «элегантность» происходит от французского gant – утонченный, изысканный, изящный.\r\n\r\n<div id=\"collapseText\" class=\"collapse\">\r\nЧто же делает нашу одежду элегантной? Элегантно – не тождественно моде: элегантность всегда в моде, она вне контекста времени. Элегантность не равнозначна стилю: стиль может быть эпатажным, вызывающим, дерзким и вообще весьма далеким от элегантности. Не стоит путать также элегантность с красотой: наличие одного совершенно не обязательно предполагает другое.\r\n\r\nЭлегантный стиль это – совершенный покрой, отсутствие любых излишеств, безукоризненное качество, сдержанность и четкий силуэт. Не имеет значения, какой ценник висел на вашем польто – оно должно просто выглядеть дорого. Вы должны выглядеть так, будто вас в любой момент могут пригласить на чай к королеве Англии.\r\n\r\nЭлегантная одежда: больше, чем стиль, выше, чем мода\r\n\r\nКлассика актуальна во все времена, особенно это утверждение относится к мужской одежде. Классический стиль подчеркивает элегантность, хороший вкус и социальное положение мужчины. Такой стиль является одновременно простым и стильным. Простота проявляется в традиционном однобортном или двубортном пиджаке, неяркой и аккуратной верхней одежде, ботинках и рубашке в классическом стиле. Но если всё это хорошо скомбинировать и добавить стильные мужские аксессуары, такое сочетание позволит мужчине выглядеть модно и выдержанно.\r\n</div>\r\n<a class=\"collapseBtn font-weight-bold text-white text-uppercase\" data-toggle=\"collapse\" href=\"#collapseText\" role=\"button\" aria-expanded=\"false\" aria-controls=\"collapseExample\">Узнать больше</a>', 'Главная', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2018-08-10 08:39:29', '2018-08-10 05:39:29', '', 2, 'http://bodyfest.kz/2018/08/10/2-revision-v1/', 0, 'revision', '', 0),
(114, 1, '2018-08-10 08:39:49', '2018-08-10 05:39:49', '<span class=\"h1 font-weight-bold text-uppercase d-block w-100\">Создай свой образ вместе с нами</span>\r\n\r\nЧто такое элегантность? Мы часто произносим это слово, характеризуя человека или предмет, руководствуясь каким-то внутренним ощущением, точно описать которое мы порой не в силах. Слово «элегантность» происходит от французского gant – утонченный, изысканный, изящный.\r\n\r\n<div id=\"collapseText\" class=\"collapse\">\r\nЧто же делает нашу одежду элегантной? Элегантно – не тождественно моде: элегантность всегда в моде, она вне контекста времени. Элегантность не равнозначна стилю: стиль может быть эпатажным, вызывающим, дерзким и вообще весьма далеким от элегантности. Не стоит путать также элегантность с красотой: наличие одного совершенно не обязательно предполагает другое.\r\n\r\nЭлегантный стиль это – совершенный покрой, отсутствие любых излишеств, безукоризненное качество, сдержанность и четкий силуэт. Не имеет значения, какой ценник висел на вашем польто – оно должно просто выглядеть дорого. Вы должны выглядеть так, будто вас в любой момент могут пригласить на чай к королеве Англии.\r\n\r\nЭлегантная одежда: больше, чем стиль, выше, чем мода\r\n\r\nКлассика актуальна во все времена, особенно это утверждение относится к мужской одежде. Классический стиль подчеркивает элегантность, хороший вкус и социальное положение мужчины. Такой стиль является одновременно простым и стильным. Простота проявляется в традиционном однобортном или двубортном пиджаке, неяркой и аккуратной верхней одежде, ботинках и рубашке в классическом стиле. Но если всё это хорошо скомбинировать и добавить стильные мужские аксессуары, такое сочетание позволит мужчине выглядеть модно и выдержанно.\r\n</div>\r\n<a class=\"collapseBtn font-weight-bold text-white text-uppercase d-block w-100 text-right\" data-toggle=\"collapse\" href=\"#collapseText\" role=\"button\" aria-expanded=\"false\" aria-controls=\"collapseExample\">Узнать больше</a>', 'Главная', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2018-08-10 08:39:49', '2018-08-10 05:39:49', '', 2, 'http://bodyfest.kz/2018/08/10/2-revision-v1/', 0, 'revision', '', 0),
(115, 1, '2018-08-10 08:51:04', '2018-08-10 05:51:04', '', 'Заказы', '', 'publish', 'closed', 'closed', '', '%d0%b7%d0%b0%d0%ba%d0%b0%d0%b7%d1%8b', '', '', '2018-08-10 08:51:04', '2018-08-10 05:51:04', '', 0, 'http://bodyfest.kz/?p=115', 2, 'nav_menu_item', '', 0),
(116, 1, '2018-08-10 08:51:04', '2018-08-10 05:51:04', '', 'Детали учетной записи', '', 'publish', 'closed', 'closed', '', '%d0%b4%d0%b5%d1%82%d0%b0%d0%bb%d0%b8-%d1%83%d1%87%d0%b5%d1%82%d0%bd%d0%be%d0%b9-%d0%b7%d0%b0%d0%bf%d0%b8%d1%81%d0%b8', '', '', '2018-08-10 08:51:04', '2018-08-10 05:51:04', '', 0, 'http://bodyfest.kz/?p=116', 3, 'nav_menu_item', '', 0),
(117, 1, '2018-08-10 08:51:04', '2018-08-10 05:51:04', '', 'Забыли свой пароль?', '', 'publish', 'closed', 'closed', '', '%d0%b7%d0%b0%d0%b1%d1%8b%d0%bb%d0%b8-%d1%81%d0%b2%d0%be%d0%b9-%d0%bf%d0%b0%d1%80%d0%be%d0%bb%d1%8c', '', '', '2018-08-10 08:51:04', '2018-08-10 05:51:04', '', 0, 'http://bodyfest.kz/?p=117', 4, 'nav_menu_item', '', 0),
(118, 1, '2018-08-10 08:51:04', '2018-08-10 05:51:04', '', 'Вход / Регистрация', '', 'publish', 'closed', 'closed', '', '%d0%b2%d1%85%d0%be%d0%b4-%d1%80%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b0%d1%86%d0%b8%d1%8f', '', '', '2018-08-10 08:51:04', '2018-08-10 05:51:04', '', 0, 'http://bodyfest.kz/?p=118', 1, 'nav_menu_item', '', 0),
(119, 1, '2018-08-13 07:59:53', '2018-08-13 04:59:53', ' ', '', '', 'publish', 'closed', 'closed', '', '119', '', '', '2018-08-15 14:26:02', '2018-08-15 11:26:02', '', 0, 'http://bodyfest.kz/?p=119', 2, 'nav_menu_item', '', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(120, 1, '2018-08-13 08:00:20', '2018-08-13 05:00:20', 'Quidam vocent facilis eam te, no primis torquatos duo. Brute fugit recusabo ea eum, cu accumsan albucius liberavisse eos, et dolore dolorem quo. Impedit ceteros eloquentiam ut mel. Et cum alia officiis. Quo ad delectus volutpat, eum nonumy tritani nostrum ad. Malorum volumus scaevola ut per.\r\n\r\nConsul laudem usu ne, eos error primis neglegentur id. Commune delicata deterruisset per ut. Sed officiis honestatis eu. Timeam graecis no vim, ad congue ornatus quo, sea ad salutatus iracundia assueverit. Nam sensibus adversarium no. Id est fuisset sapientem conceptam. Adipiscing sadipscing ad usu.\r\n\r\nNo nominavi tractatos definitiones mel, prompta neglegentur ad ius, homero inermis his an. Te atqui quaeque nec, an laoreet urbanitas posidonium his. No sea veniam delenit, sit ut numquam pericula. Vocent tacimates est ex, illud appellantur ea duo, eam discere partiendo dignissim at. Eum ridens apeirian ut, dicta putant no vis, has an primis impetus theophrastus. Nisl summo libris at eos, an eos vivendo principes.', 'Quidam vocent facilis eam te?', '', 'publish', 'open', 'open', '', 'quidam-vocent-facilis-eam-te', '', '', '2018-08-13 08:00:20', '2018-08-13 05:00:20', '', 0, 'http://bodyfest.kz/?p=120', 0, 'post', '', 0),
(121, 1, '2018-08-13 08:00:20', '2018-08-13 05:00:20', 'Quidam vocent facilis eam te, no primis torquatos duo. Brute fugit recusabo ea eum, cu accumsan albucius liberavisse eos, et dolore dolorem quo. Impedit ceteros eloquentiam ut mel. Et cum alia officiis. Quo ad delectus volutpat, eum nonumy tritani nostrum ad. Malorum volumus scaevola ut per.\r\n\r\nConsul laudem usu ne, eos error primis neglegentur id. Commune delicata deterruisset per ut. Sed officiis honestatis eu. Timeam graecis no vim, ad congue ornatus quo, sea ad salutatus iracundia assueverit. Nam sensibus adversarium no. Id est fuisset sapientem conceptam. Adipiscing sadipscing ad usu.\r\n\r\nNo nominavi tractatos definitiones mel, prompta neglegentur ad ius, homero inermis his an. Te atqui quaeque nec, an laoreet urbanitas posidonium his. No sea veniam delenit, sit ut numquam pericula. Vocent tacimates est ex, illud appellantur ea duo, eam discere partiendo dignissim at. Eum ridens apeirian ut, dicta putant no vis, has an primis impetus theophrastus. Nisl summo libris at eos, an eos vivendo principes.', 'Quidam vocent facilis eam te?', '', 'inherit', 'closed', 'closed', '', '120-revision-v1', '', '', '2018-08-13 08:00:20', '2018-08-13 05:00:20', '', 120, 'http://bodyfest.kz/2018/08/13/120-revision-v1/', 0, 'revision', '', 0),
(122, 1, '2018-08-13 08:28:10', '2018-08-13 05:28:10', '<strong>Адрес:</strong> Санкибай батыра 45 Б\r\n\r\n<strong>Телефон:</strong> +7 (7132) 54-62-85\r\n\r\n<strong>Почта:</strong> tshirts@mail.kz\r\n\r\n<strong>НАПИШИТЕ НАМ</strong>\r\n\r\n<div class=\"row\">\r\n<div class=\"col-lg-4\">\r\n[contact-form-7 id=\"143\" title=\"Обратная связь\"]\r\n</div>\r\n</div>', 'Контакты', '', 'publish', 'closed', 'closed', '', 'contacts', '', '', '2018-08-13 12:40:11', '2018-08-13 09:40:11', '', 0, 'http://bodyfest.kz/?page_id=122', 0, 'page', '', 0),
(123, 1, '2018-08-13 08:28:10', '2018-08-13 05:28:10', '', 'О нас', '', 'inherit', 'closed', 'closed', '', '122-revision-v1', '', '', '2018-08-13 08:28:10', '2018-08-13 05:28:10', '', 122, 'http://bodyfest.kz/2018/08/13/122-revision-v1/', 0, 'revision', '', 0),
(124, 1, '2018-08-13 08:29:11', '2018-08-13 05:29:11', '<strong>Адрес:</strong> Санкибай батыра 45 Б\r\n<strong>Телефон:</strong> +7 (7132) 54-62-85\r\n<strong>Почта:</strong> tshirts@mail.kz', 'Контакты', '', 'inherit', 'closed', 'closed', '', '122-revision-v1', '', '', '2018-08-13 08:29:11', '2018-08-13 05:29:11', '', 122, 'http://bodyfest.kz/2018/08/13/122-revision-v1/', 0, 'revision', '', 0),
(125, 1, '2018-08-13 08:30:04', '2018-08-13 05:30:04', ' ', '', '', 'publish', 'closed', 'closed', '', '125', '', '', '2018-08-15 14:26:02', '2018-08-15 11:26:02', '', 0, 'http://bodyfest.kz/?p=125', 3, 'nav_menu_item', '', 0),
(126, 1, '2018-08-13 09:12:52', '2018-08-13 06:12:52', ' ', '', '', 'publish', 'closed', 'closed', '', '126', '', '', '2018-08-13 12:09:32', '2018-08-13 09:09:32', '', 20, 'http://bodyfest.kz/?p=126', 4, 'nav_menu_item', '', 0),
(127, 1, '2018-08-13 09:12:52', '2018-08-13 06:12:52', ' ', '', '', 'publish', 'closed', 'closed', '', '127', '', '', '2018-08-13 12:09:32', '2018-08-13 09:09:32', '', 20, 'http://bodyfest.kz/?p=127', 1, 'nav_menu_item', '', 0),
(128, 1, '2018-08-13 09:12:53', '2018-08-13 06:12:53', ' ', '', '', 'publish', 'closed', 'closed', '', '128', '', '', '2018-08-13 12:09:32', '2018-08-13 09:09:32', '', 20, 'http://bodyfest.kz/?p=128', 6, 'nav_menu_item', '', 0),
(129, 1, '2018-08-13 09:12:52', '2018-08-13 06:12:52', ' ', '', '', 'publish', 'closed', 'closed', '', '129', '', '', '2018-08-13 12:09:32', '2018-08-13 09:09:32', '', 20, 'http://bodyfest.kz/?p=129', 3, 'nav_menu_item', '', 0),
(130, 1, '2018-08-13 09:12:52', '2018-08-13 06:12:52', ' ', '', '', 'publish', 'closed', 'closed', '', '130', '', '', '2018-08-13 12:09:32', '2018-08-13 09:09:32', '', 20, 'http://bodyfest.kz/?p=130', 2, 'nav_menu_item', '', 0),
(131, 1, '2018-08-13 09:12:52', '2018-08-13 06:12:52', ' ', '', '', 'publish', 'closed', 'closed', '', '131', '', '', '2018-08-13 12:09:32', '2018-08-13 09:09:32', '', 20, 'http://bodyfest.kz/?p=131', 5, 'nav_menu_item', '', 0),
(132, 1, '2018-08-13 09:13:21', '2018-08-13 06:13:21', ' ', '', '', 'publish', 'closed', 'closed', '', '132', '', '', '2018-08-13 09:13:21', '2018-08-13 06:13:21', '', 48, 'http://bodyfest.kz/?p=132', 1, 'nav_menu_item', '', 0),
(133, 1, '2018-08-13 09:13:21', '2018-08-13 06:13:21', ' ', '', '', 'publish', 'closed', 'closed', '', '133', '', '', '2018-08-13 09:13:21', '2018-08-13 06:13:21', '', 48, 'http://bodyfest.kz/?p=133', 2, 'nav_menu_item', '', 0),
(134, 1, '2018-08-13 09:26:16', '2018-08-13 06:26:16', ' ', '', '', 'publish', 'closed', 'closed', '', '134', '', '', '2018-08-15 14:26:02', '2018-08-15 11:26:02', '', 0, 'http://bodyfest.kz/?p=134', 4, 'nav_menu_item', '', 0),
(135, 1, '2018-08-13 09:56:16', '2018-08-13 06:56:16', '', 'Каталог', '', 'publish', 'closed', 'closed', '', '135', '', '', '2018-08-15 14:26:02', '2018-08-15 11:26:02', '', 0, 'http://bodyfest.kz/?p=135', 1, 'nav_menu_item', '', 0),
(137, 1, '2018-08-13 10:59:08', '2018-08-13 07:59:08', '', 'SALE10', '', 'publish', 'closed', 'closed', '', '137', '', '', '2018-08-13 10:59:19', '2018-08-13 07:59:19', '', 0, 'http://bodyfest.kz/?post_type=shop_coupon&#038;p=137', 0, 'shop_coupon', '', 0),
(138, 1, '2018-08-13 11:02:36', '2018-08-13 08:02:36', '', 'logo', '', 'inherit', 'open', 'closed', '', 'logo-3', '', '', '2018-08-13 11:02:36', '2018-08-13 08:02:36', '', 0, 'http://bodyfest.kz/wp-content/uploads/2018/08/logo-1.png', 0, 'attachment', 'image/png', 0),
(142, 1, '2018-08-13 12:09:50', '2018-08-13 09:09:50', ' ', '', '', 'publish', 'closed', 'closed', '', '142', '', '', '2018-08-13 12:09:50', '2018-08-13 09:09:50', '', 18, 'http://bodyfest.kz/?p=142', 8, 'nav_menu_item', '', 0),
(143, 1, '2018-08-13 12:26:42', '2018-08-13 09:26:42', '[text* your-name class:form-control \"Ваше имя\"][email* your-email class:form-control \"Ваша почта\"][tel your-tel class:form-control \"Ваш телефон\"][textarea your-message class:form-control \"Сообщение...\"]\r\n[submit  class:btn class:btn-default \"Отправить\"]\n1\nОбратная связь Bodyfest\n[your-name] <wordpress@bodyfest.kz>\ntshirts@mail.kz\nОт: [your-name] <[your-email]>\r\nТелефон: [your-tel]\r\n\r\nСообщение:\r\n[your-message]\r\n\r\n-- \r\nЭто сообщение отправлено с сайта Bodyfest (http://bodyfest.kz)\nReply-To: [your-email]\n\n\n1\n\nBodyfest \"[your-subject]\"\nBodyfest <wordpress@bodyfest.kz>\n[your-email]\nСообщение:\r\n[your-message]\r\n\r\n-- \r\nЭто сообщение отправлено с сайта Bodyfest (http://bodyfest.kz)\nReply-To: help@ai.kz\n\n\n\nСпасибо за Ваше сообщение. Оно успешно отправлено.\nПри отправке сообщения произошла ошибка. Пожалуйста, попробуйте ещё раз позже.\nОдно или несколько полей содержат ошибочные данные. Пожалуйста, проверьте их и попробуйте ещё раз.\nПри отправке сообщения произошла ошибка. Пожалуйста, попробуйте ещё раз позже.\nВы должны принять условия и положения перед отправкой вашего сообщения.\nПоле обязательно для заполнения.\nПоле слишком длинное.\nПоле слишком короткое.\nФормат даты некорректен.\nВведённая дата слишком далеко в прошлом.\nВведённая дата слишком далеко в будущем.\nПри загрузке файла произошла неизвестная ошибка.\nВам не разрешено загружать файлы этого типа.\nФайл слишком большой.\nПри загрузке файла произошла ошибка.\nФормат числа некорректен.\nЧисло меньше минимально допустимого.\nЧисло больше максимально допустимого.\nНеверный ответ на проверочный вопрос.\nКод введен неверно.\nНеверно введён электронный адрес.\nВведён некорректный URL адрес.\nВведён некорректный телефонный номер.', 'Обратная связь', '', 'publish', 'closed', 'closed', '', '%d0%ba%d0%be%d0%bd%d1%82%d0%b0%d0%ba%d1%82%d0%bd%d0%b0%d1%8f-%d1%84%d0%be%d1%80%d0%bc%d0%b0-1', '', '', '2018-09-12 07:32:24', '2018-09-12 04:32:24', '', 0, 'http://bodyfest.kz/?post_type=wpcf7_contact_form&#038;p=143', 0, 'wpcf7_contact_form', '', 0),
(144, 1, '2018-08-13 12:30:47', '2018-08-13 09:30:47', '<strong>Адрес:</strong> Санкибай батыра 45 Б\r\n<strong>Телефон:</strong> +7 (7132) 54-62-85\r\n<strong>Почта:</strong> tshirts@mail.kz\r\n\r\n[contact-form-7 id=\"143\" title=\"Обратная связь\"]', 'Контакты', '', 'inherit', 'closed', 'closed', '', '122-revision-v1', '', '', '2018-08-13 12:30:47', '2018-08-13 09:30:47', '', 122, 'http://bodyfest.kz/2018/08/13/122-revision-v1/', 0, 'revision', '', 0),
(145, 1, '2018-08-13 12:37:22', '2018-08-13 09:37:22', '<div class=\"row\">\r\n<div class=\"col-lg-8\">\r\n<strong>Адрес:</strong> Санкибай батыра 45 Б\r\n<strong>Телефон:</strong> +7 (7132) 54-62-85\r\n<strong>Почта:</strong> tshirts@mail.kz\r\n</div>\r\n<div class=\"col-lg-4\">\r\n[contact-form-7 id=\"143\" title=\"Обратная связь\"]\r\n</div>\r\n</div>', 'Контакты', '', 'inherit', 'closed', 'closed', '', '122-revision-v1', '', '', '2018-08-13 12:37:22', '2018-08-13 09:37:22', '', 122, 'http://bodyfest.kz/2018/08/13/122-revision-v1/', 0, 'revision', '', 0),
(146, 1, '2018-08-13 12:38:15', '2018-08-13 09:38:15', '<div class=\"row\">\r\n<div class=\"col-lg-4\">\r\n<strong>Адрес:</strong> Санкибай батыра 45 Б\r\n<strong>Телефон:</strong> +7 (7132) 54-62-85\r\n<strong>Почта:</strong> tshirts@mail.kz\r\n</div>\r\n<div class=\"col-lg-4\">\r\n[contact-form-7 id=\"143\" title=\"Обратная связь\"]\r\n</div>\r\n</div>', 'Контакты', '', 'inherit', 'closed', 'closed', '', '122-revision-v1', '', '', '2018-08-13 12:38:15', '2018-08-13 09:38:15', '', 122, 'http://bodyfest.kz/2018/08/13/122-revision-v1/', 0, 'revision', '', 0),
(147, 1, '2018-08-13 12:39:59', '2018-08-13 09:39:59', '<strong>Адрес:</strong> Санкибай батыра 45 Б\r\n\r\n<strong>Телефон:</strong> +7 (7132) 54-62-85\r\n\r\n<strong>Почта:</strong> tshirts@mail.kz\r\n\r\nНАПИШИТЕ НАМ\r\n\r\n<div class=\"row\">\r\n<div class=\"col-lg-4\">\r\n[contact-form-7 id=\"143\" title=\"Обратная связь\"]\r\n</div>\r\n</div>', 'Контакты', '', 'inherit', 'closed', 'closed', '', '122-revision-v1', '', '', '2018-08-13 12:39:59', '2018-08-13 09:39:59', '', 122, 'http://bodyfest.kz/2018/08/13/122-revision-v1/', 0, 'revision', '', 0),
(148, 1, '2018-08-13 12:40:11', '2018-08-13 09:40:11', '<strong>Адрес:</strong> Санкибай батыра 45 Б\r\n\r\n<strong>Телефон:</strong> +7 (7132) 54-62-85\r\n\r\n<strong>Почта:</strong> tshirts@mail.kz\r\n\r\n<strong>НАПИШИТЕ НАМ</strong>\r\n\r\n<div class=\"row\">\r\n<div class=\"col-lg-4\">\r\n[contact-form-7 id=\"143\" title=\"Обратная связь\"]\r\n</div>\r\n</div>', 'Контакты', '', 'inherit', 'closed', 'closed', '', '122-revision-v1', '', '', '2018-08-13 12:40:11', '2018-08-13 09:40:11', '', 122, 'http://bodyfest.kz/2018/08/13/122-revision-v1/', 0, 'revision', '', 0),
(149, 1, '2018-08-20 06:15:12', '2018-08-20 03:15:12', '', 'Order &ndash; Август 20, 2018 @ 06:15 ДП', '', 'wc-processing', 'open', 'closed', 'order_5b7a3240b67f4', '%d0%b7%d0%b0%d0%ba%d0%b0%d0%b7-20-aug-2018-%d0%b2-0315', '', '', '2018-08-20 06:15:12', '2018-08-20 03:15:12', '', 0, 'http://bodyfest.kz/?post_type=shop_order&#038;p=149', 0, 'shop_order', '', 1),
(150, 1, '2018-08-20 15:03:46', '2018-08-20 12:03:46', '', 'Order &ndash; Август 20, 2018 @ 03:03 ПП', '', 'wc-processing', 'open', 'closed', 'order_5b7aae22ddf6f', '%d0%b7%d0%b0%d0%ba%d0%b0%d0%b7-20-aug-2018-%d0%b2-1203', '', '', '2018-08-20 15:03:47', '2018-08-20 12:03:47', '', 0, 'http://bodyfest.kz/?post_type=shop_order&#038;p=150', 0, 'shop_order', '', 1),
(151, 1, '2018-08-21 13:43:48', '2018-08-21 10:43:48', '', 'Order &ndash; Август 21, 2018 @ 01:43 ПП', '', 'wc-cancelled', 'open', 'closed', 'order_5b7bece4090a0', '%d0%b7%d0%b0%d0%ba%d0%b0%d0%b7-21-aug-2018-%d0%b2-1043', '', '', '2018-08-21 13:50:52', '2018-08-21 10:50:52', '', 0, 'http://bodyfest.kz/?post_type=shop_order&#038;p=151', 0, 'shop_order', '', 1),
(153, 1, '2018-09-11 19:32:13', '2018-09-11 16:32:13', '', 'Order &ndash; Сентябрь 11, 2018 @ 07:32 ПП', '', 'wc-processing', 'open', 'closed', 'order_5b97ee0df086e', '%d0%97%d0%b0%d0%ba%d0%b0%d0%b7-11-sep-2018-%d0%b2-1632', '', '', '2018-09-11 19:33:14', '2018-09-11 16:33:14', '', 0, 'http://bodyfest.kz/?post_type=shop_order&#038;p=153', 0, 'shop_order', '', 1),
(155, 1, '2019-02-17 17:54:59', '2019-02-17 14:54:59', '', 'Order &ndash; Февраль 17, 2019 @ 05:54 ПП', '', 'wc-processing', 'open', 'closed', 'order_5c6975c37286a', '%d0%97%d0%b0%d0%ba%d0%b0%d0%b7-17-feb-2019-%d0%b2-1454', '', '', '2019-02-17 17:55:01', '2019-02-17 14:55:01', '', 0, 'http://bodyfest.kz/?post_type=shop_order&#038;p=155', 0, 'shop_order', '', 1),
(156, 1, '2019-03-15 22:34:15', '2019-03-15 19:34:15', '', 'Order &ndash; Март 15, 2019 @ 10:34 ПП', '', 'wc-processing', 'open', 'closed', 'order_5c8bfe37c0171', '%d0%97%d0%b0%d0%ba%d0%b0%d0%b7-15-mar-2019-%d0%b2-1934', '', '', '2019-03-15 22:34:18', '2019-03-15 19:34:18', '', 0, 'http://bodyfest.kz/?post_type=shop_order&#038;p=156', 0, 'shop_order', '', 1),
(157, 1, '2019-03-15 22:47:31', '2019-03-15 19:47:31', '', 'Order &ndash; Март 15, 2019 @ 10:47 ПП', '', 'wc-failed', 'open', 'closed', 'order_5c8c0153a37b2', '%d0%97%d0%b0%d0%ba%d0%b0%d0%b7-15-mar-2019-%d0%b2-1947', '', '', '2019-03-16 23:15:13', '2019-03-16 20:15:13', '', 0, 'http://bodyfest.kz/?post_type=shop_order&#038;p=157', 0, 'shop_order', '', 1),
(159, 1, '2019-03-16 13:18:59', '2019-03-16 10:18:59', '', 'Order &ndash; Март 16, 2019 @ 01:18 ПП', '', 'wc-failed', 'open', 'closed', 'order_5c8ccd93d547e', '%d0%97%d0%b0%d0%ba%d0%b0%d0%b7-16-mar-2019-%d0%b2-1018', '', '', '2019-03-17 13:45:25', '2019-03-17 10:45:25', '', 0, 'http://bodyfest.kz/?post_type=shop_order&#038;p=159', 0, 'shop_order', '', 1),
(160, 1, '2019-03-16 13:30:42', '2019-03-16 10:30:42', 'Краткое описание', 'Спортивный костюм', 'Краткое описание', 'publish', 'open', 'closed', '', '%d0%a1%d0%bf%d0%be%d1%80%d1%82%d0%b8%d0%b2%d0%bd%d1%8b%d0%b9-%d0%ba%d0%be%d1%81%d1%82%d1%8e%d0%bc', '', '', '2019-03-16 13:37:24', '2019-03-16 10:37:24', '', 0, 'http://bodyfest.kz/?post_type=product&#038;p=160', 0, 'product', '', 0),
(161, 1, '2019-03-16 13:37:22', '2019-03-16 10:37:22', '', 'Order &ndash; Март 16, 2019 @ 01:37 ПП', '', 'wc-processing', 'open', 'closed', 'order_5c8cd1e24ee64', '%d0%97%d0%b0%d0%ba%d0%b0%d0%b7-16-mar-2019-%d0%b2-1037', '', '', '2019-03-16 13:37:23', '2019-03-16 10:37:23', '', 0, 'http://bodyfest.kz/?post_type=shop_order&#038;p=161', 0, 'shop_order', '', 2);

-- --------------------------------------------------------

--
-- Структура таблицы `wp_queue`
--

CREATE TABLE `wp_queue` (
  `id` bigint(20) NOT NULL,
  `job` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(1) NOT NULL DEFAULT '0',
  `locked` tinyint(1) NOT NULL DEFAULT '0',
  `locked_at` datetime DEFAULT NULL,
  `available_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `wp_termmeta`
--

CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `wp_termmeta`
--

INSERT INTO `wp_termmeta` (`meta_id`, `term_id`, `meta_key`, `meta_value`) VALUES
(1, 18, 'order', '0'),
(2, 18, 'display_type', 'subcategories'),
(3, 18, 'thumbnail_id', '0'),
(4, 19, 'order', '0'),
(5, 19, 'display_type', 'subcategories'),
(6, 19, 'thumbnail_id', '0'),
(7, 20, 'order', '0'),
(8, 20, 'display_type', 'subcategories'),
(9, 20, 'thumbnail_id', '0'),
(10, 21, 'order', '0'),
(11, 21, 'display_type', 'products'),
(12, 21, 'thumbnail_id', '0'),
(13, 22, 'order', '0'),
(14, 22, 'display_type', 'products'),
(15, 22, 'thumbnail_id', '0'),
(16, 23, 'order', '0'),
(17, 23, 'display_type', 'products'),
(18, 23, 'thumbnail_id', '0'),
(19, 24, 'order', '0'),
(20, 24, 'display_type', 'products'),
(21, 24, 'thumbnail_id', '0'),
(22, 25, 'order', '0'),
(23, 25, 'display_type', ''),
(24, 25, 'thumbnail_id', '0'),
(25, 26, 'order', '0'),
(26, 26, 'display_type', 'products'),
(27, 26, 'thumbnail_id', '0'),
(28, 27, 'order', '0'),
(29, 27, 'display_type', 'products'),
(30, 27, 'thumbnail_id', '0'),
(31, 28, 'order', '0'),
(32, 28, 'display_type', 'products'),
(33, 28, 'thumbnail_id', '0'),
(49, 37, 'order', '0'),
(50, 37, 'display_type', ''),
(51, 37, 'thumbnail_id', '0'),
(52, 37, 'product_count_product_cat', '4'),
(53, 20, 'product_count_product_cat', '4'),
(54, 40, 'order_pa_color', '0'),
(55, 41, 'order_pa_material', '0'),
(56, 43, 'order', '0'),
(57, 43, 'display_type', ''),
(58, 43, 'thumbnail_id', '0'),
(59, 44, 'order', '0'),
(60, 44, 'display_type', ''),
(61, 44, 'thumbnail_id', '0'),
(62, 45, 'order', '0'),
(63, 45, 'display_type', ''),
(64, 45, 'thumbnail_id', '0'),
(65, 46, 'order', '0'),
(66, 46, 'display_type', ''),
(67, 46, 'thumbnail_id', '0'),
(68, 47, 'order', '0'),
(69, 47, 'display_type', ''),
(70, 47, 'thumbnail_id', '0'),
(71, 48, 'order', '0'),
(72, 48, 'display_type', ''),
(73, 48, 'thumbnail_id', '0'),
(74, 49, 'order', '0'),
(75, 49, 'display_type', ''),
(76, 49, 'thumbnail_id', '0'),
(77, 50, 'order', '0'),
(78, 50, 'display_type', ''),
(79, 50, 'thumbnail_id', '0'),
(80, 17, 'product_count_product_cat', '0'),
(81, 18, 'product_count_product_cat', '1'),
(82, 25, 'product_count_product_cat', '1');

-- --------------------------------------------------------

--
-- Структура таблицы `wp_terms`
--

CREATE TABLE `wp_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) NOT NULL DEFAULT '',
  `slug` varchar(200) NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `wp_terms`
--

INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Без рубрики', '%d0%b1%d0%b5%d0%b7-%d1%80%d1%83%d0%b1%d1%80%d0%b8%d0%ba%d0%b8', 0),
(2, 'header_menu', 'header_menu', 0),
(3, 'Помощь', '%d0%bf%d0%be%d0%bc%d0%be%d1%89%d1%8c', 0),
(4, 'simple', 'simple', 0),
(5, 'grouped', 'grouped', 0),
(6, 'variable', 'variable', 0),
(7, 'external', 'external', 0),
(8, 'exclude-from-search', 'exclude-from-search', 0),
(9, 'exclude-from-catalog', 'exclude-from-catalog', 0),
(10, 'featured', 'featured', 0),
(11, 'outofstock', 'outofstock', 0),
(12, 'rated-1', 'rated-1', 0),
(13, 'rated-2', 'rated-2', 0),
(14, 'rated-3', 'rated-3', 0),
(15, 'rated-4', 'rated-4', 0),
(16, 'rated-5', 'rated-5', 0),
(17, 'Без категории', 'uncategorized', 0),
(18, 'Одежда', 'clothing', 0),
(19, 'Обувь', 'footwear', 0),
(20, 'Аксессуары', 'accessories', 0),
(21, 'Головные уборы', 'hats', 0),
(22, 'Рубашки', 'shirts', 0),
(23, 'Жилеты', 'waistcoats', 0),
(24, 'Пиджаки', 'jackets', 0),
(25, 'Костюмы', 'costumes', 0),
(26, 'Брюки', 'pants', 0),
(27, 'Свитера', 'sweaters', 0),
(28, 'Нижнее белье', 'underwear', 0),
(34, 'Odejda', 'odejda', 0),
(36, 'Aksessuari', 'aksessuari', 0),
(37, 'Сумки, клатчи, ключницы', 'pouch', 0),
(38, 'katalog', 'katalog', 0),
(39, 'my_account', 'my_account', 0),
(40, 'черный', '%d1%87%d0%b5%d1%80%d0%bd%d1%8b%d0%b9', 0),
(41, 'кожа крокодила', '%d0%ba%d0%be%d0%b6%d0%b0-%d0%ba%d1%80%d0%be%d0%ba%d0%be%d0%b4%d0%b8%d0%bb%d0%b0', 0),
(42, 'Проблемы?', 'problems', 0),
(43, 'Ремни', 'belt', 0),
(44, 'Подтяжки', 'suspenders', 0),
(45, 'Галстуки', 'ties', 0),
(46, 'Шарфы', 'scarves', 0),
(47, 'Запонки', 'cufflinks', 0),
(48, 'Средства ухода', 'means-of-care', 0),
(49, 'За одеждой', 'for-clothes', 0),
(50, 'Мужская косметика', 'mens-cosmetics', 0),
(51, 'Sredstva', 'sredstva', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `wp_term_relationships`
--

CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `wp_term_relationships`
--

INSERT INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(37, 3, 0),
(38, 3, 0),
(39, 3, 0),
(40, 3, 0),
(41, 3, 0),
(42, 3, 0),
(43, 3, 0),
(48, 34, 0),
(50, 34, 0),
(51, 34, 0),
(52, 34, 0),
(53, 34, 0),
(54, 34, 0),
(55, 34, 0),
(63, 4, 0),
(63, 37, 0),
(92, 36, 0),
(95, 4, 0),
(95, 11, 0),
(95, 37, 0),
(95, 40, 0),
(95, 41, 0),
(96, 4, 0),
(96, 37, 0),
(97, 4, 0),
(97, 37, 0),
(115, 39, 0),
(116, 39, 0),
(117, 39, 0),
(118, 39, 0),
(119, 2, 0),
(120, 42, 0),
(125, 2, 0),
(126, 36, 0),
(127, 36, 0),
(128, 36, 0),
(129, 36, 0),
(130, 36, 0),
(131, 36, 0),
(132, 51, 0),
(133, 51, 0),
(134, 2, 0),
(135, 2, 0),
(142, 34, 0),
(160, 4, 0),
(160, 18, 0),
(160, 25, 0),
(160, 40, 0),
(160, 41, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `wp_term_taxonomy`
--

CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) NOT NULL DEFAULT '',
  `description` longtext NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `wp_term_taxonomy`
--

INSERT INTO `wp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 1),
(2, 2, 'nav_menu', '', 0, 4),
(3, 3, 'nav_menu', '', 0, 7),
(4, 4, 'product_type', '', 0, 5),
(5, 5, 'product_type', '', 0, 0),
(6, 6, 'product_type', '', 0, 0),
(7, 7, 'product_type', '', 0, 0),
(8, 8, 'product_visibility', '', 0, 0),
(9, 9, 'product_visibility', '', 0, 0),
(10, 10, 'product_visibility', '', 0, 0),
(11, 11, 'product_visibility', '', 0, 1),
(12, 12, 'product_visibility', '', 0, 0),
(13, 13, 'product_visibility', '', 0, 0),
(14, 14, 'product_visibility', '', 0, 0),
(15, 15, 'product_visibility', '', 0, 0),
(16, 16, 'product_visibility', '', 0, 0),
(17, 17, 'product_cat', '', 0, 0),
(18, 18, 'product_cat', '', 0, 1),
(19, 19, 'product_cat', '', 18, 0),
(20, 20, 'product_cat', '', 0, 0),
(21, 21, 'product_cat', '', 20, 0),
(22, 22, 'product_cat', '', 18, 0),
(23, 23, 'product_cat', '', 18, 0),
(24, 24, 'product_cat', '', 18, 0),
(25, 25, 'product_cat', '', 18, 1),
(26, 26, 'product_cat', '', 18, 0),
(27, 27, 'product_cat', '', 18, 0),
(28, 28, 'product_cat', '', 18, 0),
(34, 34, 'nav_menu', '', 0, 8),
(36, 36, 'nav_menu', '', 0, 7),
(37, 37, 'product_cat', '', 20, 4),
(38, 38, 'nav_menu', '', 0, 0),
(39, 39, 'nav_menu', '', 0, 4),
(40, 40, 'pa_color', '', 0, 2),
(41, 41, 'pa_material', '', 0, 2),
(42, 42, 'category', '', 0, 1),
(43, 43, 'product_cat', '', 20, 0),
(44, 44, 'product_cat', '', 20, 0),
(45, 45, 'product_cat', '', 20, 0),
(46, 46, 'product_cat', '', 20, 0),
(47, 47, 'product_cat', '', 20, 0),
(48, 48, 'product_cat', '', 0, 0),
(49, 49, 'product_cat', '', 48, 0),
(50, 50, 'product_cat', '', 48, 0),
(51, 51, 'nav_menu', '', 0, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `wp_tinvwl_items`
--

CREATE TABLE `wp_tinvwl_items` (
  `ID` int(11) NOT NULL,
  `wishlist_id` int(11) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL DEFAULT '0',
  `variation_id` int(11) NOT NULL DEFAULT '0',
  `formdata` text NOT NULL,
  `author` int(11) NOT NULL DEFAULT '0',
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `quantity` int(11) NOT NULL DEFAULT '1',
  `price` varchar(255) NOT NULL,
  `in_stock` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `wp_tinvwl_items`
--

INSERT INTO `wp_tinvwl_items` (`ID`, `wishlist_id`, `product_id`, `variation_id`, `formdata`, `author`, `date`, `quantity`, `price`, `in_stock`) VALUES
(2, 1, 63, 0, '', 1, '2018-07-17 10:01:36', 1, '40000', 1),
(5, 2, 96, 0, '', 0, '2018-08-09 15:32:54', 1, '74000', 1),
(6, 2, 63, 0, '', 0, '2018-08-09 15:32:58', 1, '40000', 1),
(7, 2, 97, 0, '', 0, '2018-08-09 15:33:00', 1, '81000', 1),
(8, 2, 95, 0, '', 0, '2018-08-09 15:33:03', 1, '137000', 1),
(10, 3, 95, 0, '', 0, '2018-08-10 19:01:21', 1, '137000', 1),
(15, 6, 63, 0, '', 3, '2018-08-16 10:33:17', 1, '6801', 1),
(17, 6, 95, 0, '', 3, '2018-08-20 14:59:16', 2, '137000', 0),
(20, 8, 95, 0, '', 0, '2018-08-23 19:09:04', 1, '137000', 0),
(21, 5, 97, 0, '', 2, '2018-09-12 19:57:45', 1, '81000', 1),
(23, 9, 97, 0, '', 4, '2018-11-19 15:20:48', 1, '81000', 1),
(27, 10, 95, 0, '', 0, '2018-12-01 23:27:18', 1, '137000', 0),
(28, 12, 95, 0, '', 6, '2019-02-18 18:41:13', 1, '137000', 0),
(29, 12, 63, 0, '', 6, '2019-02-28 12:20:51', 1, '40000', 1),
(30, 12, 96, 0, '', 6, '2019-02-28 12:20:53', 1, '74000', 1),
(31, 12, 97, 0, '', 6, '2019-02-28 12:20:57', 1, '81000', 1),
(33, 13, 95, 0, '', 0, '2019-02-28 21:48:17', 1, '23290', 0),
(34, 15, 95, 0, '', 0, '2019-04-10 07:48:57', 1, '137000', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `wp_tinvwl_lists`
--

CREATE TABLE `wp_tinvwl_lists` (
  `ID` int(11) NOT NULL,
  `author` int(11) NOT NULL DEFAULT '0',
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `title` text NOT NULL,
  `status` varchar(20) NOT NULL DEFAULT 'public',
  `type` varchar(20) NOT NULL DEFAULT 'list',
  `share_key` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `wp_tinvwl_lists`
--

INSERT INTO `wp_tinvwl_lists` (`ID`, `author`, `date`, `title`, `status`, `type`, `share_key`) VALUES
(1, 1, '2018-07-17 09:35:33', '', 'share', 'default', 'D44DDA'),
(2, 0, '2018-08-09 10:05:00', '', 'share', 'default', '501DBA'),
(3, 0, '2018-08-10 19:01:21', '', 'share', 'default', '4E12C6'),
(4, 0, '2018-08-13 10:50:39', '', 'share', 'default', '058B81'),
(5, 2, '2018-08-15 09:52:29', '', 'share', 'default', '41CCEB'),
(6, 3, '2018-08-16 10:30:05', '', 'share', 'default', '2FCFAE'),
(7, 0, '2018-08-20 13:58:34', '', 'share', 'default', '78C632'),
(8, 0, '2018-08-23 19:09:04', '', 'share', 'default', 'C1E05C'),
(9, 4, '2018-11-15 14:34:32', '', 'share', 'default', 'DF94D9'),
(10, 0, '2018-12-01 23:21:49', '', 'share', 'default', '644E4F'),
(11, 0, '2019-02-17 17:42:42', '', 'share', 'default', '2D5CF0'),
(12, 6, '2019-02-18 18:41:12', '', 'share', 'default', 'E017A2'),
(13, 0, '2019-02-28 21:44:02', '', 'share', 'default', 'CB2B7A'),
(14, 8, '2019-03-15 22:33:03', '', 'share', 'default', '029B9F'),
(15, 0, '2019-04-10 07:48:57', '', 'share', 'default', 'E15FCB');

-- --------------------------------------------------------

--
-- Структура таблицы `wp_usermeta`
--

CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `wp_usermeta`
--

INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'admin'),
(2, 1, 'first_name', 'ыфывф фывфыв'),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'wp_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'wp_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', 'wp496_privacy,theme_editor_notice,text_widget_custom_html,plugin_editor_notice'),
(15, 1, 'show_welcome_panel', '0'),
(17, 1, 'wp_dashboard_quick_press_last_post_id', '164'),
(18, 1, 'community-events-location', 'a:1:{s:2:\"ip\";s:13:\"193.193.239.0\";}'),
(19, 1, 'wp_user-settings', 'libraryContent=browse&editor=html'),
(20, 1, 'wp_user-settings-time', '1534153037'),
(21, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:\"link-target\";i:1;s:11:\"css-classes\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";i:4;s:15:\"title-attribute\";}'),
(22, 1, 'metaboxhidden_nav-menus', 'a:3:{i:0;s:22:\"add-post-type-partners\";i:1;s:12:\"add-post_tag\";i:2;s:15:\"add-post_format\";}'),
(23, 1, 'nav_menu_recently_edited', '2'),
(25, 1, 'wc_last_active', '1553731200'),
(26, 1, 'dismissed_no_secure_connection_notice', '1'),
(27, 1, 'dismissed_template_files_notice', '1'),
(28, 2, 'nickname', 'melan_19_86'),
(29, 2, 'first_name', 'Азамат'),
(30, 2, 'last_name', ''),
(31, 2, 'description', ''),
(32, 2, 'rich_editing', 'true'),
(33, 2, 'syntax_highlighting', 'true'),
(34, 2, 'comment_shortcuts', 'false'),
(35, 2, 'admin_color', 'fresh'),
(36, 2, 'use_ssl', '0'),
(37, 2, 'show_admin_bar_front', 'true'),
(38, 2, 'locale', ''),
(39, 2, 'wp_capabilities', 'a:1:{s:8:\"customer\";b:1;}'),
(40, 2, 'wp_user_level', '0'),
(43, 2, 'wc_last_active', '1536710400'),
(44, 3, 'nickname', 'kuzmina_231'),
(45, 3, 'first_name', 'ncvn v n'),
(46, 3, 'last_name', ''),
(47, 3, 'description', ''),
(48, 3, 'rich_editing', 'true'),
(49, 3, 'syntax_highlighting', 'true'),
(50, 3, 'comment_shortcuts', 'false'),
(51, 3, 'admin_color', 'fresh'),
(52, 3, 'use_ssl', '0'),
(53, 3, 'show_admin_bar_front', 'true'),
(54, 3, 'locale', ''),
(55, 3, 'wp_capabilities', 'a:1:{s:8:\"customer\";b:1;}'),
(56, 3, 'wp_user_level', '0'),
(59, 3, 'wc_last_active', '1550361600'),
(60, 3, 'last_update', '1534766626'),
(61, 3, 'billing_first_name', 'ncvn v n'),
(62, 3, 'billing_address_1', 'vn bn'),
(63, 3, 'billing_city', 'актобе'),
(64, 3, 'billing_country', 'KZ'),
(65, 3, 'billing_email', 'kuzmina_231@mail.ru'),
(66, 3, 'billing_phone', '211915'),
(70, 3, 'shipping_method', ''),
(72, 3, '_woocommerce_persistent_cart_1', 'a:1:{s:4:\"cart\";a:2:{s:32:\"03afdbd66e7929b125f8597834fa83a4\";a:11:{s:3:\"key\";s:32:\"03afdbd66e7929b125f8597834fa83a4\";s:10:\"product_id\";i:63;s:12:\"variation_id\";i:0;s:9:\"variation\";a:0:{}s:8:\"quantity\";i:1;s:9:\"data_hash\";s:32:\"b5c1d5ca8bae6d4896cf1807cdf763f0\";s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:40000;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:40000;s:8:\"line_tax\";i:0;}s:32:\"26657d5ff9020d2abefe558796b99584\";a:11:{s:3:\"key\";s:32:\"26657d5ff9020d2abefe558796b99584\";s:10:\"product_id\";i:96;s:12:\"variation_id\";i:0;s:9:\"variation\";a:0:{}s:8:\"quantity\";i:1;s:9:\"data_hash\";s:32:\"b5c1d5ca8bae6d4896cf1807cdf763f0\";s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:74000;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:74000;s:8:\"line_tax\";i:0;}}}'),
(73, 2, 'session_tokens', 'a:1:{s:64:\"c9f4990a97aa96b1052d763ae437085bd4272ac84c69957e09d99c050b488762\";a:4:{s:10:\"expiration\";i:1536855949;s:2:\"ip\";s:14:\"176.223.103.22\";s:2:\"ua\";s:126:\"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36 OPR/55.0.2994.44\";s:5:\"login\";i:1536683149;}}'),
(74, 2, 'last_update', '1536683593'),
(75, 2, 'billing_first_name', 'Азамат'),
(76, 2, 'billing_address_1', 'город Шымкет ул. Кобланды батыра 12'),
(77, 2, 'billing_city', 'Шымкент'),
(78, 2, 'billing_country', 'KZ'),
(79, 2, 'billing_email', 'melan_19_86@mail.ru'),
(80, 2, 'billing_phone', '87014028065'),
(83, 2, 'shipping_method', ''),
(85, 2, '_woocommerce_persistent_cart_1', 'a:1:{s:4:\"cart\";a:2:{s:32:\"e2ef524fbf3d9fe611d5a8e90fefdc9c\";a:11:{s:3:\"key\";s:32:\"e2ef524fbf3d9fe611d5a8e90fefdc9c\";s:10:\"product_id\";i:97;s:12:\"variation_id\";i:0;s:9:\"variation\";a:0:{}s:8:\"quantity\";i:1;s:9:\"data_hash\";s:32:\"b5c1d5ca8bae6d4896cf1807cdf763f0\";s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:81000;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:81000;s:8:\"line_tax\";i:0;}s:32:\"26657d5ff9020d2abefe558796b99584\";a:11:{s:3:\"key\";s:32:\"26657d5ff9020d2abefe558796b99584\";s:10:\"product_id\";i:96;s:12:\"variation_id\";i:0;s:9:\"variation\";a:0:{}s:8:\"quantity\";i:1;s:9:\"data_hash\";s:32:\"b5c1d5ca8bae6d4896cf1807cdf763f0\";s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:74000;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:74000;s:8:\"line_tax\";i:0;}}}'),
(86, 4, 'nickname', 'mail'),
(87, 4, 'first_name', ''),
(88, 4, 'last_name', ''),
(89, 4, 'description', ''),
(90, 4, 'rich_editing', 'true'),
(91, 4, 'syntax_highlighting', 'true'),
(92, 4, 'comment_shortcuts', 'false'),
(93, 4, 'admin_color', 'fresh'),
(94, 4, 'use_ssl', '0'),
(95, 4, 'show_admin_bar_front', 'true'),
(96, 4, 'locale', ''),
(97, 4, 'wp_capabilities', 'a:1:{s:8:\"customer\";b:1;}'),
(98, 4, 'wp_user_level', '0'),
(99, 4, 'session_tokens', 'a:1:{s:64:\"9765dfdfa8abc9ce41599dddb1215c695be6eb7a883ea2bff54e8a11650c702c\";a:4:{s:10:\"expiration\";i:1543491208;s:2:\"ip\";s:13:\"212.96.91.126\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36\";s:5:\"login\";i:1542281608;}}'),
(100, 4, '_woocommerce_persistent_cart_1', 'a:1:{s:4:\"cart\";a:1:{s:32:\"03afdbd66e7929b125f8597834fa83a4\";a:11:{s:3:\"key\";s:32:\"03afdbd66e7929b125f8597834fa83a4\";s:10:\"product_id\";i:63;s:12:\"variation_id\";i:0;s:9:\"variation\";a:0:{}s:8:\"quantity\";i:2;s:9:\"data_hash\";s:32:\"b5c1d5ca8bae6d4896cf1807cdf763f0\";s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:80000;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:80000;s:8:\"line_tax\";i:0;}}}'),
(101, 4, 'wc_last_active', '1542585600'),
(102, 1, 'session_tokens', 'a:1:{s:64:\"fd9d5709cefe32fa217fc19a986dd4cdeb59c0b1c19ac93af97d7b3ae04a7a2f\";a:4:{s:10:\"expiration\";i:1554830209;s:2:\"ip\";s:14:\"193.193.239.26\";s:2:\"ua\";s:126:\"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36 OPR/58.0.3135.118\";s:5:\"login\";i:1553620609;}}'),
(104, 1, 'billing_first_name', 'ыфывф фывфыв'),
(105, 1, 'billing_last_name', ''),
(106, 1, 'billing_company', ''),
(107, 1, 'billing_address_1', 'ZXZXZX'),
(108, 1, 'billing_address_2', ''),
(109, 1, 'billing_city', 'Актобе'),
(110, 1, 'billing_postcode', ''),
(111, 1, 'billing_country', 'KZ'),
(112, 1, 'billing_state', ''),
(113, 1, 'billing_phone', '+77715206222'),
(114, 1, 'billing_email', 'bessonoff_a_v@mail.ru'),
(115, 1, 'shipping_first_name', ''),
(116, 1, 'shipping_last_name', ''),
(117, 1, 'shipping_company', ''),
(118, 1, 'shipping_address_1', ''),
(119, 1, 'shipping_address_2', ''),
(120, 1, 'shipping_city', ''),
(121, 1, 'shipping_postcode', ''),
(122, 1, 'shipping_country', ''),
(123, 1, 'shipping_state', ''),
(124, 1, 'last_update', '1552732642'),
(125, 5, 'nickname', 'info'),
(126, 5, 'first_name', ''),
(127, 5, 'last_name', ''),
(128, 5, 'description', ''),
(129, 5, 'rich_editing', 'true'),
(130, 5, 'syntax_highlighting', 'true'),
(131, 5, 'comment_shortcuts', 'false'),
(132, 5, 'admin_color', 'fresh'),
(133, 5, 'use_ssl', '0'),
(134, 5, 'show_admin_bar_front', 'true'),
(135, 5, 'locale', ''),
(136, 5, 'wp_capabilities', 'a:1:{s:8:\"customer\";b:1;}'),
(137, 5, 'wp_user_level', '0'),
(138, 5, 'session_tokens', 'a:1:{s:64:\"9f594749ba5c7dc9a4d8bdf31460510e8363efff8c1048aedc47fcb36b9526c1\";a:4:{s:10:\"expiration\";i:1551713877;s:2:\"ip\";s:11:\"37.99.76.23\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36\";s:5:\"login\";i:1550504277;}}'),
(139, 6, 'nickname', 'infogis'),
(140, 6, 'first_name', ''),
(141, 6, 'last_name', ''),
(142, 6, 'description', ''),
(143, 6, 'rich_editing', 'true'),
(144, 6, 'syntax_highlighting', 'true'),
(145, 6, 'comment_shortcuts', 'false'),
(146, 6, 'admin_color', 'fresh'),
(147, 6, 'use_ssl', '0'),
(148, 6, 'show_admin_bar_front', 'true'),
(149, 6, 'locale', ''),
(150, 6, 'wp_capabilities', 'a:1:{s:8:\"customer\";b:1;}'),
(151, 6, 'wp_user_level', '0'),
(152, 6, 'session_tokens', 'a:1:{s:64:\"3aa1e7c421739b233ab99fae66550a353c35351631c7d5096089905f5467e36d\";a:4:{s:10:\"expiration\";i:1551714014;s:2:\"ip\";s:11:\"37.99.76.23\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36\";s:5:\"login\";i:1550504414;}}'),
(153, 6, '_woocommerce_persistent_cart_1', 'a:1:{s:4:\"cart\";a:0:{}}'),
(154, 6, 'wc_last_active', '1551312000'),
(155, 7, 'nickname', 'gavrilov.srg'),
(156, 7, 'first_name', ''),
(157, 7, 'last_name', ''),
(158, 7, 'description', ''),
(159, 7, 'rich_editing', 'true'),
(160, 7, 'syntax_highlighting', 'true'),
(161, 7, 'comment_shortcuts', 'false'),
(162, 7, 'admin_color', 'fresh'),
(163, 7, 'use_ssl', '0'),
(164, 7, 'show_admin_bar_front', 'true'),
(165, 7, 'locale', ''),
(166, 7, 'wp_capabilities', 'a:1:{s:8:\"customer\";b:1;}'),
(167, 7, 'wp_user_level', '0'),
(169, 7, '_woocommerce_persistent_cart_1', 'a:1:{s:4:\"cart\";a:0:{}}'),
(170, 7, 'wc_last_active', '1552608000'),
(171, 8, 'nickname', 'dev'),
(172, 8, 'first_name', 'ыфывф фывфыв'),
(173, 8, 'last_name', ''),
(174, 8, 'description', ''),
(175, 8, 'rich_editing', 'true'),
(176, 8, 'syntax_highlighting', 'true'),
(177, 8, 'comment_shortcuts', 'false'),
(178, 8, 'admin_color', 'fresh'),
(179, 8, 'use_ssl', '0'),
(180, 8, 'show_admin_bar_front', 'true'),
(181, 8, 'locale', ''),
(182, 8, 'wp_capabilities', 'a:1:{s:8:\"customer\";b:1;}'),
(183, 8, 'wp_user_level', '0'),
(186, 8, 'wc_last_active', '1552694400'),
(187, 8, 'last_update', '1552679251'),
(188, 8, 'billing_first_name', 'ыфывф фывфыв'),
(189, 8, 'billing_address_1', 'Некрасова 99'),
(190, 8, 'billing_city', 'Актобе'),
(191, 8, 'billing_country', 'KZ'),
(192, 8, 'billing_email', 'dev@domain.com'),
(193, 8, 'billing_phone', '+77715206222'),
(196, 8, '_woocommerce_persistent_cart_1', 'a:1:{s:4:\"cart\";a:1:{s:32:\"e2ef524fbf3d9fe611d5a8e90fefdc9c\";a:11:{s:3:\"key\";s:32:\"e2ef524fbf3d9fe611d5a8e90fefdc9c\";s:10:\"product_id\";i:97;s:12:\"variation_id\";i:0;s:9:\"variation\";a:0:{}s:8:\"quantity\";i:2;s:9:\"data_hash\";s:32:\"b5c1d5ca8bae6d4896cf1807cdf763f0\";s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:162000;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:162000;s:8:\"line_tax\";i:0;}}}'),
(197, 8, 'shipping_method', ''),
(199, 1, 'closedpostboxes_product', 'a:1:{i:0;s:11:\"postexcerpt\";}'),
(200, 1, 'metaboxhidden_product', 'a:2:{i:0;s:10:\"postcustom\";i:1;s:7:\"slugdiv\";}'),
(201, 1, 'shipping_method', ''),
(203, 1, '_woocommerce_persistent_cart_1', 'a:1:{s:4:\"cart\";a:2:{s:32:\"e2ef524fbf3d9fe611d5a8e90fefdc9c\";a:11:{s:3:\"key\";s:32:\"e2ef524fbf3d9fe611d5a8e90fefdc9c\";s:10:\"product_id\";i:97;s:12:\"variation_id\";i:0;s:9:\"variation\";a:0:{}s:8:\"quantity\";i:4;s:9:\"data_hash\";s:32:\"b5c1d5ca8bae6d4896cf1807cdf763f0\";s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:324000;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:324000;s:8:\"line_tax\";i:0;}s:32:\"26657d5ff9020d2abefe558796b99584\";a:11:{s:3:\"key\";s:32:\"26657d5ff9020d2abefe558796b99584\";s:10:\"product_id\";i:96;s:12:\"variation_id\";i:0;s:9:\"variation\";a:0:{}s:8:\"quantity\";i:1;s:9:\"data_hash\";s:32:\"b5c1d5ca8bae6d4896cf1807cdf763f0\";s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:74000;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:74000;s:8:\"line_tax\";i:0;}}}');

-- --------------------------------------------------------

--
-- Структура таблицы `wp_users`
--

CREATE TABLE `wp_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) NOT NULL DEFAULT '',
  `user_pass` varchar(255) NOT NULL DEFAULT '',
  `user_nicename` varchar(50) NOT NULL DEFAULT '',
  `user_email` varchar(100) NOT NULL DEFAULT '',
  `user_url` varchar(100) NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `wp_users`
--

INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'admin', '$P$BgIYMAdWxkP4/md4vzuZKXcrYnWIWx0', 'admin', 'bessonoff_a_v@mail.ru', '', '2018-07-03 11:05:24', '', 0, 'admin'),
(2, 'melan_19_86', '$P$B0hZLaQUxFrHe4.qV9MUk5jAcHXbqj1', 'melan_19_86', 'melan_19_86@mail.ru', '', '2018-08-15 06:51:40', '', 0, 'melan_19_86'),
(3, 'kuzmina_231', '$P$B7OAwFM7Krqq5ixiTqg8XkTxWUjwVA.', 'kuzmina_231', 'kuzmina_231@mail.ru', '', '2018-08-16 07:01:39', '', 0, 'kuzmina_231'),
(4, 'mail', '$P$BsMlP86.x/89dsiH16FwfM4aGpIrMQ0', 'mail', 'mail@mail.kz', '', '2018-11-15 11:33:28', '', 0, 'mail'),
(5, 'info', '$P$BEE3zW4sz.vnABS1O.z2BqhF1G1Eby1', 'info', 'info@kaznetmedia.kz', '', '2019-02-18 15:37:56', '1550504351:$P$B0qOSjrO58uiD5i1hZwwwR1063tdub1', 0, 'info'),
(6, 'infogis', '$P$BubZhB3vyZePEl/BR8Lf43MIKRTTxv/', 'infogis', 'infogis@infogis.kz', '', '2019-02-18 15:40:13', '', 0, 'infogis'),
(7, 'gavrilov.srg', '$P$B/7Xot4UL8rZPRrsFnecgkOmIr/H6N/', 'gavrilov-srg', 'gavrilov.srg@gmail.com', '', '2019-03-15 19:24:51', '', 0, 'gavrilov.srg'),
(8, 'dev', '$P$B8OsA2Pl1ekZ7h8zSc9VOeC03e5RZ1.', 'dev', 'dev@domain.com', '', '2019-03-15 19:25:39', '', 0, 'dev');

-- --------------------------------------------------------

--
-- Структура таблицы `wp_wc_download_log`
--

CREATE TABLE `wp_wc_download_log` (
  `download_log_id` bigint(20) UNSIGNED NOT NULL,
  `timestamp` datetime NOT NULL,
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `user_ip_address` varchar(100) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `wp_wc_webhooks`
--

CREATE TABLE `wp_wc_webhooks` (
  `webhook_id` bigint(20) UNSIGNED NOT NULL,
  `status` varchar(200) NOT NULL,
  `name` text NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `delivery_url` text NOT NULL,
  `secret` text NOT NULL,
  `topic` varchar(200) NOT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_created_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `api_version` smallint(4) NOT NULL,
  `failure_count` smallint(10) NOT NULL DEFAULT '0',
  `pending_delivery` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `wp_woocommerce_api_keys`
--

CREATE TABLE `wp_woocommerce_api_keys` (
  `key_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `description` varchar(200) DEFAULT NULL,
  `permissions` varchar(10) NOT NULL,
  `consumer_key` char(64) NOT NULL,
  `consumer_secret` char(43) NOT NULL,
  `nonces` longtext,
  `truncated_key` char(7) NOT NULL,
  `last_access` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `wp_woocommerce_attribute_taxonomies`
--

CREATE TABLE `wp_woocommerce_attribute_taxonomies` (
  `attribute_id` bigint(20) UNSIGNED NOT NULL,
  `attribute_name` varchar(200) NOT NULL,
  `attribute_label` varchar(200) DEFAULT NULL,
  `attribute_type` varchar(20) NOT NULL,
  `attribute_orderby` varchar(20) NOT NULL,
  `attribute_public` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `wp_woocommerce_attribute_taxonomies`
--

INSERT INTO `wp_woocommerce_attribute_taxonomies` (`attribute_id`, `attribute_name`, `attribute_label`, `attribute_type`, `attribute_orderby`, `attribute_public`) VALUES
(1, 'material', 'Метериал', 'select', 'menu_order', 0),
(2, 'color', 'Цвет', 'select', 'menu_order', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `wp_woocommerce_downloadable_product_permissions`
--

CREATE TABLE `wp_woocommerce_downloadable_product_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `download_id` varchar(36) NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `order_key` varchar(200) NOT NULL,
  `user_email` varchar(200) NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `downloads_remaining` varchar(9) DEFAULT NULL,
  `access_granted` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access_expires` datetime DEFAULT NULL,
  `download_count` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `wp_woocommerce_log`
--

CREATE TABLE `wp_woocommerce_log` (
  `log_id` bigint(20) UNSIGNED NOT NULL,
  `timestamp` datetime NOT NULL,
  `level` smallint(4) NOT NULL,
  `source` varchar(200) NOT NULL,
  `message` longtext NOT NULL,
  `context` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `wp_woocommerce_order_itemmeta`
--

CREATE TABLE `wp_woocommerce_order_itemmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `order_item_id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `wp_woocommerce_order_itemmeta`
--

INSERT INTO `wp_woocommerce_order_itemmeta` (`meta_id`, `order_item_id`, `meta_key`, `meta_value`) VALUES
(1, 1, '_product_id', '97'),
(2, 1, '_variation_id', '0'),
(3, 1, '_qty', '1'),
(4, 1, '_tax_class', ''),
(5, 1, '_line_subtotal', '13771'),
(6, 1, '_line_subtotal_tax', '0'),
(7, 1, '_line_total', '13771'),
(8, 1, '_line_tax', '0'),
(9, 1, '_line_tax_data', 'a:2:{s:5:\"total\";a:0:{}s:8:\"subtotal\";a:0:{}}'),
(10, 2, '_product_id', '63'),
(11, 2, '_variation_id', '0'),
(12, 2, '_qty', '1'),
(13, 2, '_tax_class', ''),
(14, 2, '_line_subtotal', '6801'),
(15, 2, '_line_subtotal_tax', '0'),
(16, 2, '_line_total', '6801'),
(17, 2, '_line_tax', '0'),
(18, 2, '_line_tax_data', 'a:2:{s:5:\"total\";a:0:{}s:8:\"subtotal\";a:0:{}}'),
(19, 3, '_product_id', '63'),
(20, 3, '_variation_id', '0'),
(21, 3, '_qty', '1'),
(22, 3, '_tax_class', ''),
(23, 3, '_line_subtotal', '6801'),
(24, 3, '_line_subtotal_tax', '0'),
(25, 3, '_line_total', '6801'),
(26, 3, '_line_tax', '0'),
(27, 3, '_line_tax_data', 'a:2:{s:5:\"total\";a:0:{}s:8:\"subtotal\";a:0:{}}'),
(28, 4, '_product_id', '63'),
(29, 4, '_variation_id', '0'),
(30, 4, '_qty', '1'),
(31, 4, '_tax_class', ''),
(32, 4, '_line_subtotal', '40000'),
(33, 4, '_line_subtotal_tax', '0'),
(34, 4, '_line_total', '40000'),
(35, 4, '_line_tax', '0'),
(36, 4, '_line_tax_data', 'a:2:{s:5:\"total\";a:0:{}s:8:\"subtotal\";a:0:{}}'),
(46, 6, '_product_id', '63'),
(47, 6, '_variation_id', '0'),
(48, 6, '_qty', '2'),
(49, 6, '_tax_class', ''),
(50, 6, '_line_subtotal', '80000'),
(51, 6, '_line_subtotal_tax', '0'),
(52, 6, '_line_total', '80000'),
(53, 6, '_line_tax', '0'),
(54, 6, '_line_tax_data', 'a:2:{s:5:\"total\";a:0:{}s:8:\"subtotal\";a:0:{}}'),
(55, 7, '_product_id', '97'),
(56, 7, '_variation_id', '0'),
(57, 7, '_qty', '1'),
(58, 7, '_tax_class', ''),
(59, 7, '_line_subtotal', '81000'),
(60, 7, '_line_subtotal_tax', '0'),
(61, 7, '_line_total', '81000'),
(62, 7, '_line_tax', '0'),
(63, 7, '_line_tax_data', 'a:2:{s:5:\"total\";a:0:{}s:8:\"subtotal\";a:0:{}}'),
(64, 8, '_product_id', '63'),
(65, 8, '_variation_id', '0'),
(66, 8, '_qty', '1'),
(67, 8, '_tax_class', ''),
(68, 8, '_line_subtotal', '40000'),
(69, 8, '_line_subtotal_tax', '0'),
(70, 8, '_line_total', '40000'),
(71, 8, '_line_tax', '0'),
(72, 8, '_line_tax_data', 'a:2:{s:5:\"total\";a:0:{}s:8:\"subtotal\";a:0:{}}'),
(73, 9, '_product_id', '96'),
(74, 9, '_variation_id', '0'),
(75, 9, '_qty', '1'),
(76, 9, '_tax_class', ''),
(77, 9, '_line_subtotal', '74000'),
(78, 9, '_line_subtotal_tax', '0'),
(79, 9, '_line_total', '74000'),
(80, 9, '_line_tax', '0'),
(81, 9, '_line_tax_data', 'a:2:{s:5:\"total\";a:0:{}s:8:\"subtotal\";a:0:{}}'),
(82, 10, '_product_id', '97'),
(83, 10, '_variation_id', '0'),
(84, 10, '_qty', '1'),
(85, 10, '_tax_class', ''),
(86, 10, '_line_subtotal', '81000'),
(87, 10, '_line_subtotal_tax', '0'),
(88, 10, '_line_total', '81000'),
(89, 10, '_line_tax', '0'),
(90, 10, '_line_tax_data', 'a:2:{s:5:\"total\";a:0:{}s:8:\"subtotal\";a:0:{}}'),
(91, 11, '_product_id', '97'),
(92, 11, '_variation_id', '0'),
(93, 11, '_qty', '2'),
(94, 11, '_tax_class', ''),
(95, 11, '_line_subtotal', '162000'),
(96, 11, '_line_subtotal_tax', '0'),
(97, 11, '_line_total', '162000'),
(98, 11, '_line_tax', '0'),
(99, 11, '_line_tax_data', 'a:2:{s:5:\"total\";a:0:{}s:8:\"subtotal\";a:0:{}}'),
(100, 11, '_tinvwl_wishlist_cart', 'a:1:{s:6:\"029B9F\";i:1;}'),
(101, 12, '_product_id', '63'),
(102, 12, '_variation_id', '0'),
(103, 12, '_qty', '2'),
(104, 12, '_tax_class', ''),
(105, 12, '_line_subtotal', '80000'),
(106, 12, '_line_subtotal_tax', '0'),
(107, 12, '_line_total', '80000'),
(108, 12, '_line_tax', '0'),
(109, 12, '_line_tax_data', 'a:2:{s:5:\"total\";a:0:{}s:8:\"subtotal\";a:0:{}}'),
(110, 13, '_product_id', '160'),
(111, 13, '_variation_id', '0'),
(112, 13, '_qty', '1'),
(113, 13, '_tax_class', ''),
(114, 13, '_line_subtotal', '2000'),
(115, 13, '_line_subtotal_tax', '0'),
(116, 13, '_line_total', '2000'),
(117, 13, '_line_tax', '0'),
(118, 13, '_line_tax_data', 'a:2:{s:5:\"total\";a:0:{}s:8:\"subtotal\";a:0:{}}');

-- --------------------------------------------------------

--
-- Структура таблицы `wp_woocommerce_order_items`
--

CREATE TABLE `wp_woocommerce_order_items` (
  `order_item_id` bigint(20) UNSIGNED NOT NULL,
  `order_item_name` text NOT NULL,
  `order_item_type` varchar(200) NOT NULL DEFAULT '',
  `order_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `wp_woocommerce_order_items`
--

INSERT INTO `wp_woocommerce_order_items` (`order_item_id`, `order_item_name`, `order_item_type`, `order_id`) VALUES
(1, 'Кошелек из кожи крокодила, ExoticLUX', 'line_item', 149),
(2, 'Кошелек мужской', 'line_item', 149),
(3, 'Кошелек мужской', 'line_item', 150),
(4, 'Кошелек мужской', 'line_item', 151),
(6, 'Кошелек мужской', 'line_item', 153),
(7, 'Кошелек из кожи крокодила, ExoticLUX', 'line_item', 155),
(8, 'Кошелек мужской', 'line_item', 155),
(9, 'Портмоне, Dr. Koffer', 'line_item', 155),
(10, 'Кошелек из кожи крокодила, ExoticLUX', 'line_item', 156),
(11, 'Кошелек из кожи крокодила, ExoticLUX', 'line_item', 157),
(12, 'Кошелек мужской', 'line_item', 159),
(13, 'Спортивный костюм', 'line_item', 161);

-- --------------------------------------------------------

--
-- Структура таблицы `wp_woocommerce_payment_tokenmeta`
--

CREATE TABLE `wp_woocommerce_payment_tokenmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `payment_token_id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `wp_woocommerce_payment_tokens`
--

CREATE TABLE `wp_woocommerce_payment_tokens` (
  `token_id` bigint(20) UNSIGNED NOT NULL,
  `gateway_id` varchar(200) NOT NULL,
  `token` text NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `type` varchar(200) NOT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `wp_woocommerce_sessions`
--

CREATE TABLE `wp_woocommerce_sessions` (
  `session_id` bigint(20) UNSIGNED NOT NULL,
  `session_key` char(32) NOT NULL,
  `session_value` longtext NOT NULL,
  `session_expiry` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `wp_woocommerce_shipping_zones`
--

CREATE TABLE `wp_woocommerce_shipping_zones` (
  `zone_id` bigint(20) UNSIGNED NOT NULL,
  `zone_name` varchar(200) NOT NULL,
  `zone_order` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `wp_woocommerce_shipping_zone_locations`
--

CREATE TABLE `wp_woocommerce_shipping_zone_locations` (
  `location_id` bigint(20) UNSIGNED NOT NULL,
  `zone_id` bigint(20) UNSIGNED NOT NULL,
  `location_code` varchar(200) NOT NULL,
  `location_type` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `wp_woocommerce_shipping_zone_methods`
--

CREATE TABLE `wp_woocommerce_shipping_zone_methods` (
  `zone_id` bigint(20) UNSIGNED NOT NULL,
  `instance_id` bigint(20) UNSIGNED NOT NULL,
  `method_id` varchar(200) NOT NULL,
  `method_order` bigint(20) UNSIGNED NOT NULL,
  `is_enabled` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `wp_woocommerce_tax_rates`
--

CREATE TABLE `wp_woocommerce_tax_rates` (
  `tax_rate_id` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_country` varchar(2) NOT NULL DEFAULT '',
  `tax_rate_state` varchar(200) NOT NULL DEFAULT '',
  `tax_rate` varchar(8) NOT NULL DEFAULT '',
  `tax_rate_name` varchar(200) NOT NULL DEFAULT '',
  `tax_rate_priority` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_compound` int(1) NOT NULL DEFAULT '0',
  `tax_rate_shipping` int(1) NOT NULL DEFAULT '1',
  `tax_rate_order` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_class` varchar(200) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `wp_woocommerce_tax_rate_locations`
--

CREATE TABLE `wp_woocommerce_tax_rate_locations` (
  `location_id` bigint(20) UNSIGNED NOT NULL,
  `location_code` varchar(200) NOT NULL,
  `tax_rate_id` bigint(20) UNSIGNED NOT NULL,
  `location_type` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Индексы таблицы `wp_comments`
--
ALTER TABLE `wp_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10)),
  ADD KEY `woo_idx_comment_type` (`comment_type`);

--
-- Индексы таблицы `wp_failed_jobs`
--
ALTER TABLE `wp_failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `wp_links`
--
ALTER TABLE `wp_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Индексы таблицы `wp_options`
--
ALTER TABLE `wp_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`);

--
-- Индексы таблицы `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Индексы таблицы `wp_posts`
--
ALTER TABLE `wp_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Индексы таблицы `wp_queue`
--
ALTER TABLE `wp_queue`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Индексы таблицы `wp_terms`
--
ALTER TABLE `wp_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Индексы таблицы `wp_term_relationships`
--
ALTER TABLE `wp_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Индексы таблицы `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Индексы таблицы `wp_tinvwl_items`
--
ALTER TABLE `wp_tinvwl_items`
  ADD PRIMARY KEY (`ID`);

--
-- Индексы таблицы `wp_tinvwl_lists`
--
ALTER TABLE `wp_tinvwl_lists`
  ADD PRIMARY KEY (`ID`);

--
-- Индексы таблицы `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Индексы таблицы `wp_users`
--
ALTER TABLE `wp_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- Индексы таблицы `wp_wc_download_log`
--
ALTER TABLE `wp_wc_download_log`
  ADD PRIMARY KEY (`download_log_id`),
  ADD KEY `permission_id` (`permission_id`),
  ADD KEY `timestamp` (`timestamp`);

--
-- Индексы таблицы `wp_wc_webhooks`
--
ALTER TABLE `wp_wc_webhooks`
  ADD PRIMARY KEY (`webhook_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Индексы таблицы `wp_woocommerce_api_keys`
--
ALTER TABLE `wp_woocommerce_api_keys`
  ADD PRIMARY KEY (`key_id`),
  ADD KEY `consumer_key` (`consumer_key`),
  ADD KEY `consumer_secret` (`consumer_secret`);

--
-- Индексы таблицы `wp_woocommerce_attribute_taxonomies`
--
ALTER TABLE `wp_woocommerce_attribute_taxonomies`
  ADD PRIMARY KEY (`attribute_id`),
  ADD KEY `attribute_name` (`attribute_name`(20));

--
-- Индексы таблицы `wp_woocommerce_downloadable_product_permissions`
--
ALTER TABLE `wp_woocommerce_downloadable_product_permissions`
  ADD PRIMARY KEY (`permission_id`),
  ADD KEY `download_order_key_product` (`product_id`,`order_id`,`order_key`(16),`download_id`),
  ADD KEY `download_order_product` (`download_id`,`order_id`,`product_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Индексы таблицы `wp_woocommerce_log`
--
ALTER TABLE `wp_woocommerce_log`
  ADD PRIMARY KEY (`log_id`),
  ADD KEY `level` (`level`);

--
-- Индексы таблицы `wp_woocommerce_order_itemmeta`
--
ALTER TABLE `wp_woocommerce_order_itemmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `order_item_id` (`order_item_id`),
  ADD KEY `meta_key` (`meta_key`(32));

--
-- Индексы таблицы `wp_woocommerce_order_items`
--
ALTER TABLE `wp_woocommerce_order_items`
  ADD PRIMARY KEY (`order_item_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Индексы таблицы `wp_woocommerce_payment_tokenmeta`
--
ALTER TABLE `wp_woocommerce_payment_tokenmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `payment_token_id` (`payment_token_id`),
  ADD KEY `meta_key` (`meta_key`(32));

--
-- Индексы таблицы `wp_woocommerce_payment_tokens`
--
ALTER TABLE `wp_woocommerce_payment_tokens`
  ADD PRIMARY KEY (`token_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Индексы таблицы `wp_woocommerce_sessions`
--
ALTER TABLE `wp_woocommerce_sessions`
  ADD PRIMARY KEY (`session_key`),
  ADD UNIQUE KEY `session_id` (`session_id`);

--
-- Индексы таблицы `wp_woocommerce_shipping_zones`
--
ALTER TABLE `wp_woocommerce_shipping_zones`
  ADD PRIMARY KEY (`zone_id`);

--
-- Индексы таблицы `wp_woocommerce_shipping_zone_locations`
--
ALTER TABLE `wp_woocommerce_shipping_zone_locations`
  ADD PRIMARY KEY (`location_id`),
  ADD KEY `location_id` (`location_id`),
  ADD KEY `location_type_code` (`location_type`(10),`location_code`(20));

--
-- Индексы таблицы `wp_woocommerce_shipping_zone_methods`
--
ALTER TABLE `wp_woocommerce_shipping_zone_methods`
  ADD PRIMARY KEY (`instance_id`);

--
-- Индексы таблицы `wp_woocommerce_tax_rates`
--
ALTER TABLE `wp_woocommerce_tax_rates`
  ADD PRIMARY KEY (`tax_rate_id`),
  ADD KEY `tax_rate_country` (`tax_rate_country`),
  ADD KEY `tax_rate_state` (`tax_rate_state`(2)),
  ADD KEY `tax_rate_class` (`tax_rate_class`(10)),
  ADD KEY `tax_rate_priority` (`tax_rate_priority`);

--
-- Индексы таблицы `wp_woocommerce_tax_rate_locations`
--
ALTER TABLE `wp_woocommerce_tax_rate_locations`
  ADD PRIMARY KEY (`location_id`),
  ADD KEY `tax_rate_id` (`tax_rate_id`),
  ADD KEY `location_type_code` (`location_type`(10),`location_code`(20));

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `wp_comments`
--
ALTER TABLE `wp_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT для таблицы `wp_failed_jobs`
--
ALTER TABLE `wp_failed_jobs`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `wp_links`
--
ALTER TABLE `wp_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `wp_options`
--
ALTER TABLE `wp_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14672;

--
-- AUTO_INCREMENT для таблицы `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1260;

--
-- AUTO_INCREMENT для таблицы `wp_posts`
--
ALTER TABLE `wp_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=162;

--
-- AUTO_INCREMENT для таблицы `wp_queue`
--
ALTER TABLE `wp_queue`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;

--
-- AUTO_INCREMENT для таблицы `wp_terms`
--
ALTER TABLE `wp_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT для таблицы `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT для таблицы `wp_tinvwl_items`
--
ALTER TABLE `wp_tinvwl_items`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT для таблицы `wp_tinvwl_lists`
--
ALTER TABLE `wp_tinvwl_lists`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT для таблицы `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=204;

--
-- AUTO_INCREMENT для таблицы `wp_users`
--
ALTER TABLE `wp_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `wp_wc_download_log`
--
ALTER TABLE `wp_wc_download_log`
  MODIFY `download_log_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `wp_wc_webhooks`
--
ALTER TABLE `wp_wc_webhooks`
  MODIFY `webhook_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `wp_woocommerce_api_keys`
--
ALTER TABLE `wp_woocommerce_api_keys`
  MODIFY `key_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `wp_woocommerce_attribute_taxonomies`
--
ALTER TABLE `wp_woocommerce_attribute_taxonomies`
  MODIFY `attribute_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `wp_woocommerce_downloadable_product_permissions`
--
ALTER TABLE `wp_woocommerce_downloadable_product_permissions`
  MODIFY `permission_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `wp_woocommerce_log`
--
ALTER TABLE `wp_woocommerce_log`
  MODIFY `log_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `wp_woocommerce_order_itemmeta`
--
ALTER TABLE `wp_woocommerce_order_itemmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=119;

--
-- AUTO_INCREMENT для таблицы `wp_woocommerce_order_items`
--
ALTER TABLE `wp_woocommerce_order_items`
  MODIFY `order_item_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT для таблицы `wp_woocommerce_payment_tokenmeta`
--
ALTER TABLE `wp_woocommerce_payment_tokenmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `wp_woocommerce_payment_tokens`
--
ALTER TABLE `wp_woocommerce_payment_tokens`
  MODIFY `token_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `wp_woocommerce_sessions`
--
ALTER TABLE `wp_woocommerce_sessions`
  MODIFY `session_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `wp_woocommerce_shipping_zones`
--
ALTER TABLE `wp_woocommerce_shipping_zones`
  MODIFY `zone_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `wp_woocommerce_shipping_zone_locations`
--
ALTER TABLE `wp_woocommerce_shipping_zone_locations`
  MODIFY `location_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `wp_woocommerce_shipping_zone_methods`
--
ALTER TABLE `wp_woocommerce_shipping_zone_methods`
  MODIFY `instance_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `wp_woocommerce_tax_rates`
--
ALTER TABLE `wp_woocommerce_tax_rates`
  MODIFY `tax_rate_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `wp_woocommerce_tax_rate_locations`
--
ALTER TABLE `wp_woocommerce_tax_rate_locations`
  MODIFY `location_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `wp_wc_download_log`
--
ALTER TABLE `wp_wc_download_log`
  ADD CONSTRAINT `fk_wc_download_log_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `wp_woocommerce_downloadable_product_permissions` (`permission_id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
