<?php /*
Template Name: Category
 * The template for displaying categories
 *
 * This is the template that displays all categories by default.
 *
 * @package WordPress
 * @subpackage os_theme
 * @since 1.0
 */
get_header(); ?>
<div class="block-category row ">
		<div class="container py-3 my-3">
			<h3 class="h1 text-uppercase font-weight-bold"><?php single_cat_title('');?></h3>
			<?php if (function_exists('dimox_breadcrumbs')) {dimox_breadcrumbs();}?>
			<div class="row">
			<?php 		
			global $post;
			$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
			$cat_id = get_query_var('cat');
			$args = array( 'posts_per_page' => 12,'category'=> $cat_id, 'paged'=>$paged );
			$myposts = get_posts( $args );
			foreach( $myposts as $post )
			{ setup_postdata($post);?>					
			<div id="post-<?php the_ID(); ?>" <?php post_class('col-md-6 py-3'); ?>>
				<a href="<?php echo get_permalink(); ?>" class="post-hover  text-dark">
					<?php						
						the_title('<h4 class="title">','</h4>');
			 			echo '<span class="text-secondary d-block w-100">'.get_the_date('d F Y').'</span>';
						the_excerpt();					
					?>
				</a>						
			</div>
			<?php } 
			wp_reset_postdata();
			the_posts_navigation( array(
			'prev_text'          => '',
			'next_text'          => '',
			'screen_reader_text' => '',) );			
		?>
		</div>	
	</div>	
</div>
<?php get_footer(); ?>
