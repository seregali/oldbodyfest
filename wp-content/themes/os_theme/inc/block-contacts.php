<?php
/**
 * The template for displaying the contacts with two or three columns 
 *
 * @package WordPress
 * @subpackage os_theme
 * @since 1.0
 */
?>					
	<div class="block-contacts row py-4 text-white bg-sran">
		<div class="container">
			<div class="row">
 				<?php if ( is_active_sidebar('footer_block_1') and is_active_sidebar('footer_block_2') and is_active_sidebar('footer_block_3') and is_active_sidebar('footer_block_4')) {?>
					<div class="col-lg-3 col-md-6">
						<?php dynamic_sidebar('footer_block_1');?>
					</div>
					<div class="col-lg-3 col-md-6">
						<?php dynamic_sidebar('footer_block_2');?>
					</div>
					<div class="col-lg-3 col-md-6">
						<?php dynamic_sidebar('footer_block_3');?>
					</div>
					<div class="col-lg-3 col-md-6">
						<?php dynamic_sidebar('footer_block_4');?>
					</div>
				<?php } else { ?>	
					<?php if ( is_active_sidebar('footer_block_1') ):?>
						<div class="col-md-4">
							<?php dynamic_sidebar('footer_block_1');?>
						</div>
					<? endif; ?>
					<?php if ( is_active_sidebar('footer_block_2') ):?>
						<div class="col-md-8">
							<?php dynamic_sidebar('footer_block_2');?>
						</div>
					<? endif; ?>
					<?php if ( is_active_sidebar('footer_block_3') ):?>
						<div class="col-md-4">
							<?php dynamic_sidebar('footer_block_3');?>
						</div>
					<? endif; ?>
					<?php if ( is_active_sidebar('footer_block_4') ):?>
						<div class="col-md-4">
							<?php dynamic_sidebar('footer_block_4');?>
						</div>
					<? endif; ?>
				<?php } ?>
			</div>
		</div>
	</div>