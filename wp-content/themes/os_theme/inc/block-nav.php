<?php
/**
 * The template for displaying the navbar 
 *
 * @package WordPress
 * @subpackage os_theme
 * @since 1.0
 */
?>	
	<div class="block-top row bg-sran py-2">
		<div class="container text-white">
			<?php dynamic_sidebar('block_social_top');?>
			<?php dynamic_sidebar('block_top_1');?>
		</div>
	</div>
	<div class="block-nav row bg-black-trans">
		<div class="container">
			<div class="row">		
			<nav class="navbar navbar-dark navbar-expand-md text-uppercase col">	
				<?php 
					$custom_logo_id = get_theme_mod( 'custom_logo' );
					$logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
					if ( has_custom_logo() ):echo '<a class="navbar-brand" href="'.home_url().'"><img src="'. esc_url( $logo[0] ) .'"></a>';endif;
				?> 
				<button class="navbar-toggler navbar-light" type="button" data-toggle="collapse" data-target="#OsNavbar">
    				<span class="navbar-toggler-icon"></span>
  				</button>
				<?php 
					$args = array( 
						'container' => 'div',
						'container_class' => 'collapse navbar-collapse',
						'container_id'=>'OsNavbar',
						'menu_class' => 'navbar-nav', 
						'theme_location' => 'header_menu',
						'walker'=> new True_Walker_Nav_Menu());
					wp_nav_menu( $args );?>
			</nav>
			</div>
		</div>	
	</div>