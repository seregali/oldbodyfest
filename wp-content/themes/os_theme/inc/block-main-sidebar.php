<?php
/**
 * The template for displaying the links 
 *
 * @package WordPress
 * @subpackage os_theme
 * @since 1.0
 */
?>					
	<div class="col-lg-6 block-main-sidebar">
		<div class="row">
			<div class="col-lg-1">
				
			</div>
			<div class="col-lg-11 py-3 bg-black-trans">
				<nav class="mb-3">
  					<div class="nav nav-tabs" id="nav-tab" role="tablist">
    					<a class="nav-item nav-link active" id="nav-clothing-tab" data-toggle="tab" href="#nav-clothing" role="tab" aria-controls="nav-clothing" aria-selected="true">Одежда</a>
    					<a class="nav-item nav-link" id="nav-accessories-tab" data-toggle="tab" href="#nav-accessories" role="tab" aria-controls="nav-accessories" aria-selected="false">Аксессуары</a>
						<a class="nav-item nav-link" id="nav-sredstva-tab" data-toggle="tab" href="#nav-sredstva" role="tab" aria-controls="nav-sredstva" aria-selected="false">Средства ухода</a>
  					</div>
				</nav>
				<div class="tab-content" id="nav-tabContent">
  					<div class="tab-pane fade show active" id="nav-clothing" role="tabpanel" aria-labelledby="nav-clothing-tab"><?php wp_nav_menu( array('menu' => 'Odejda' )); ?></div>
  					<div class="tab-pane fade" id="nav-accessories" role="tabpanel" aria-labelledby="nav-accessories-tab"><?php wp_nav_menu( array('menu' => 'Aksessuari' )); ?></div>
					<div class="tab-pane fade" id="nav-sredstva" role="tabpanel" aria-labelledby="nav-sredstva-tab"><?php wp_nav_menu( array('menu' => 'Sredstva' )); ?></div>
				</div>			
			</div>			
		</div>				
	</div>