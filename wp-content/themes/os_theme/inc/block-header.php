<?php
/**
 * The template for displaying the header 
 *
 * @package WordPress
 * @subpackage os_theme
 * @since 1.0
 */
?>					
	<div class="jumbotron jumbotron-fluid row" style="<?php echo sprintf( 'color: #%s;', get_header_textcolor() ); ?>;<?php if( has_header_image()):?>background-image:url('<?php header_image();?>');<?php endif;?>">
		<div class="container py-5">
			<h3 class=""><?php echo bloginfo('name');?></h3>
			<?php if (bloginfo('description')): ?><p class="description"><?php echo bloginfo('description');?></p><?php endif;?>
		</div>				
	</div>