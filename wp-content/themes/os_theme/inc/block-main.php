<?php
/**
 * The template for displaying the block with categories menu 
 *
 * @package WordPress
 * @subpackage os_theme
 * @since 1.0
 */
?>					
	<div class="block-main row py-4">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 py-3 bg-black-trans">
					<?php 
						if (have_posts()): while (have_posts()): the_post(); 
						the_content(); 	
						endwhile; endif; 
					?>
					
				<div class="preim py-3">				
					<?php dynamic_sidebar('bonus');?>
				</div>
				</div>
				<?php get_template_part( 'inc/block','main-sidebar' ); ?>				
			</div>
		</div>	
	</div>