<?php
/**
 * The template for displaying the copyright with one columns 
 *
 * @package WordPress
 * @subpackage os_theme
 * @since 1.0
 */
?>					
	<div class="block-copyright row py-3 bg-sran">
		<div class="container">
			<span class="text-white">Дизайн и разработка Медиа-агентства <a href="http://ai.kz" class="text-white" target="_blank">"АктобеИНФО"</a></span>
		</div>
	</div>
		