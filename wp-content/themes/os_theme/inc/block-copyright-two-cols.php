<?php
/**
 * The template for displaying the copyright with two columns 
 *
 * @package WordPress
 * @subpackage os_theme
 * @since 1.0
 */
?>					
	<div class="block-copyright row py-4">
		<div class="col-lg-8 mx-auto">
			<div class="row">
				<div class="col-sm-6">
					<span class="float-sm-left"><?php echo bloginfo('name');?></span>
				</div>	
				<div class="col-sm-6">
					<span class="float-sm-right">Дизайн и разработка Медиа-агентства <a href="http://ai.kz" class="text-black" target="_blank">"АктобеИНФО"</a></span>
				</div>
			</div>
		</div>
	</div>
		