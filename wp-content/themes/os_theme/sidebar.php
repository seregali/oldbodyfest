<?php 
/**
 * The template for displaying sidebar
 *
 * @package WordPress
 * @subpackage os_theme
 * @since 1.0
 */
;?>
<div class="btn-menu hidden-lg hidden-md hiden-sm">
	<button type="button" class="navbar-toggle">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> 
      </button>
</div>
<aside class="col-md-3 col-sm-4 hidden-menu">
	<?php dynamic_sidebar( 'sidebar' ); ?>
</aside>

