<?php 
/*
 * Template Name: Front page
 * The template for displaying pages
 *
 * This is the template that display main page by default.
 * @package WordPress
 * @subpackage os_theme
 * @since 1.0
 */
 get_header(); ?>
 
<?php get_template_part( 'inc/block','main' ); ?>

<?php get_footer(); ?>