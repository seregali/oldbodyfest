<?php 
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage os_theme
 * @since 1.0
 */
 get_header();?> 
<div class="block-content row pt-3">
	<div class="container">
		<div class="row">			
			<div class="col-lg-3 block-main-sidebar">
				<div class="row">
					<div class="col-lg-11 py-3">
						<span class="h1 text-uppercase font-weight-bold d-block w-100">Каталог</span>
						<?php dynamic_sidebar('block_catalog');?>
					</div>			
				</div>				
			</div>			
			<div class="col-lg-9 py-3">
				<?php 
				if (is_single()) { 
					the_title('<h3 class="h1 text-uppercase font-weight-bold">','</h3>'); } 
				else { ?>
				<h3 class="h1 text-uppercase font-weight-bold"><?php woocommerce_page_title(); ?></h3>
				<?php }?>
				<?php woocommerce_breadcrumb( $args = array() );?> 
				<div id="page-<?php the_ID(); ?>"  <?php post_class('py-3'); ?>>
					<?php woocommerce_content(); ?>
				</div>
			</div>
		</div>		
	</div>
</div>		
	
<?php get_footer(); ?>
