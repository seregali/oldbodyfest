<?php 
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage os_theme
 * @since 1.0
 */
 get_header();?> 
<div class="block-content row">
	<div class="container py-3 my-3">
		<?php 
		the_title('<h3 class="h1 text-uppercase font-weight-bold">','</h3>'); ?>
		<?php if (function_exists('dimox_breadcrumbs')) {dimox_breadcrumbs();}?>
		<div id="page-<?php the_ID(); ?>"  <?php post_class('py-3'); ?>>
		<?php
		if ( has_post_thumbnail()) {
			$srcmain = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large');
   			$src = wp_get_attachment_image_src( get_post_thumbnail_id(), 'medium');
			echo '<a href="'.$srcmain[0].'" data-toggle="lightbox" class="d-block mb-3 mr-3 float-md-left"><img class="img-fluid" src="'.$src[0].'"></a>';
   			//echo kama_thumb_img('w=450 &h=450 &yes_stub &class=img-responsive thumb', $src[0] );	
			if (have_posts()): while (have_posts()): the_post(); 
			the_content(); 	
			endwhile; endif; 
		}
		else {
			if (have_posts()): while (have_posts()): the_post(); 
			the_content(); 	
			endwhile; endif; 
		}
		?>
		</div>
	</div>
</div>		
	
<?php get_footer(); ?>
