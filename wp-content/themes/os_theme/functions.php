<?php
/**
 * 60school functions and definitions
 *
 * @package WordPress
 * @subpackage os_theme
 * @since 1.0
 **/
add_theme_support( 'menus' );
add_theme_support( 'post-thumbnails' );
add_theme_support( 'post-formats', array( 'aside', 'gallery' ) );
add_theme_support( 'title-tag' );
add_theme_support( 'automatic-feed-links' );
add_theme_support( 'custom-logo', array(
		'height'      => 514,
		'width'       => 471,
		'flex-height' => true,
		'flex-width' => true,
	) );
add_theme_support( 'custom-header', array(
	'default-image'          => '',
	'random-default'         => false,
	'width'                  => 1920,
	'height'                 => 1080,
	'flex-height'            => true,
	'flex-width'             => true,
	'default-text-color'     => '', 
	'header-text'            => true,
	'uploads'                => true,
	'wp-head-callback'       => '',
	'admin-head-callback'    => '',
	'admin-preview-callback' => '',
	'video'                  => true,
	'video-active-callback'  => 'is_front_page', 
	) );
add_theme_support( 'custom-background', array(
	'default-color'          => '',
	'default-image'          => '',
	'wp-head-callback'       => '_custom_background_cb',
	'admin-head-callback'    => '',
	'admin-preview-callback' => ''
	) );
add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );

function enqueue_styles() {
	wp_enqueue_style( 'AA', get_stylesheet_uri(),array(),null);
	wp_enqueue_style( 'bootstrap',get_template_directory_uri() . '/css/bootstrap.min.css',array(),null);
	wp_enqueue_style( 'ekko',get_template_directory_uri() . '/css/ekko-lightbox.css',array(),null);
	wp_enqueue_style( 'fontawesome',get_template_directory_uri() . '/css/font-awesome.css',array(),null);
}
add_action('wp_enqueue_scripts', 'enqueue_styles');

function enqueue_scripts () {
}
add_action('wp_enqueue_scripts', 'enqueue_scripts',true);

function head_hook_links(){
	echo '<link rel="pingback" href="';bloginfo('pingback_url');echo '" />';
	/*echo '<link rel="icon" type="image/png" href="/favicon.png" />';*/
}


function head_hook_meta(){
	echo '<meta name="viewport" content="width=device-width, initial-scale=1">';
}

// Отключаем jQuery WordPress
/*add_filter('wp_default_scripts', 'dequeue_jquery_migrate');

function dequeue_jquery_migrate( & $scripts) {
  if (!(is_admin_bar_showing())) {
    $scripts->remove('jquery');
  }
}*/

function footer_hook_js(){
	echo '<script src="'.get_template_directory_uri() .'/js/jquery.min.js"></script>';
	echo '<script src="'.get_template_directory_uri() .'/js/bootstrap.min.js"></script>';
	echo '<script src="'.get_template_directory_uri() .'/js/ekko-lightbox.js"></script>';
	echo '<script src="'.get_template_directory_uri() .'/js/parallax.min.js"></script>';
	}

add_action('wp_head', 'head_hook_meta');
add_action('wp_head', 'head_hook_links');
add_action('wp_footer', 'footer_hook_js');
remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );
remove_action( 'wp_head', 'wp_oembed_add_host_js' );
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

add_action('after_setup_theme', function(){
	register_nav_menus( array(
		'header_menu' => 'Меню в шапке',
		'sidebar_menu' => 'Меню сбоку'
	) );
});

add_theme_support( 'woocommerce' );
add_filter( 'woocommerce_show_page_title','__return_false' );
add_action( 'after_setup_theme', 'layerswoo_theme_setup' );
function layerswoo_theme_setup() {
   add_theme_support( 'wc-product-gallery-zoom' );
   add_theme_support( 'wc-product-gallery-lightbox' );
   add_theme_support( 'wc-product-gallery-slider' );
}

function arphabet_widgets_init() {
	register_sidebar( array(
		'name'          => 'Соц.сети верх',
		'id'            => 'block_social_top',
		'before_widget' => '<div class="d-block float-left">',
		'after_widget'  => '</div>',
		'before_title'  => '<span>',
		'after_title'   => '</span>',
	) );
	
	register_sidebar( array(
		'name'          => 'Каталог',
		'id'            => 'block_catalog',
		'before_widget' => '<div class="d-block w-100">',
		'after_widget'  => '</div>',
		'before_title'  => '<span>',
		'after_title'   => '</span>',
	) );
	
	register_sidebar( array(
		'name'          => 'Верх первая колонка',
		'id'            => 'block_top_1',
		'before_widget' => '<div class="d-xl-block d-lg-block d-md-block d-none float-right">',
		'after_widget'  => '</div>',
		'before_title'  => '<span>',
		'after_title'   => '</span>',
	) );
	
	register_sidebar( array(
		'name'          => 'Меню',
		'id'            => 'block_product_menu',
		'before_widget' => '<div class="d-block w-100">',
		'after_widget'  => '</div>',
		'before_title'  => '',
		'after_title'   => '',
	) );
	
	register_sidebar( array(
		'name'          => 'Преимущества',
		'id'            => 'bonus',
		'before_widget' => '<div class="d-block w-100">',
		'after_widget'  => '</div>',
		'before_title'  => '<span class="strong d-block mb-1">',
		'after_title'   => '</span>',
	) );

	register_sidebar( array(
		'name'          => 'Подвал первая колонка',
		'id'            => 'footer_block_1',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '<span class="strong d-block mb-2">',
		'after_title'   => '</span>',
	) );
	
	register_sidebar( array(
		'name'          => 'Подвал вторая колонка',
		'id'            => 'footer_block_2',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '<span class="strong d-block mb-2">',
		'after_title'   => '</span>',
	) );
	
	register_sidebar( array(
		'name'          => 'Подвал третья колонка',
		'id'            => 'footer_block_3',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '<span class="strong d-block mb-2">',
		'after_title'   => '</span>',
	) );
	
	register_sidebar( array(
		'name'          => 'Подвал четвертая колонка',
		'id'            => 'footer_block_4',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '<span class="strong d-block mb-2">',
		'after_title'   => '</span>',
	) );
		
	}
add_action( 'widgets_init', 'arphabet_widgets_init' );

/* Регулируем количество слов в отрывке */
function new_excerpt_length($length) {
	return 27;
}
add_filter('excerpt_length', 'new_excerpt_length');

add_filter('excerpt_more', 'new_excerpt_more');
function new_excerpt_more($more) {
	global $post;
	return '...';
}

add_filter('widget_text', 'do_shortcode');

add_filter( 'wpcf7_load_css', '__return_false' );
/*
 * "Хлебные крошки" для WordPress
 * автор: Dimox
 * версия: 2017.01.21
 * лицензия: MIT
*/
function dimox_breadcrumbs() {

  /* === ОПЦИИ === */
  $text['home'] = 'Главная'; // текст ссылки "Главная"
  $text['category'] = '%s'; // текст для страницы рубрики
  $text['search'] = 'Результаты поиска по запросу "%s"'; // текст для страницы с результатами поиска
  $text['tag'] = 'Записи с тегом "%s"'; // текст для страницы тега
  $text['author'] = 'Статьи автора %s'; // текст для страницы автора
  $text['404'] = 'Ошибка 404'; // текст для страницы 404
  $text['page'] = 'страница %s'; // текст 'Страница N'
  $text['cpage'] = 'Страница комментариев %s'; // текст 'Страница комментариев N'

  $wrap_before = '<div class="breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">'; // открывающий тег обертки
  $wrap_after = '</div><!-- .breadcrumbs -->'; // закрывающий тег обертки
  $sep = '/'; // разделитель между "крошками"
  $sep_before = '<span class="sep">'; // тег перед разделителем
  $sep_after = '</span>'; // тег после разделителя
  $show_home_link = 1; // 1 - показывать ссылку "Главная", 0 - не показывать
  $show_on_home = 0; // 1 - показывать "хлебные крошки" на главной странице, 0 - не показывать
  $show_current = 1; // 1 - показывать название текущей страницы, 0 - не показывать
  $before = '<span class="current">'; // тег перед текущей "крошкой"
  $after = '</span>'; // тег после текущей "крошки"
  /* === КОНЕЦ ОПЦИЙ === */

  global $post;
  $home_url = home_url('/');
  $link_before = '<span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">';
  $link_after = '</span>';
  $link_attr = ' itemprop="item"';
  $link_in_before = '<span itemprop="name">';
  $link_in_after = '</span>';
  $link = $link_before . '<a href="%1$s"' . $link_attr . '>' . $link_in_before . '%2$s' . $link_in_after . '</a>' . $link_after;
  $frontpage_id = get_option('page_on_front');
  $parent_id = ($post) ? $post->post_parent : '';
  $sep = ' ' . $sep_before . $sep . $sep_after . ' ';
  $home_link = $link_before . '<a href="' . $home_url . '"' . $link_attr . ' class="home">' . $link_in_before . $text['home'] . $link_in_after . '</a>' . $link_after;

  if (is_home() || is_front_page()) {

    if ($show_on_home) echo $wrap_before . $home_link . $wrap_after;

  } else {

    echo $wrap_before;
    if ($show_home_link) echo $home_link;

    if ( is_category() ) {
      $cat = get_category(get_query_var('cat'), false);
      if ($cat->parent != 0) {
        $cats = get_category_parents($cat->parent, TRUE, $sep);
        $cats = preg_replace("#^(.+)$sep$#", "$1", $cats);
        $cats = preg_replace('#<a([^>]+)>([^<]+)<\/a>#', $link_before . '<a$1' . $link_attr .'>' . $link_in_before . '$2' . $link_in_after .'</a>' . $link_after, $cats);
        if ($show_home_link) echo $sep;
        echo $cats;
      }
      if ( get_query_var('paged') ) {
        $cat = $cat->cat_ID;
        echo $sep . sprintf($link, get_category_link($cat), get_cat_name($cat)) . $sep . $before . sprintf($text['page'], get_query_var('paged')) . $after;
      } else {
        if ($show_current) echo $sep . $before . sprintf($text['category'], single_cat_title('', false)) . $after;
      }

    } elseif ( is_search() ) {
      if (have_posts()) {
        if ($show_home_link && $show_current) echo $sep;
        if ($show_current) echo $before . sprintf($text['search'], get_search_query()) . $after;
      } else {
        if ($show_home_link) echo $sep;
        echo $before . sprintf($text['search'], get_search_query()) . $after;
      }

    } elseif ( is_day() ) {
      if ($show_home_link) echo $sep;
      echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y')) . $sep;
      echo sprintf($link, get_month_link(get_the_time('Y'), get_the_time('m')), get_the_time('F'));
      if ($show_current) echo $sep . $before . get_the_time('d') . $after;

    } elseif ( is_month() ) {
      if ($show_home_link) echo $sep;
      echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y'));
      if ($show_current) echo $sep . $before . get_the_time('F') . $after;

    } elseif ( is_year() ) {
      if ($show_home_link && $show_current) echo $sep;
      if ($show_current) echo $before . get_the_time('Y') . $after;

    } elseif ( is_single() && !is_attachment() ) {
      if ($show_home_link) echo $sep;
      if ( get_post_type() != 'post' ) {
        $post_type = get_post_type_object(get_post_type());
        $slug = $post_type->rewrite;
        printf($link, $home_url . $slug['slug'] . '/', $post_type->labels->singular_name);
        if ($show_current) echo $sep . $before . get_the_title() . $after;
      } else {
        $cat = get_the_category(); $cat = $cat[0];
        $cats = get_category_parents($cat, TRUE, $sep);
        if (!$show_current || get_query_var('cpage')) $cats = preg_replace("#^(.+)$sep$#", "$1", $cats);
        $cats = preg_replace('#<a([^>]+)>([^<]+)<\/a>#', $link_before . '<a$1' . $link_attr .'>' . $link_in_before . '$2' . $link_in_after .'</a>' . $link_after, $cats);
        echo $cats;
        if ( get_query_var('cpage') ) {
          echo $sep . sprintf($link, get_permalink(), get_the_title()) . $sep . $before . sprintf($text['cpage'], get_query_var('cpage')) . $after;
        } else {
          if ($show_current) echo $before . get_the_title() . $after;
        }
      }

    // custom post type
    } elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
      $post_type = get_post_type_object(get_post_type());
      if ( get_query_var('paged') ) {
        echo $sep . sprintf($link, get_post_type_archive_link($post_type->name), $post_type->label) . $sep . $before . sprintf($text['page'], get_query_var('paged')) . $after;
      } else {
        if ($show_current) echo $sep . $before . $post_type->label . $after;
      }

    } elseif ( is_attachment() ) {
      if ($show_home_link) echo $sep;
      $parent = get_post($parent_id);
      $cat = get_the_category($parent->ID); $cat = $cat[0];
      if ($cat) {
        $cats = get_category_parents($cat, TRUE, $sep);
        $cats = preg_replace('#<a([^>]+)>([^<]+)<\/a>#', $link_before . '<a$1' . $link_attr .'>' . $link_in_before . '$2' . $link_in_after .'</a>' . $link_after, $cats);
        echo $cats;
      }
      printf($link, get_permalink($parent), $parent->post_title);
      if ($show_current) echo $sep . $before . get_the_title() . $after;

    } elseif ( is_page() && !$parent_id ) {
      if ($show_current) echo $sep . $before . get_the_title() . $after;

    } elseif ( is_page() && $parent_id ) {
      if ($show_home_link) echo $sep;
      if ($parent_id != $frontpage_id) {
        $breadcrumbs = array();
        while ($parent_id) {
          $page = get_page($parent_id);
          if ($parent_id != $frontpage_id) {
            $breadcrumbs[] = sprintf($link, get_permalink($page->ID), get_the_title($page->ID));
          }
          $parent_id = $page->post_parent;
        }
        $breadcrumbs = array_reverse($breadcrumbs);
        for ($i = 0; $i < count($breadcrumbs); $i++) {
          echo $breadcrumbs[$i];
          if ($i != count($breadcrumbs)-1) echo $sep;
        }
      }
      if ($show_current) echo $sep . $before . get_the_title() . $after;

    } elseif ( is_tag() ) {
      if ( get_query_var('paged') ) {
        $tag_id = get_queried_object_id();
        $tag = get_tag($tag_id);
        echo $sep . sprintf($link, get_tag_link($tag_id), $tag->name) . $sep . $before . sprintf($text['page'], get_query_var('paged')) . $after;
      } else {
        if ($show_current) echo $sep . $before . sprintf($text['tag'], single_tag_title('', false)) . $after;
      }

    } elseif ( is_author() ) {
      global $author;
      $author = get_userdata($author);
      if ( get_query_var('paged') ) {
        if ($show_home_link) echo $sep;
        echo sprintf($link, get_author_posts_url($author->ID), $author->display_name) . $sep . $before . sprintf($text['page'], get_query_var('paged')) . $after;
      } else {
        if ($show_home_link && $show_current) echo $sep;
        if ($show_current) echo $before . sprintf($text['author'], $author->display_name) . $after;
      }

    } elseif ( is_404() ) {
      if ($show_home_link && $show_current) echo $sep;
      if ($show_current) echo $before . $text['404'] . $after;

    } elseif ( has_post_format() && !is_singular() ) {
      if ($show_home_link) echo $sep;
      echo get_post_format_string( get_post_format() );
    }

    echo $wrap_after;

  }
} // end of dimox_breadcrumbs()

function additional_mime_types($mimes){
$mimes['ppt'] = 'application/powerpoint';
$mimes['svg'] =	'application/svg+xml';
return $mimes;
}
add_filter('upload_mimes', 'additional_mime_types');


function posts_active_class() {
  global $post_num;
  if ( ++$post_num % 2 == $post_num )
    $class = 'active';
  return $class;
}


add_filter('navigation_markup_template', 'my_navigation_template', 10, 2 );

function my_navigation_template( $template, $class ){
	/*
	Вид базового шаблона:
	<nav class="navigation %1$s" role="navigation">
		<h2 class="screen-reader-text">%2$s</h2>
		<div class="nav-links">%3$s</div>
	</nav>
	*/

	return '
	<nav class="navigation %1$s" role="navigation">
		<div class="nav-links">%3$s</div>
	</nav>    
	';
}

add_shortcode( 'subpages', 'subpages_func' );

function subpages_func(){ 
global $post; 
$my_wp_query = new WP_Query();
$children = get_page_children($post->ID,$my_wp_query->query(array('post_type' => 'page')));
echo '<ul class="subpages">';
foreach( $children as $child ){ echo '<li><a href="'.$child->guid.'">'.$child->post_title.'</a></li>'; } 
echo '</ul>'; 
}


/**
 * Добавление нового виджета.
 */
class OS_PostWidget extends WP_Widget {

	// Регистрация виджета используя основной класс
	function __construct() {
		// вызов конструктора выглядит так:
		// __construct( $id_base, $name, $widget_options = array(), $control_options = array() )
		parent::__construct(
			'os_pw', // ID виджета, если не указать (оставить ''), то ID будет равен названию класса в нижнем регистре: foo_widget
			'OS:Записи',
			array( 'description' => 'Выводит записи', /*'classname' => 'my_widget',*/ )
		);
	}

	/**
	 * Вывод виджета во Фронт-энде
	 *
	 * @param array $args     аргументы виджета.
	 * @param array $instance сохраненные данные из настроек
	 */
	function widget( $args, $instance ) {
		$title = apply_filters( 'widget_title', $instance['title'] );		
		echo $args['before_widget'];
		if ( ! empty( $title ) ) {
			echo $args['before_title'] . $title . $args['after_title'];
		}?>
		<div class="row">
		<?php global $post;
			$args = array( 'posts_per_page' => $instance['count'],'category'=> $instance['post_category'] );
			$myposts = get_posts( $args );
			foreach( $myposts as $post )
			{ setup_postdata($post);?>					
			<div id="post-<?php the_ID(); ?>" <?php post_class( $instance['class'] ); ?>>
				<a href="<?php echo get_permalink(); ?>" class="post-hover">
					<div class="col-md-12">
					<?php 
						if ($instance['thumbnail']=="on") {				
							$src = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large');
							echo '<div class="row">'.kama_thumb_img('w=360 &h=225 &yes_stub &class=img-responsive', $src[0] ).'</div>';}
						if ($instance['date']=="on") {the_date('d F Y', '<h4 class="date">', '</h4>');}
						if ($instance['vis_title']=="on") {the_title('<h4 class="title">','</h4>');}
						if ($instance['excerpt']=="on") {the_excerpt();}					
					?>
					<div class="more">Подробнее</div>
					</div>
				</a>					
			</div>
			<?php } 
			wp_reset_postdata();?>
		</div>		
		<?php echo $args['after_widget'];
	}

	/**
	 * Админ-часть виджета
	 *
	 * @param array $instance сохраненные данные из настроек
	 */
	function form( $instance ) {
		$title = @ $instance['title'] ?: '';

		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'post_category' ); ?>"><?php _e('Выберите рубрику:');?></label>
			<select class="widefat" id="<?php echo $this->get_field_id( 'post_category' ); ?>" name="<?php echo $this->get_field_name('post_category'); ?>"> 				
 				<option value="<?php echo $instance['post_category'];?>"><?php echo get_cat_name($instance['post_category']);?></option>	
				<?php
  					$categories=  get_categories('');
  					foreach ($categories as $category) {
					if ($category->cat_ID<>$instance['post_category']){				
						$option = '<option value="'.$category->cat_ID.'">';
						$option .= $category->cat_name;
						$option .= '</option>';
						echo $option;}
  					}
 				?>
			</select>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'count' ); ?>"><?php _e( 'Количество записей:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'count' ); ?>" name="<?php echo $this->get_field_name( 'count' ); ?>" type="text" value="<?php echo $instance['count']; ?>">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'class' ); ?>"><?php _e( 'Класс обертки записи:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'class' ); ?>" name="<?php echo $this->get_field_name( 'class' ); ?>" type="text" value="<?php echo $instance['class']; ?>">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'thumbnail' ); ?>"><?php _e("Отображать миниатюру?");?></label>
    			<?php if($instance['thumbnail']=="on"):?>
        			<input type="checkbox" id="<?php echo $this->get_field_id( 'thumbnail' ); ?>" name="<?php echo $this->get_field_name('thumbnail'); ?>" checked>
    			<?php else: ?>
        			<input type="checkbox" id="<?php echo $this->get_field_id( 'thumbnail' ); ?>" name="<?php echo $this->get_field_name('thumbnail'); ?>">
    			<?php endif;?>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'date' ); ?>"><?php _e("Отображать дату?");?></label>
    			<?php if($instance['excerpt']=="on"):?>
        			<input type="checkbox" id="<?php echo $this->get_field_id( 'date' ); ?>" name="<?php echo $this->get_field_name('date'); ?>" checked>
    			<?php else: ?>
        			<input type="checkbox" id="<?php echo $this->get_field_id( 'date' ); ?>" name="<?php echo $this->get_field_name('date'); ?>">
    			<?php endif;?>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'vis_title' ); ?>"><?php _e("Отображать заголовок?");?></label>
    			<?php if($instance['vis_title']=="on"):?>
        			<input type="checkbox" id="<?php echo $this->get_field_id( 'vis_title' ); ?>" name="<?php echo $this->get_field_name('vis_title'); ?>" checked>
    			<?php else: ?>
        			<input type="checkbox" id="<?php echo $this->get_field_id( 'vis_title' ); ?>" name="<?php echo $this->get_field_name('vis_title'); ?>">
    			<?php endif;?>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'excerpt' ); ?>"><?php _e("Отображать отрывок?");?></label>
    			<?php if($instance['excerpt']=="on"):?>
        			<input type="checkbox" id="<?php echo $this->get_field_id( 'excerpt' ); ?>" name="<?php echo $this->get_field_name('excerpt'); ?>" checked>
    			<?php else: ?>
        			<input type="checkbox" id="<?php echo $this->get_field_id( 'excerpt' ); ?>" name="<?php echo $this->get_field_name('excerpt'); ?>">
    			<?php endif;?>
		</p>
		<?php 
	}

	/**
	 * Сохранение настроек виджета. Здесь данные должны быть очищены и возвращены для сохранения их в базу данных.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance новые настройки
	 * @param array $old_instance предыдущие настройки
	 *
	 * @return array данные которые будут сохранены
	 */
	function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['post_category'] = ( ! empty( $new_instance['post_category'] ) ) ? strip_tags( $new_instance['post_category'] ) : '';
		$instance['count'] = ( ! empty( $new_instance['count'] ) ) ? strip_tags( $new_instance['count'] ) : '';
		$instance['class'] = ( ! empty( $new_instance['class'] ) ) ? strip_tags( $new_instance['class'] ) : '';
		$instance['thumbnail'] = ( ! empty( $new_instance['thumbnail'] ) ) ? strip_tags( $new_instance['thumbnail'] ) : '';
		$instance['date'] = ( ! empty( $new_instance['date'] ) ) ? strip_tags( $new_instance['date'] ) : '';
		$instance['vis_title'] = ( ! empty( $new_instance['vis_title'] ) ) ? strip_tags( $new_instance['vis_title'] ) : '';
		$instance['excerpt'] = ( ! empty( $new_instance['excerpt'] ) ) ? strip_tags( $new_instance['excerpt'] ) : '';
		return $instance;
	}
} 
// конец класса OS_PostWidget


/**
 * Добавление нового виджета .
 */
class OS_SocialWidget extends WP_Widget {

	// Регистрация виджета используя основной класс
	function __construct() {
		// вызов конструктора выглядит так:
		// __construct( $id_base, $name, $widget_options = array(), $control_options = array() )
		parent::__construct(
			'os_sw', // ID виджета, если не указать (оставить ''), то ID будет равен названию класса в нижнем регистре: foo_widget
			'OS:Соц.сети',
			array( 'description' => 'Выводит ссылки на соц.сети', /*'classname' => 'my_widget',*/ )
		);
	}

	/**
	 * Вывод виджета во Фронт-энде
	 *
	 * @param array $args     аргументы виджета.
	 * @param array $instance сохраненные данные из настроек
	 */
	function widget( $args, $instance ) {
		$title = apply_filters( 'widget_title', $instance['title'] );		
		echo $args['before_widget'].'<div class="os-soc">';
		if ( ! empty( $title ) ) {
			echo $args['before_title'] . $title . $args['after_title'];}
		if ( ! empty( $instance['class'] ) ) {$class=' class="'.$instance['class'].'"';}
		if ( ! empty( $instance['sc'] ) ) { echo '<a href="'.$instance['sc'].'"'.$class.' id="search" data-toggle="modal" data-target="#Search"><i class="fa fa-search" aria-hidden="true"></i></a>';}
		if ( ! empty( $instance['fb'] ) ) { echo '<a href="'.$instance['fb'].'"'.$class.'><i class="fa fa-facebook" aria-hidden="true"></i></a>';}
		if ( ! empty( $instance['ok'] ) ) { echo '<a href="'.$instance['ok'].'"'.$class.'><i class="fa fa-instagram" aria-hidden="true"></i></a>';}
		if ( ! empty( $instance['vk'] ) ) { echo '<a href="'.$instance['vk'].'"'.$class.'><i class="fa fa-vk" aria-hidden="true"></i></a>';}
		if ( ! empty( $instance['mm'] ) ) { echo '<a href="'.$instance['mm'].'"'.$class.'><i class="fa fa-youtube" aria-hidden="true"></i></a>';}
	
		echo $args['after_widget'].'</div>';
	}

	/**
	 * Админ-часть виджета
	 *
	 * @param array $instance сохраненные данные из настроек
	 */
	function form( $instance ) {
		$title = @ $instance['title'] ?: '';

		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'sc' ); ?>"><?php _e( 'Поиск:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'sc' ); ?>" name="<?php echo $this->get_field_name( 'sc' ); ?>" type="text" value="<?php echo $instance['sc']; ?>">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'fb' ); ?>"><?php _e( 'Facebook:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'fb' ); ?>" name="<?php echo $this->get_field_name( 'fb' ); ?>" type="text" value="<?php echo $instance['fb']; ?>">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'ok' ); ?>"><?php _e( 'Instagram:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'ok' ); ?>" name="<?php echo $this->get_field_name( 'ok' ); ?>" type="text" value="<?php echo $instance['ok']; ?>">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'vk' ); ?>"><?php _e( 'ВКонтакте:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'vk' ); ?>" name="<?php echo $this->get_field_name( 'vk' ); ?>" type="text" value="<?php echo $instance['vk']; ?>">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'mm' ); ?>"><?php _e( 'YouTube:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'mm' ); ?>" name="<?php echo $this->get_field_name( 'mm' ); ?>" type="text" value="<?php echo $instance['mm']; ?>">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'class' ); ?>"><?php _e( 'Класс ссылки:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'class' ); ?>" name="<?php echo $this->get_field_name( 'class' ); ?>" type="text" value="<?php echo $instance['class']; ?>">
		</p>
		
		<?php 
	}

	/**
	 * Сохранение настроек виджета. Здесь данные должны быть очищены и возвращены для сохранения их в базу данных.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance новые настройки
	 * @param array $old_instance предыдущие настройки
	 *
	 * @return array данные которые будут сохранены
	 */
	function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['sc'] = ( ! empty( $new_instance['sc'] ) ) ? strip_tags( $new_instance['sc'] ) : '';
		$instance['fb'] = ( ! empty( $new_instance['fb'] ) ) ? strip_tags( $new_instance['fb'] ) : '';
		$instance['ok'] = ( ! empty( $new_instance['ok'] ) ) ? strip_tags( $new_instance['ok'] ) : '';
		$instance['vk'] = ( ! empty( $new_instance['vk'] ) ) ? strip_tags( $new_instance['vk'] ) : '';
		$instance['mm'] = ( ! empty( $new_instance['mm'] ) ) ? strip_tags( $new_instance['mm'] ) : '';
		$instance['class'] = ( ! empty( $new_instance['class'] ) ) ? strip_tags( $new_instance['class'] ) : '';
		return $instance;
	}
} 
// конец класса OS_SocialWidget

// регистрация OS_Widgets в WordPress
function register_os_widget() {
	register_widget( 'OS_SocialWidget' );
	register_widget( 'OS_PostWidget' );
}
add_action( 'widgets_init', 'register_os_widget' );

//Скрытое поле для запроса цены
//
add_filter( 'shortcode_atts_wpcf7', 'custom_shortcode_atts_wpcf7_filter', 10, 3 );
 
function custom_shortcode_atts_wpcf7_filter( $out, $pairs, $atts ) {
 	global $post;
    if (! isset( $atts['cars'] ) ) {
		$doorsname = ''.$post->post_title.' '.$post->guid.'';
        $out['cars'] = $doorsname;
    } 
    return $out;
}

//Добавляем классы к элементам меню
//
//
add_filter( 'nav_menu_css_class', 'bootstrap_class_to_nav_menu', 10, 2 );

function bootstrap_class_to_nav_menu( $classes, $item ){
	/* $classes содержит
	Array(
		[1] => menu-item
		[2] => menu-item-type-post_type
		[3] => menu-item-object-page
		[4] => menu-item-284
	)
	*/
	$classes[1] = 'nav-item';

	return $classes;
}

class True_Walker_Nav_Menu extends Walker_Nav_Menu {
	/**
	 * @see Walker::start_el()
	 * @since 3.0.0
	 *
	 * @param string $output
	 * @param object $item Объект элемента меню, подробнее ниже.
	 * @param int $depth Уровень вложенности элемента меню.
	 * @param object $args Параметры функции wp_nav_menu
	 */
	function start_el(&$output, $item, $depth, $args) {
		global $wp_query;           
		/*
		 * Некоторые из параметров объекта $item
		 * ID - ID самого элемента меню, а не объекта на который он ссылается
		 * menu_item_parent - ID родительского элемента меню
		 * classes - массив классов элемента меню
		 * post_date - дата добавления
		 * post_modified - дата последнего изменения
		 * post_author - ID пользователя, добавившего этот элемент меню
		 * title - заголовок элемента меню
		 * url - ссылка
		 * attr_title - HTML-атрибут title ссылки
		 * xfn - атрибут rel
		 * target - атрибут target
		 * current - равен 1, если является текущим элементов
		 * current_item_ancestor - равен 1, если текущим является вложенный элемент
		 * current_item_parent - равен 1, если текущим является вложенный элемент
		 * menu_order - порядок в меню
		 * object_id - ID объекта меню
		 * type - тип объекта меню (таксономия, пост, произвольно)
		 * object - какая это таксономия / какой тип поста (page /category / post_tag и т д)
		 * type_label - название данного типа с локализацией (Рубрика, Страница)
		 * post_parent - ID родительского поста / категории
		 * post_title - заголовок, который был у поста, когда он был добавлен в меню
		 * post_name - ярлык, который был у поста при его добавлении в меню
		 */
		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
 
		/*
		 * Генерируем строку с CSS-классами элемента меню
		 */
		$class_names = $value = '';
		$classes = empty( $item->classes ) ? array() : (array) $item->classes;
		$classes[] = 'menu-item-' . $item->ID;
 
		// функция join превращает массив в строку
		$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
		$class_names = ' class="' . esc_attr( $class_names ) . '"';
 
		/*
		 * Генерируем ID элемента
		 */
		$id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
		$id = strlen( $id ) ? ' id="' . esc_attr( $id ) . '"' : '';
 
		/*
		 * Генерируем элемент меню
		 */
		$output .= $indent . '<li' . $id . $value . $class_names .'>';
 
		// атрибуты элемента, title="", rel="", target="" и href=""
		$attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
		$attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
		$attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
		$attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
 
		// ссылка и околоссылочный текст
		$item_output = $args->before;
		$item_output .= '<a class="nav-link" '. $attributes .'>';
		$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
		$item_output .= '</a>';
		$item_output .= $args->after;
 
 		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}
}


//Страница для каталога

add_action('init', 'my_partners_init');
function my_partners_init(){
	register_post_type('partners', array(
		'labels'             => array(
			'name'               => 'Ссылка', // Основное название типа записи
			'singular_name'      => 'Ссылка', // отдельное название записи типа Book
			'add_new'            => 'Добавить ссылку',
			'add_new_item'       => 'Добавить новую ссылку',
			'edit_item'          => 'Редактировать ссылку',
			'new_item'           => 'Новая ссылка',
			'view_item'          => 'Посмотреть сслыку',
			'search_items'       => 'Найти сслыку',
			'not_found'          =>  'Ссылка не найдена',
			'not_found_in_trash' => 'В корзине ссылок не найдено',
			'parent_item_colon'  => '',
			'menu_name'          => 'Ссылки'

		  ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array('with_front' => true),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => true,
		'menu_position'      => null,
		'taxonomies'          => array(),
		'supports'           => array('title','author','thumbnail','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats')	
	) );
}

add_filter('post_updated_messages', 'partners_updated_messages');
function partners_updated_messages( $messages ) {
	global $post;

	$messages['partners'] = array(
		0 => '', // Не используется. Сообщения используются с индекса 1.
		1 => sprintf( 'Ссылка обновлена. <a href="%s">Посмотреть сслыку</a>', esc_url( get_permalink($post->ID) ) ),
		2 => 'Произвольное поле обновлено.',
		3 => 'Произвольное поле удалено.',
		4 => 'Сслыка обновлена.',
		/* %s: дата и время ревизии */
		5 => isset($_GET['revision']) ? sprintf( 'Сслыка восстановлена из ревизии %s', wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( 'Ссылка размещена. <a href="%s">Перейти к ссылке</a>', esc_url( get_permalink($post->ID) ) ),
		7 => 'Ссылка сохранена.',
		8 => sprintf( 'Сссылка сохранен. <a target="_blank" href="%s">Предпросмотр ссылки</a>', esc_url( add_query_arg( 'preview', 'true', get_permalink($post->ID) ) ) ),
		9 => sprintf( 'Размещение сслыки запланировано на: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Предпросмотр ссылки</a>',
		  // Как форматировать даты в PHP можно посмотреть тут: http://php.net/date
		  date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post->ID) ) ),
		10 => sprintf( 'Черновик ссылки обновлен. <a target="_blank" href="%s">Предпросмотр ссылки</a>', esc_url( add_query_arg( 'preview', 'true', get_permalink($post->ID) ) ) ),
	);

	return $messages;
}
?>