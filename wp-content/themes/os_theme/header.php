<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "main-content" div.
 *
 * @package WordPress
 * @subpackage os_theme
 * @since 1.0
 */

	header("Content-type: text/html; charset=UTF-8");
	//header("Cache-Control: max-age=14400"); //
	//header("Expires: ".gmdate("D, d M Y H:i:s", time() + 14400)." GMT");//
	header("X-UA-Compatible: IE=Edge");

 ?>
<!doctype html>
<html <?php language_attributes();?>>

<head>
	<?php wp_head();?>
</head>

<body <?php if (! is_front_page()) {body_class('rest');} else {body_class();} ?>>

<?php if ( ! isset( $content_width ) ) $content_width = 900;if (is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) wp_enqueue_script('comment-reply'); ?>

<div class="wrapper container-fluid <?php if (is_front_page()){ echo 'text-white';} else { echo 'text-black';}?>">

	<?php get_template_part( 'inc/block','nav' ); ?>
	
