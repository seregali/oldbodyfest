<?php
/**
 * Template for displaying search forms
 *
 * @package WordPress
 * @subpackage os_theme
 * @since 1.0
 */
?>

<?php $unique_id = esc_attr( uniqid( 'search-form-' ) ); ?>

<form role="search" method="get" class="search-form navbar-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<div class="input-group">
		<input type="search" id="<?php echo $unique_id; ?>" class="search-field form-control" placeholder="<?php echo esc_attr_x( 'Search &hellip;', 'placeholder', 'twentyseventeen' ); ?>" value="<?php echo get_search_query(); ?>" name="s" />
		<div class="input-group-btn">
			<button type="submit" class="search-submit btn btn-default"><i class="glyphicon glyphicon-search"></i></button>
		</div>
	</div>
</form>
