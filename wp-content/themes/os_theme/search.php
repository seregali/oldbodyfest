<?php 
/**
 * Template for search form
 *
 * @package WordPress
 * @subpackage os_theme
 * @since 1.0
 */
 get_header(); ?>
<div class="title" style="background:url('/wp-content/uploads/2017/12/slide2.jpg') center center no-repeat;">
	<div class="container">		
		<h3>Поиск по: "<?php echo $_GET['s'];?>"</h3>
	</div>
</div>
<div class="container search">
	
	 <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<div class="col-md-12"> 
			<strong>Страница : <a href="<?php the_permalink();?>"><?php the_title(); ?></a></strong>
			<?php the_excerpt(''); ?>
		</div>
 	<?php endwhile; else: ?>
 		<p>Поиск не дал результатов.</p>
 	<?php endif;?>
</div>
<?php get_footer(); ?>