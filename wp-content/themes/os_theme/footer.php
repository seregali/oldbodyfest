<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "wrapper" div.
 *
 * @package WordPress
 * @subpackage os_theme
 * @since 1.0
 */
?>			
	
	<?php get_template_part( 'inc/block','contacts' ); ?>
	
	<?php get_template_part( 'inc/block','copyright-one-col' ); ?>

	
	<!--a href="/my-account" id="house" class="scrlBtn" style="display: block;"  title="Личный кабинет"></a>
	<a href="/cart" id="cart" class="scrlBtn" style="display: block;" title="Корзина"></a>		
	<a href="#" id="wardrobe" class="scrlBtn" style="display: block;"  title="Гардероб"></a-->
	<div id="scrollup" class="scrlBtn" style="display: block;"  title="Наверх"><i class="fa fa-chevron-up"></i></div>
	
	<div class="all_scripts">
		<?php wp_footer(); ?>
		<script>
			$(document).ready(function(){
				$('body').addClass('fadeIn');
				$('.animTop').addClass('fadeInTop');
				$('.animBottom').addClass('fadeInBottom');
				$('.wishlist_products_counter').addClass('wardrobe');
			});	
			$(document).on('click', '[data-toggle="lightbox"]', function(event) {
    			event.preventDefault();
   				$(this).ekkoLightbox();
			});
			$(function(){
       			var h_hght = $('.block-top').outerHeight();
       			var h_nav = $('.block-nav').outerHeight();
       			var top;
       			$(window).scroll(function(){
           			top = $(this).scrollTop();
           			if(h_hght <= top){
           				$('.block-nav').addClass('fixed-top bg-sran px-3');
						$('.wrapper').css({'margin-top':h_nav});
          			}
           			else if(top < h_hght && top > 0){
              			$('.block-nav').removeClass('fixed-top bg-sran px-3');
						$('.wrapper').css({'margin-top':'0px'});
           			}
      			});
   			});
			$( document ).ready(function() {
				$('#scrollup i').mouseover( function(){
					$( this ).animate({opacity: 0.65},100);
				}).mouseout( function(){
					$( this ).animate({opacity: 1},100);
				}).click( function(){
					window.scroll(0 ,0); 
				return false;
				});
				$(window).scroll(function(){
					if ( $(document).scrollTop() > 0 ) {
						$('.scrlBtn').fadeIn('slow');
					} else {
						$('.scrlBtn').fadeOut('slow');
					}
				});
			});	
			$('.product-categories li.cat-parent>a').addClass('selected');
			$('a.selected').click(function(e) {
    			e.preventDefault();
				if ($(this).hasClass('in')) {
					$(this).next('.children').slideUp( "fast" );
					$(this).removeClass('in');
				}
				else {
					$('.selected').removeClass('in');
					$('.selected').next('.children').slideUp( "fast" );
					$(this).addClass('in');
					$(this).next('.children').slideDown( "fast" );
				}
    		});
		</script>
	</div>
</div>
</body>
</html>